# HTML5 TypeScript Game Framework

## This is a game development framework using TypeScript.
If you do not have TypeScript installed, you can download it from npm.  
```npm install typescript --global```

## Cloning this repository
This repository uses Git Large File Support (LFS).  
A Git GUI client like Sourcetree or GitHub Desktop (which has LFS pre-installed) will make this easier.  

## To compile the framework
1. Open a terminal in the root folder.
2. Run ```npm install``` to get the required dependencies.
3. Run one of the following npm scripts:
	* ```npm run build```  Compiles in production mode.
	* ```npm run debug```  Compiles in development mode. (Includes inline source maps for easy debugging)
	* ```npm run watch```  Same as debug, but runs in watch mode (Automatically compiles when you save changes)
4. This will create/update the **scriptbundle.js** in the build folder.
	
## Once the framework is compiled
Once the scripts folder has been compiled, you can use the build folder in your server.  
For easy testing, the ["Visual Studio Code" editor](https://code.visualstudio.com/) and the ["Live Server" extension](https://marketplace.visualstudio.com/items?itemName=ritwickdey.LiveServer) are recommended.  

import { Time, FixedUpdateLoop } from "./Modules/System/Time";
import { Keyboard } from "./Modules/Input/Keyboard";
import { Debug } from "./Modules/System/Debug";
import { ClearDestroyQueue, ProcessDestructionQueue, CleanDestructionQueue } from "./Modules/GameObjects/BaseObject";
import { CleanObjectComponents } from "./Modules/GameObjects/GameObject";
import { Camera2D } from "./Modules/Render2D/Camera2D";
import { HandleUpdates, HandleLateUpdates, HandleStarts, HandleFixedUpdates } from "./Modules/GameObjects/Script";
import { PhysicsUpdate, SetupPhysics, PhysicsInterpolation, DrawPhysicsDebugData } from "./Modules/Physics2D/Physics2D";
import { HandleAnimationUpdates } from "./Modules/Animation/Animator";
import { CollisionFilter } from "./Modules/Physics2D/CollisionFilter";
import { AssetManager, BatchData } from "./Modules/System/AssetManager";
import { Mouse } from "./Modules/Input/Mouse";
import { ServiceWorkerHandler } from "./Modules/System/ServiceWorkerHandler";
import { SetupDebugScene } from "./TestClasses/DebugSetupScene";
import { LoadingScreen } from "./Modules/System/LoadingScreen";
import { InitializeGame } from "./Modules/System/GameInitializator";
import { PhysicsObjectHandler } from "./Modules/Physics2D/Physics2DObjectHandler";
//import { RuntimeLoop } from "./Modules/System/RuntimeLoop";

//Start the game
(function(){
	//Initialize the game
	InitializeGame()

	//Start the game
	.then(() => RuntimeLoop())
	.catch(err => console.error("Failed to start game.", err));
})();

function RuntimeLoop(timestamp: number = 0) {
  //Preparations
  requestAnimationFrame(RuntimeLoop);
  Time.calculateNextFrameTime(timestamp);
  Camera2D.ClearCanvas("#303");

  //Initialization
  HandleStarts();

  //Fixed Time Step here
  FixedUpdateLoop(function() {
    HandleFixedUpdates();
    PhysicsUpdate();
  });
  //Physics interpolation
  PhysicsInterpolation();

  //Update Game State
  HandleUpdates();
  HandleAnimationUpdates();
  HandleLateUpdates();

	//Remove destroyed objects from the scene, so we don't draw them.
	ProcessDestructionQueue();
	//Clear any physics bodies marked for removal.
	PhysicsObjectHandler.CleanPhysicsObjectsMap();

  //Render Actions
  Camera2D.Draw();
  DrawPhysicsDebugData();

  //Prepare for next loop
  Keyboard.LateUpdate();
  Mouse.LateUpdate();
	Debug.handleDebugCommands();
	
	//Defer Destruction and deep reference clearing to the end
	//ClearDestroyQueue();
	CleanDestructionQueue();

	//Post Destroy Actions
	CleanObjectComponents();
}

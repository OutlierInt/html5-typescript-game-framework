import { GameObject } from "../Modules/GameObjects/GameObject";
import { Vector } from "../Modules/Math/Vector";
import { SpriteRenderer } from "../Modules/Render2D/SpriteRenderer";
import { Sprite } from "../Modules/Render2D/Sprite";
import { CircleCollider2D } from "../Modules/Physics2D/CircleCollider2D";
import { Physics2D } from "../Modules/Physics2D/Physics2D";
import { Script } from "../Modules/GameObjects/Script";
import { Time } from "../Modules/System/Time";
import { Rigidbody2D, RigidbodyType } from "../Modules/Physics2D/Rigidbody2D";
import { Physics2DUtil } from "../Modules/Physics2D/Physics2DUtil";
import { PhysicsGameObjectMap } from "../Modules/Physics2D/Physics2DObjectHandler";
import { Mouse } from "../Modules/Input/Mouse";
import { Collider2D } from "../Modules/Physics2D/Collider2D";

export class PhysicsController extends Script {

	fireRate = 0;
	fireRateMax = 0.1;
	player: GameObject | null = null;

	Start(){
		this.player = GameObject.Find("Player");
	}

	Update() {
		if (Mouse.GetMouse(0) && this.fireRate <= 0) {
      this.fireRate = this.fireRateMax;
      const obj = new GameObject("BallGO");
      obj.transform.position = new Vector(Math.random() * 1280, 25 * Math.random());

      const spriteRenderer = obj.AddComponent(SpriteRenderer);
      spriteRenderer.debugSetup();
      spriteRenderer.scale = new Vector(3, 3);
      let sprite = spriteRenderer.sprite as Sprite;

      const collider = obj.AddComponent(CircleCollider2D);
			collider.Radius = sprite.height * spriteRenderer.scale.y;
			
			const rigidbody = obj.AddComponent(Rigidbody2D);
			
			let torque = 1000000;
			torque *= Math.random() > 0.5 ? -1 : 1;
			///@ts-ignore
			collider.body.ApplyTorque(torque);

			//if(this.player){
			//	const pCollider = this.player.GetComponent(Collider2D);
			//	if(pCollider) Physics2D.IgnoreCollision(collider, pCollider, true);
			//}

			obj.Destroy(10);
		}

		if (this.fireRate > 0) this.fireRate -= Time.deltaTime;
	}
}
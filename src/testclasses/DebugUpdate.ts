import { Script } from "../Modules/GameObjects/Script";
import { Keyboard } from "../Modules/Input/Keyboard";
import { Key } from "../Modules/Input/Keys";
import { Time } from "../Modules/System/Time";
import { Camera2D } from "../Modules/Render2D/Camera2D";
import { Vector } from "../Modules/Math/Vector";
import { MathX } from "../Modules/Math/MathX";
import { Debug } from "../Modules/System/Debug";
import { Mouse } from "../Modules/Input/Mouse";
import { BaseObjects } from "../Modules/GameObjects/BaseObject";
import { GameObject } from "../Modules/GameObjects/GameObject";
import { Physics2D } from "../Modules/Physics2D/Physics2D";
import { ParticleSystem } from "../Modules/Render2D/ParticleRenderer";
import { canvasContainer, canvas } from "../Modules/System/Canvas";

setInterval(() => {
	DebugUpdate.valueTracker = (performance as any).memory;
}, 500);

export class DebugUpdate extends Script{
	timeStep = 0;
	testVector = new Vector(5,5,5,5);
	static valueTracker: any;

	private debugLinePosition2 = new Vector(5, 50);
	private debugLinePosition3 = new Vector(5, 75);
	private debugLinePosition3b = new Vector(5, 100);
	private debugLinePosition4 = new Vector(5, 125);
	private center = new Vector(1280 / 2, 720 / 2);

	boxy: GameObject | null = null;
	boxyChild: GameObject | null = null;
	boxyGrandChild: GameObject | null = null;

	particles: GameObject | null = null;
	particleSystem: ParticleSystem | null = null;

	//fullscreen = false;

	Start(){
		this.boxy = GameObject.Find("Boxy", GameObject);
		this.boxyChild = GameObject.Find("BoxyChild", GameObject);
		this.boxyGrandChild = GameObject.Find("BoxyGrandChild", GameObject);

		this.particles = GameObject.Find("Particles", GameObject);
		if(this.particles) this.particleSystem = this.particles.GetComponent(ParticleSystem);

	}

	Update(){
		//Slow Time
		if (Keyboard.GetKeyDown(Key.R)) {
			this.timeStep = (this.timeStep + 1) % 10;
			Time.timescale = MathX.Lerp(1, 0.1, this.timeStep / (10 - 1));
			Time.fixedDeltaTime = Time.defaultFixedTime * Time.timescale;
		}

		//Move Camera
		if (Keyboard.GetKey(Key.UpArrow)) Camera2D.cameraPosition.y -= 750 * Time.deltaTime;
		if (Keyboard.GetKey(Key.DownArrow)) Camera2D.cameraPosition.y += 750 * Time.deltaTime;
		if (Keyboard.GetKey(Key.LeftArrow)) Camera2D.cameraPosition.x -= 750 * Time.deltaTime;
		if (Keyboard.GetKey(Key.RightArrow)) Camera2D.cameraPosition.x += 750 * Time.deltaTime;
		if (Keyboard.GetKeyDown(Key.C)) Camera2D.cameraPosition = Vector.zero;

		//Test Reset Constructor call
		if(Keyboard.GetKeyDown(Key.Y)){
			//const constructor = new Vector();
			//constructor.call(this.testVector);

			//let newVector = {};
			//Reflect.construct(Vector.prototype.constructor, [], newVector);
			//console.log(this.testVector);
			//this.testReinitialize();

			console.log(this.testVector);

			//const newVector = new Vector();
			//Object.assign(this.testVector, newVector); 

			const constructText = Vector.prototype.constructor.toString();
			console.log(constructText);


			console.log(this.testVector);
		}

		//Toggle Physics2d debug draw
		if (Keyboard.GetKeyDown(Key.U)) { Physics2D.DrawDebugShapes = !Physics2D.DrawDebugShapes; }

		if (Keyboard.GetKeyDown(Key.I)) {
			if(Keyboard.GetKey(Key.Shift))
				Camera2D.cameraScale = Camera2D.cameraScale === 1 ? 1 / 5 : 1;
			else
				Camera2D.cameraScale = Camera2D.cameraScale === 1 ? 5 : 1;
		}

		if(Keyboard.GetKeyDown(Key.Equals)) Camera2D.cameraRotation = (Camera2D.cameraRotation + 360/16 * MathX.DEG2RAD) % (MathX.PI2);
		//Camera2D.cameraRotation = (Time.time * 360 * MathX.DEG2RAD) % (2 * Math.PI);
		//Camera2D.cameraScale = Time.time % 5;

		//Raycasting
		//let rayCount = 8;
		//let raySpeed = 1;
		//let rayAngle = 2 * Math.PI;
		//for(let i = 0; i < rayCount; i++){
		//	const endPoint = this.center.add(new Vector(1000).Rotate2D(Time.time * raySpeed + (rayAngle / rayCount * i) ));
		//	const raycastResult = Physics2D.Raycast(this.center, endPoint, 65533); //Doesn't collide with bullets.
		//	if(raycastResult){
		//		Debug.DrawLine(this.center, raycastResult.point, "#0F0");
		//		Debug.DrawLine(raycastResult.point, raycastResult.point.add(raycastResult.normal.multiply(50)), "#FFF");
		//	}
		//	else{
		//		Debug.DrawLine(this.center, endPoint, "#F00");
		//	}
		//}
		
		if(this.boxy && this.boxyChild && this.boxyGrandChild){
			const parent = this.boxy.transform;
			const child = this.boxyChild.transform;
			const grandchild = this.boxyGrandChild.transform;

			if (Keyboard.GetKeyDown(Key.Backspace)){
				if(Keyboard.GetKey(Key.Shift)){
					this.boxyGrandChild.Destroy();
				}
				else{
					this.boxy.isEnabled ? this.boxy.Disable() : this.boxy.Enable();
				}
			}

			//const cScl = child.scale.invScale(parent.scale);
			//const cRot = child.rotation.subtract(parent.rotation);
			//const cPos = child.position.subtract(parent.position);

			//const offset = child
			//	.localPosition
			//	.Clone()
			//	.scaleVolatile(child.scale)
			//	.rotate2DVolatile(child.getAngle2d);
			//child.position = this.position.add(offset);

			//Debug.DrawLine(parent.position, parent.position.add(cPos), "#F00");

			//Debug.print(`
			//	BoxC Pos: ${child.localPosition.toString()} |
			//	BoxC Rot: ${(child.getAngle2d * MathX.RAD2DEG).toFixed(0)} |
			//	BoxC Scl: ${child.localScale.toString()}`,
			//this.debugLinePosition4);

			//Debug.print(`
			//	GCBox Pos: ${grandchild.localPosition.toString()}`,
			//this.debugLinePosition4);
		}

		if(this.particleSystem){
			Debug.print(`
				Particles: ${this.particleSystem.particles.length}`,
			this.debugLinePosition4);
		}

		if (Keyboard.GetKeyDown(Key.Numpad1)) Debug.enablePrint = !Debug.enablePrint;
		if (Keyboard.GetKeyDown(Key.Numpad2)) Debug.enableDrawLine = !Debug.enableDrawLine;

		//if(Keyboard.GetKeyDown(Key.Numpad3)){
		//	this.fullscreen = !this.fullscreen;
		//	if(this.fullscreen){
		//		document.body.classList.add("fullscreen");
		//		canvasContainer.classList.remove("canvas-container");
		//		canvasContainer.classList.add("canvas-fullscreen");
		//		canvas.width = document.body.clientWidth;
		//		canvas.height = document.body.clientHeight;
		//		//const fullscreen = document.body.requestFullscreen || document.body.webkitRequestFullscreen || document.body.webkitRequestFullScreen || null;
		//		//if(fullscreen) fullscreen.call(document.body);
		//	}
		//	else{
		//		document.body.classList.remove("fullscreen");
		//		canvasContainer.classList.remove("canvas-fullscreen");
		//		canvasContainer.classList.add("canvas-container");
		//		canvas.width = 1280;
		//		canvas.height = 720;
		//		canvasContainer.setAttribute("width", canvas.width.toString());
		//		canvasContainer.setAttribute("height", canvas.height.toString());
		//		//document.exitFullscreen();
		//	}
		//}

		//Test Update
		Debug.print(`
			BaseObjects: ${BaseObjects.size} |
			GameObjects: ${GameObject.getGameObjectCount()} |
			FPS: ${Time.fps.toFixed(3)} |
			Timescale: ${Time.timescale.toFixed(2)}`);

		Debug.print(`
			FixedDeltaTime: ${Time.fixedDeltaTime} |
			Camera Pos: ${Camera2D.cameraPosition.toString()} |
			Mouse: ${Mouse.mousePosition.toString()}`,
		this.debugLinePosition2);

		Debug.print(`
			Game Time: ${Time.time.toFixed(3)} |
			Real Time: ${Time.realTime.toFixed(3)} |
			Test Vector: ${this.testVector.toString()}`,
		this.debugLinePosition3);
		//Particles: ${particleSystem.positions.length} |
		
		if(DebugUpdate.valueTracker){
			Debug.print(`
				${(Number(DebugUpdate.valueTracker.usedJSHeapSize)/1024/1024).toFixed(1)} / ${(Number(DebugUpdate.valueTracker.totalJSHeapSize)/1024/1024).toFixed(1)} MB`,
			this.debugLinePosition3b);
		}
		
	}

	testReinitialize(){
		const defaultObject = new Vector();
		for (const key in defaultObject) {
			if (defaultObject.hasOwnProperty(key)) {
				const element = (defaultObject as any)[key];
				switch (typeof element) {
					//Copy properties that are passed by value.
					case "number":
					case "boolean":
					case "string":
					case "undefined":
					case "symbol":
						(this.testVector as any)[key] = element;
						break;

					//Deal with reference properties
					case "object":
					case "function":
						//Do nothing...
						break;
				}
			}
		}
	}
}

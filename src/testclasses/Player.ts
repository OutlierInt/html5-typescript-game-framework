import { Script } from "../Modules/GameObjects/Script";
import { Vector } from "../Modules/Math/Vector";
import { Keyboard } from "../Modules/Input/Keyboard";
import { Time } from "../Modules/System/Time";
import { Key } from "../Modules/Input/Keys";
import { GameObject } from "../Modules/GameObjects/GameObject";
import { SpriteRenderer } from "../Modules/Render2D/SpriteRenderer";
import { Laser } from "./Laser";
import { MathX } from "../Modules/Math/MathX";
import { AssetManager } from "../Modules/System/AssetManager";
import { BoxCollider2D } from "../Modules/Physics2D/BoxCollider2D";
import { Physics2DUtil } from "../Modules/Physics2D/Physics2DUtil";
import { Sprite } from "../Modules/Render2D/Sprite";
import { Rigidbody2D } from "../Modules/Physics2D/Rigidbody2D";
import { Transform } from "../Modules/GameObjects/Transform";

export class Player extends Script{
	moveSpeed = 800;
	bulletSpeed = 3000;
	waveSpeed = 3000;
	fireRate = 0;
	fireRateMax = 0.14;
	fireRateManualMax = 0.02;
	animFrames: Sprite[] = [];
	curFrame = 2;
	animSpeed = 10;
	spriteRenderer: SpriteRenderer | null = null;
	fireLocations: Transform[] = [];
	fireSound = AssetManager.TryGetAudio("sounds/testfire.ogg");

	Start(){
		//console.log(`Player of [GameObject "${this.gameObject.name}"] initialized!`);
		this.GetAnimationFrames();
		this.spriteRenderer = this.gameObject.GetComponent(SpriteRenderer);
		this.fireSound.volume = 0.5;
	}

	Update(){
		let moveDirection = Vector.zero;
		if (Keyboard.GetKey(Key.W)) { moveDirection.y -= 1; }
		if (Keyboard.GetKey(Key.S)) { moveDirection.y += 1; }
		if (Keyboard.GetKey(Key.A)) { moveDirection.x -= 1; }
		if (Keyboard.GetKey(Key.D)) { moveDirection.x += 1; }
		moveDirection = moveDirection .Normalized.multiply(this.moveSpeed * Time.deltaTime);
		this.gameObject.transform.position = this.gameObject.transform.position.add(moveDirection);

		if(Keyboard.GetKeyUp(Key.Enter)) this.fireRate = Math.min(this.fireRate, this.fireRateManualMax);

		if(Keyboard.GetKey(Key.Enter) || Keyboard.GetKeyDown(Key.ForwardSlash)){
			if(this.fireRate <= 0){
				//this.FireBullet(new Vector(90, 0));

				//this.FireBullet(new Vector(90, -25));
				//this.FireBullet(new Vector(90, 25));
				
				//Old
				//this.FireBullet(new Vector(0, -75), -0, -0);
				//this.FireBullet(new Vector(0, 75),   0,  0);
				
				this.FireBullet(new Vector(50,0).add(this.fireLocations[0].position),  0,  0);// MathX.RandomRange(0,90),  MathX.RandomRange(25,100));
				this.FireBullet(new Vector(50,0).add(this.fireLocations[1].position), -0, -0);//-MathX.RandomRange(0,90), -MathX.RandomRange(25,100));


				//const multiplier = 4;
				//for(let i = 0; i < 10 * multiplier; i++){
				//	this.FireWave(new Vector(100, 10 + (i * 15 / multiplier)),  i * 3 / multiplier);
				//	this.FireWave(new Vector(100, 10 - (i * 15 / multiplier)), -i * 3 / multiplier);
				//}
				
				//this.FireWave(new Vector(100,  10),  8);
				//this.FireWave(new Vector(100, -10), -8);
				this.FireWave(new Vector(85,  10),  2);
				this.FireWave(new Vector(85, -10), -2);
				this.FireWave(new Vector(100, 0));

				this.fireRate = this.fireRateMax;
				this.fireSound.currentTime = 0;
				this.fireSound.play();

				let spriteFlashTall = new Sprite("images/testsheet.png");
				//spriteFlashTall.drawScaleX = 3;
				//spriteFlashTall.drawScaleY = 3;
				spriteFlashTall.width = 12;
				spriteFlashTall.height = 29;
				spriteFlashTall.sourceX = 232;
				spriteFlashTall.sourceY = 256;
				//spriteFlashTall.updateSpriteInfo();

				let spriteFlashSmall = new Sprite("images/testsheet.png");
        //spriteFlashSmall.drawScaleX = 3;
        //spriteFlashSmall.drawScaleY = 3;
        spriteFlashSmall.width = 15;
        spriteFlashSmall.height = 14;
        spriteFlashSmall.sourceX = 283;
        spriteFlashSmall.sourceY = 263;
				//spriteFlashSmall.updateSpriteInfo();

				this.CreateMuzzleFlash(spriteFlashTall, new Vector(70, 0).add(this.gameObject.transform.position));
				this.CreateMuzzleFlash(spriteFlashSmall, new Vector(20, 0).add(this.fireLocations[0].position));
				this.CreateMuzzleFlash(spriteFlashSmall, new Vector(20, 0).add(this.fireLocations[1].position));
			}
		}

		//Roll Animation
		if (Keyboard.GetKey(Key.W)){
			this.curFrame = Math.min(this.curFrame + Time.deltaTime * this.animSpeed, 4);
		}
		else if (Keyboard.GetKey(Key.S)){
			this.curFrame = Math.max(this.curFrame - Time.deltaTime * this.animSpeed, 0);
		}
		else{
			this.curFrame = MathX.Lerp(this.curFrame, 2, Time.deltaTime * this.animSpeed);
		}
		if (this.spriteRenderer) {
			this.spriteRenderer.sprite = this.animFrames[Math.round(this.curFrame)];
		}

		if(this.fireRate > 0)
			this.fireRate -= Time.deltaTime;
	}

	CreateMuzzleFlash(sprite: Sprite, position: Vector){
		const flash = new GameObject("Flash");
		let spriteRenderer = flash.AddComponent(SpriteRenderer);
		spriteRenderer.scale = new Vector(3, 3);
		spriteRenderer.sprite = sprite;
		flash.transform.position = position;//this.gameObject.transform.position.add(offset);
		flash.Destroy(0.01);
	}

	FireBullet(position: Vector, angle: number = 0, gravity: number = 0){
		const bullet = new GameObject("Bullet");
		bullet.transform.position = position;//this.gameObject.transform.position.add(offset);

		let spriteRenderer = bullet.AddComponent(SpriteRenderer);
		spriteRenderer.scale = new Vector(3, 3);

		let sprite = new Sprite("images/testsheet.png");
		//sprite.drawScaleX = 3;
		//sprite.drawScaleY = 3;

		// sprite.width = 29;
		// sprite.height = 64;
		// sprite.sourceX = 193;//(Math.random() > 0.5) ? 71 : 98;
		// sprite.sourceY = 181;

		sprite.width = 16;
    sprite.height = 16;
    sprite.sourceX = 375;
    sprite.sourceY = 113;
		
		// sprite.width = 54;
		// sprite.height = 10;
		// sprite.sourceX = 142;
		// sprite.sourceY = 163;

		//sprite.updateSpriteInfo();

		spriteRenderer.sprite = sprite;
		//(spriteRenderer.colorizer as any).color = "#0F0";
		//(spriteRenderer.colorizer as any).operation = "multiply";
		//(spriteRenderer.colorizer as any).color = "#0F0";
		//(spriteRenderer.colorizer as any).operation = "color-dodge";

		let col = bullet.AddComponent(BoxCollider2D);
		col.Width = sprite.width * spriteRenderer.scale.x;
		col.Height = sprite.height * spriteRenderer.scale.y;
		//col.bodyType = 0;
		//col.enableGravity = false;
		//Physics2DUtil.AddBodyToWorld(col);

		let rigid = bullet.AddComponent(Rigidbody2D);
		rigid.enableGravity = false;
		bullet.layer = 1 << 1;
		
		// let body = col.body as Box2D.Dynamics.b2Body;
    // let fixture = body.GetFixtureList();
    // let filter = fixture.GetFilterData();
    // filter.categoryBits = LayerMask.LASER;
		// filter.maskBits = LayerMask.ALL & ~LayerMask.LASER;
		// //filter.groupIndex
		// fixture.SetFilterData(filter);
		
		let laser = bullet.AddComponent(Laser);
		laser.moveSpeed = this.bulletSpeed;
		laser.moveDirection = laser.moveDirection.Rotate2D(angle * MathX.DEG2RAD);
		laser.gravity = gravity;

		// for(let i = 0; i < 100; i++){
		// 	const bullet2 = bullet.Clone();
		// 	bullet2.transform.position = this.gameObject.transform.position.add(new Vector(25, MathX.RandomRange(-50, 50)));
		// }

	}

	FireWave(offset: Vector, angle: number = 0) {
		const bullet = new GameObject("Wave");
		bullet.transform.position = this.gameObject.transform.position.add(offset);

		let spriteRenderer = bullet.AddComponent(SpriteRenderer);
		spriteRenderer.scale = new Vector(3, 3);

		let sprite = new Sprite("images/testsheet.png");
		//sprite.drawScaleX = 3;
		//sprite.drawScaleY = 3;

		// sprite.width = 22;
		// sprite.height = 64;
		// sprite.sourceX = 71;//(Math.random() > 0.5) ? 71 : 98;
		// sprite.sourceY = 181;

		sprite.width = 14;
		sprite.height = 26;
		sprite.sourceX = 33;
		sprite.sourceY = 200;

		//sprite.updateSpriteInfo();

		spriteRenderer.sprite = sprite;
		//(spriteRenderer.colorizer as any).color = "#0F0";
		//(spriteRenderer.colorizer as any).operation = "multiply";
		//(spriteRenderer.colorizer as any).color = "#0F0";
		//(spriteRenderer.colorizer as any).operation = "color-dodge";

		let col = bullet.AddComponent(BoxCollider2D);
		col.Width = sprite.width * spriteRenderer.scale.x;
		col.Height = sprite.height * spriteRenderer.scale.y;

		//col.bodyType = 0;
		//col.enableGravity = false;
		//Physics2DUtil.AddBodyToWorld(col, LayerMask.LASER, LayerMask.ALL & ~LayerMask.LASER);

		let rigid = bullet.AddComponent(Rigidbody2D);
		rigid.enableGravity = false;

		bullet.layer = 1 << 1;

		// let body = col.body as Box2D.Dynamics.b2Body;
		// let fixture = body.GetFixtureList();
		// let filter = fixture.GetFilterData()
		// filter.categoryBits = LayerMask.LASER;
		// filter.maskBits = LayerMask.ALL & ~LayerMask.LASER;
		// fixture.SetFilterData(filter);

		let laser = bullet.AddComponent(Laser);
		laser.moveSpeed = this.waveSpeed;
		laser.moveDirection = laser.moveDirection.Rotate2D(angle * MathX.DEG2RAD);

		//laser.RemoveLaserCollision(col.body as any);

		// for(let i = 0; i < 100; i++){
		// 	const bullet2 = bullet.Clone();
		// 	bullet2.transform.position = this.gameObject.transform.position.add(new Vector(25, MathX.RandomRange(-50, 50)));
		// }

		//bullet.Destroy(10);
	}

	GetAnimationFrames(){
		for(let i = 0; i < 5; i++){
			/*
			let animFrame = new Sprite(`images/Ship${i}.png`, function(this: HTMLImageElement, ev: Event){
				animFrame.width = this.width;
				animFrame.height = this.height;
				animFrame.sourceX = 0;
				animFrame.sourceY = 0;
			});
			*/
			let animFrame = new Sprite(`images/Ship${i}.png`);//Sprite.CreateImmediate(`images/Ship${i}.png`);
			animFrame.width  = animFrame.texture.width || 50;
			animFrame.height = animFrame.texture.height || 25;
			this.animFrames[i] = animFrame;
		}
	}
}

enum LayerMask {
	DEFAULT = 0x0001,
	LASER = 0x0002,
	ALL = 0xFFFF
};
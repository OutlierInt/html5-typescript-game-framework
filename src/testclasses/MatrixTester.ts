import { Script } from "../Modules/GameObjects/Script";
import { Matrix4x4 } from "../Modules/Math/Matrix4x4";
import { Vector } from "../Modules/Math/Vector";
import { Debug } from "../Modules/System/Debug";
import { Keyboard } from "../Modules/Input/Keyboard";
import { Key } from "../Modules/Input/Keys";
import { canvas } from "../Modules/System/Canvas";

export class MatrixTester extends Script{
	tMat = Matrix4x4.Identity();
	rMat = Matrix4x4.Identity();
	sMat = Matrix4x4.Identity();
	transform = this.gameObject.transform;

	pos = this.transform.position;
	rot = this.transform.rotation;
	scl = this.transform.scale;

	Start(){
		this.CreateMatrixDOM(this.tMat, 150, 50);
		this.CreateMatrixDOM(this.rMat, 150 + 50 * 3, 50);
		this.CreateMatrixDOM(this.sMat, 150 + 50 * 6, 50);
	}

	CreateMatrixDOM(mat: Matrix4x4, top: number, left: number){
		const div = document.createElement("div");
		div.setAttribute("style", `position:absolute; top: ${top}px; left:${left}px;`);
		if (canvas.parentElement)
			canvas.parentElement.appendChild(div);

		for (let row = 0; row < 4; row++) {
			for (let col = 0; col < 4; col++) {
				const input = document.createElement("input");
				input.type = "number";
				input.value = String(mat.GetCellFromCoords(row, col));
				input.addEventListener("change", () => {
					mat.SetCellFromCoords(row, col, Number(input.value));
				});
				input.setAttribute("style", `position: absolute; width: 50px; height: 25px; top: ${row * 25}px; left: ${col * 50}px;`);
				div.appendChild(input);
			}
		}
	}

	Update(){
		//Translate
		const position = Matrix4x4.Multiply(this.tMat, this.pos) as Vector;
		Debug.print(position.toString(), new Vector(25, 50));
		this.transform.position = position;

		//Rotate
		const rotation = Matrix4x4.Multiply(this.rMat, this.rot) as Vector;
		Debug.print(rotation.toString(), new Vector(25, 75));
		this.transform.rotation = rotation;
		
		//Scale
		const scale = Matrix4x4.Multiply(this.sMat, this.scl) as Vector;
		Debug.print(scale.toString(), new Vector(25, 100));
		this.transform.scale = scale;

		// if(Input.GetKeyDown(Key.M)){
		// 	console.log(this.mat.toString());
		// }
	}

}
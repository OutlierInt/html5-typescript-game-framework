import { Script } from "../Modules/GameObjects/Script";
import { Keyboard } from "../Modules/Input/Keyboard";
import { Key } from "../Modules/Input/Keys";
import { Transform } from "../Modules/GameObjects/Transform";

export class QuickParent extends Script{
	parent: Transform | null = null;
	key: number = Key.P;

	Update(){
		if(Keyboard.GetKeyDown(this.key)){
			const thisParent = this.gameObject.transform.GetParent();
			const newParent = !thisParent ? this.parent : null;
			this.gameObject.transform.SetParent(newParent);
		}
	}
}
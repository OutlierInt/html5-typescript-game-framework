import { Script } from "../Modules/GameObjects/Script";
import { Vector } from "../Modules/Math/Vector";
import { Time } from "../Modules/System/Time";
import { MathX } from "../Modules/Math/MathX";
import { Collider2D } from "../Modules/Physics2D/Collider2D";
import { Physics2DUtil } from "../Modules/Physics2D/Physics2DUtil";

export class Laser extends Script{
	moveSpeed = 500;
	gravity = 0;
	moveDirection = Vector.right;
	collider = this.gameObject.GetComponent(Collider2D);

	Start(){
		if (this.collider && this.collider.body){
			const body = this.collider.body;

			//Push the laser forward
			const physDirection = this.moveDirection.multiply(this.moveSpeed);
			const velocity = Physics2DUtil.pxVec2worldVec(physDirection);
			body.SetLinearVelocity(velocity);
		}

		//Destroy this laser after 3 seconds.
		this.gameObject.Destroy(3);
	}

	FixedUpdate(){
		if (this.collider && this.collider.body) {
			const body = this.collider.body;

			if(this.gravity != 0){
				const physDirection = Vector.down.multiply(this.gravity);
				const gravityForce = Physics2DUtil.pxVec2worldVec(physDirection);
				body.ApplyForce(gravityForce, body.GetWorldCenter());
			}
		}
	}

	Update(){
		if (this.gameObject.transform.position.x > 2000 || this.gameObject.transform.position.x < -1000 ||
			this.gameObject.transform.position.y > 1000 || this.gameObject.transform.position.y < -1000) {
			this.gameObject.Destroy();
    }
	}

	OnCollisionStart(){
		this.gameObject.Destroy();
	}
}

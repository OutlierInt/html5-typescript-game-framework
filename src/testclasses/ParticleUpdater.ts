import { Script } from "../Modules/GameObjects/Script";
import { ParticleSystem, Particle } from "../Modules/Render2D/ParticleRenderer";
import { MathX } from "../Modules/Math/MathX";
import { Keyboard } from "../Modules/Input/Keyboard";
import { Key } from "../Modules/Input/Keys";
import { canvas } from "../Modules/System/Canvas";
import { Debug } from "../Modules/System/Debug";
import { Vector } from "../Modules/Math/Vector";
import { Time } from "../Modules/System/Time";

export class ParticleUpdater extends Script{
	particleSystem: ParticleSystem | null = this.particleSystem;

	Start(){
		this.particleSystem = this.gameObject.GetComponent(ParticleSystem);
		if(this.particleSystem)
			this.particleSystem.imageSmoothingEnabled = true;
		//console.log(`ParticleUpdater of [GameObject "${this.gameObject.name}"] initialized!`);
		//console.log(`Particle System ${this.particleSystem != null ? "Was" : "Not"} Found!`);
	}

	Update(){
		if(!this.particleSystem) return;

		if (Keyboard.GetKey(Key.Q)) {
			let limit = 5;
			for (let i = 0; i < limit; i++){
				const particle = new Particle();
				this.ResetParticle(particle)
				this.particleSystem.particles.push(particle);
			}
		}

		if (Keyboard.GetKey(Key.E)) {
			if(Keyboard.GetKey(Key.Shift))
				this.particleSystem.particles.length = 0;
			else{
				for (let i = 0; i < 10; i++) this.particleSystem.particles.pop();				
			}
		}

		let pLength = this.particleSystem.particles.length;
		const dt = Time.deltaTime;

		for (let i = 0; i < pLength; i++) {
			const p = this.particleSystem.particles[i];
			if(!p) continue;
			if(p.lifetime <= 0){
				this.ResetParticle(p);
			}

			p.lifetime -= dt;
			p.x += dt * p.velocityX;
			p.y += dt * p.velocityY;
			p.scaleX += dt * MathX.RandomRange(5,15);
			p.scaleY += dt * MathX.RandomRange(5,15);
			//p.velocityY += dt * 2500;
			p.angle += dt * p.angularVelocity;
			p.opacity = 2 * p.lifetime / p.lifetimeMax;
		}
	}

	ResetParticle(particle: Particle){
		particle.x = 0;
		particle.y = 0;
		particle.angle = 0;
		particle.scaleX = 1;
		particle.scaleY = 1;
		particle.velocityX = MathX.RandomRange(150, 2000);
		particle.velocityY = MathX.RandomRange(0, -0);
		particle.angularVelocity = MathX.RandomRange(-10, 10);
		particle.opacity = 1;
		particle.lifetime = particle.lifetimeMax = MathX.RandomRange(0.5, 1);
		return particle;
	}
}
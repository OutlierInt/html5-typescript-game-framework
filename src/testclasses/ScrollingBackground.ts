import { Script } from "../Modules/GameObjects/Script";
import { Vector } from "../Modules/Math/Vector";
import { Time } from "../Modules/System/Time";
import { Key } from "../Modules/Input/Keys";
import { Keyboard } from "../Modules/Input/Keyboard";
import { Renderer } from "../Modules/Render2D/Renderer";

export class ScrollingBackground extends Script{
	transform = this.gameObject.transform;
	distance = 512;
	speed = 500;
	renderer = this.gameObject.GetComponent(Renderer);

	Update(){
		if(!this.renderer) return;
		
		const pos = this.transform.position;
		const newX = (pos.x - this.speed * Time.deltaTime) % this.distance;
		//this.transform.position = new Vector(newX, pos.y); 
		this.transform.position.Set(newX, pos.y);

		if (Keyboard.GetKeyDown(Key.Numpad0)) this.renderer.isEnabled ? this.renderer.Disable() : this.renderer.Enable();
	}
}
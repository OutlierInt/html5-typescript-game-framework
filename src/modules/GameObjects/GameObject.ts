import { Transform } from "./Transform";
import { BaseObject, BaseObjects } from "./BaseObject";
import { Component } from "./Component";
import { EventObserver } from "../System/EventObserver";
import { IComponentConstructor } from "./IComponentConstructor";

export class GameObject extends BaseObject implements ITogglable {
	//Components
	components: Array<Component>;
	readonly transform: Transform;
	//Layers
	private internalLayer: number;
	_onLayerChange: EventObserver;
	//Togglable
	private _enabled: boolean;

	constructor(name: string = "") {
		super(name);
		this.components = [];
		this.internalLayer = 0x0001;
		this._onLayerChange = new EventObserver();
		this.transform = this.AddComponent(Transform);
		this._enabled = true;
	}

	//Identify Class by Name
	GetClassName() { return "GameObject"; }

	get layer() { return this.internalLayer; }
	set layer(cBits: number){
		this.internalLayer = cBits;
		this._onLayerChange.FireEvent();
	}

	Destroy(delay = 0)	{ GameObject.Destroy(this, delay); }
	DestroyImmediate() 	{ GameObject.DestroyImmediate(this); }
	//Clone() 						{ return GameObject.Clone(this); }

	AddComponent<T extends Component>(constructorFn: IComponentConstructor<T>){
		const component = new constructorFn(this, constructorFn.name);
		
		if(component._Initialize !== Component.prototype._Initialize){
			component._Initialize();
		}
		
		this.components.push(component);
		return component || null;
	}

	GetComponent<T extends Component>(constructorFn: IComponentConstructor<T>) {
		const comp = this.components.find(comp => comp instanceof constructorFn);
		return comp ? comp as T : null;
	}

	// RemoveComponent<T extends Component>(constructorFn: IComponentConstructor<T>) {
	// 	const compIndex = this.components.findIndex(comp => comp instanceof constructorFn);
	// 	const component = this.components[compIndex] as T;
	// 	this.components.splice(compIndex, 1);
	// 	component.Destroy();
	// }

	//Toggle
	get isEnabled() { return this._enabled; }

	Enable(){
		if(!this._enabled){
			this._enabled = true;
			this.transform.SendMessageThroughHierarchy("Enable");
		}
	}

	Disable(){
		if(this._enabled){
			this._enabled = false;
			this.transform.SendMessageThroughHierarchy("Disable");
		}
	}

	SendMessage(funcName: string){
		for (const component of this.components) {
			const func = (component as any)[funcName] as Function;
			if (typeof func === "function"){
				func.call(component);
			}
		}
	}

	// static Clone(obj: GameObject){
	// 	//Create a new GameObject and clone all its components.
	// 	//Except Transform, which is made when the GameObject is created.
	// 	const clone = new GameObject(obj.name, false); //super.Clone(obj) as GameObject;
	// 	const clonedComponents = obj.components.map(comp => comp.Clone([clone]));//comp.Clone([clone]));//(comp instanceof Transform) ? comp : comp.Clone([clone]));
	// 	if(clonedComponents) clone.components = clonedComponents;
	// 	return clone;
	// }

	static Destroy(obj: GameObject, delay: number = 0){
		super.Destroy(obj, delay);
		for (const component of obj.components) {
			component.Destroy(delay);
			//super.Destroy(component, delay);
		}
	}

	static DestroyImmediate(obj: GameObject){
		super.DestroyImmediate(obj);
		for (const component of obj.components) {
			component.DestroyImmediate();
			//super.DestroyImmediate(component);
		}
		obj.components.length = 0;
	}

	static getGameObjectCount() {
		let count = 0;
		BaseObjects.forEach(obj => { if (obj instanceof GameObject) count++ });
		return count;
	}
}

const DirtyObjects = new Set<GameObject>();

export function MarkObjectAsDirty(gObj: GameObject){
	DirtyObjects.add(gObj);
}

export function CleanObjectComponents(){
	if(DirtyObjects.size <= 0) return;

	for (const gObj of DirtyObjects) {
		if(BaseObjects.has(gObj))
			gObj.components = gObj.components.filter(comp => comp != null);	
	}
	
	DirtyObjects.clear();
}

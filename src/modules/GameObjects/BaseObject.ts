import { IClonable } from "./IClonable";
import { Time } from "../System/Time";
import { IClassIdentifiable } from "./IClassIdentifiable";
import { Keyboard } from "../Input/Keyboard";
import { Key } from "../Input/Keys";
import { Vector } from "../Math/Vector";

export let BaseObjects: Set<BaseObject> = new Set();
const DelayedDestroyQueue: Map<BaseObject, number> = new Map;
const DestroySet: Set<BaseObject> = new Set();
const DeletionQueue: BaseObject[] = [];

export class BaseObject implements IClassIdentifiable{// implements IClonable{
	name: string;

	constructor(name: string = ""){
		this.name = name;
		BaseObject.AddToScene(this);
	}

	//Identify Class by Name
	GetClassName() { return "BaseObject"; }

	toString()								{ return this.name; }
	Destroy(delay: number = 0){ BaseObject.Destroy(this, delay); }
	BeforeDestroyImmediate() 	{ BaseObject.BeforeDestroyImmediate(); }
	DestroyImmediate()				{ BaseObject.DestroyImmediate(this); }
	//Clone(args: Array<any> = []) 		{ return BaseObject.Clone(this, args); }

	static Find<T extends BaseObject>(name: string, type?: IBaseObjectConstructor<T>){
		for (const obj of BaseObjects.values()) {
			if (obj.name === name) {
				if (type && !(obj instanceof type)) continue;
				return obj as T;
			}
		}
		return null;
	}

	private static AddToScene(obj: BaseObject){
		if(!BaseObjects.has(obj)){
			BaseObjects.add(obj);
    }
	}

	static Destroy(obj: BaseObject, delay: number = 0){
		//If delayed, store this destruction call and deal with it later.
		if(delay > 0){
			DelayedDestroyQueue.set(obj, Time.time + delay);
			return;
		}

		if(!BaseObjects.has(obj)) return;

		if(!DestroySet.has(obj)){
			DestroySet.add(obj);
		}
	}

	static BeforeDestroyImmediate(){
		console.log("No Pre-Destroy Handler Method Found");
	}

	static DestroyImmediate(obj: any){
		//Check if this object still exists...
		if(!BaseObjects.has(obj)) return;

		//If this object is already in the DestroySet by now,
		//Destroy was already called on it and it has been processed.
		//This object is already destroyed...
		if(DestroySet.has(obj)) return;

		//If this object needs to do something before it's destroyed, do it.
		if(obj.BeforeDestroyImmediate && obj.BeforeDestroyImmediate !== this.BeforeDestroyImmediate)
			obj.BeforeDestroyImmediate();
		
		//Remove the object from the BaseObjects list
		BaseObjects.delete(obj);

		//Recursively search all BaseObjects and their properties.
		//If this object appears as a property, set it to null.
		deepReferenceClear_Set(obj, BaseObjects);
	}

	// static Clone(obj: any, args: Array<any> = []){
	// 	if (!obj) return null;
	// 	const clone = new (obj.constructor as any)(...args);
	// 	BaseObjectAssign(clone, obj);
	// 	return clone;
	// }
}

export interface IBaseObjectConstructor<T> {
  new (...args: any[]): T;
}

function filterNullsInSet(set: Set<any>, func: Function){
	const newSet = new Set();
	for (var v of set) if(func(v)) newSet.add(v);
	return newSet;
}

export function ProcessDestructionQueue(){
	ProcessDelayedDestructionCalls();

	if (DestroySet.size <= 0) return;

  //If this object needs to do something before it's destroyed, do it.
	for (const obj of DestroySet) {
    let hasBeforeDestroyHandler = !!obj.BeforeDestroyImmediate;
    let handlerIsUnique = obj.BeforeDestroyImmediate !== BaseObject.prototype.BeforeDestroyImmediate;

		if (hasBeforeDestroyHandler && handlerIsUnique)
			obj.BeforeDestroyImmediate();
	}
	
	//Remove the Destroyed objects from the BaseObjects list
	DestroySet.forEach(objToClear => BaseObjects.delete(objToClear));
}

export function CleanDestructionQueue() {
	if (DestroySet.size <= 0) return;

	//Deep Clear everything we can reach!
	deepReferenceClearWithSet_Set(BaseObjects);
  DestroySet.clear();
}

export function ClearDestroyQueue() {
  ProcessDelayedDestructionCalls();

  if (DestroySet.size <= 0) return;

  let time1 = performance.now();

	//If this object needs to do something before it's destroyed, do it.
	for (const obj of DestroySet) {
    let hasBeforeDestroyHandler = !!obj.BeforeDestroyImmediate;
    let handlerIsUnique = obj.BeforeDestroyImmediate !== BaseObject.prototype.BeforeDestroyImmediate;

		if (hasBeforeDestroyHandler && handlerIsUnique)
			obj.BeforeDestroyImmediate();
  }

	//Remove the Destroyed objects from the BaseObjects list
	DestroySet.forEach(objToClear => BaseObjects.delete(objToClear));

	//Deep Clear Everything we can reach!
	deepReferenceClearWithSet_Set(BaseObjects);

  DestroySet.clear();

  let time2 = performance.now();
  //console.log(`Time elapsed ${time2 - time1}`);
}

function ProcessDelayedDestructionCalls(){
	for (const pair of DelayedDestroyQueue.entries()) {
		const obj = pair[0];
		const delay = pair[1];

		const objExists = BaseObjects.has(obj);
		const objDelayFinished = Time.time > delay;

		if(!objExists){
			DelayedDestroyQueue.delete(obj);
		}
		if(objDelayFinished){
			obj.Destroy();
			DelayedDestroyQueue.delete(obj);
		}
	}
}

let garbageCollectionRate = 100;
let garbageCollectionInterval = 0;
function SlowerGarbageCollection(){
	if(DeletionQueue.length <= 0) return;
	//console.log(`Deletion Queue: ${DeletionQueue.length}`);
	let i = 0;
	while (DeletionQueue.length > 0 && i < garbageCollectionRate) {
    DeletionQueue.pop();
    i++;
  }
}

//Slow down the garbage collection
//const GarbageCollectorInterval = setInterval(SlowerGarbageCollection, garbageCollectionInterval);

function CleanArray(arr: Array<any>, deleteValue: any) {
	for (var i = 0; i < arr.length; i++) {
		if (arr[i] === deleteValue) {
			arr.splice(i, 1);
			i--;
		}
	}
	return arr;
};

interface AnyObject {
	[key: string]: any;
}

function clearAllProperties(obj: AnyObject){
	for (const key in obj) {
		if (obj.hasOwnProperty(key)) {
			obj[key] = undefined;
		}
	}
}

//Destroy Immediate functions
function deepReferenceClear(objToClear: object, obj: AnyObject, visited: Set<object>) {
	//Sanity check for null is object bug.
	if(obj === null) return;

	//Some object types should not be processed.
	if(ObjectTypeBlacklisted(obj, blacklist)) return;

	//Just in case...
	if(visited.has(obj)) return;

	//Earlier, when deleting a transform, it could find its gameobject and encounter a cycle. Double-check this.
	visited.add(obj);

	//If it's a Set, iterate and deep search its contents
	if (obj instanceof Set) {
		for (const item of obj.values()) {
			if (item === objToClear) obj.delete(item);
			if (typeof item === "object" && item) {
				if (!visited.has(item)) deepReferenceClear(objToClear, item, visited);
			}
		}
	}
	//If it's a Map, iterate and deep search its keys and values
	else if (obj instanceof Map) {
		for (const key of obj.keys()) {
			const val = obj.get(key);
			if (key === objToClear) obj.delete(key);
			if (val === objToClear) obj.set(key, null);
			if (typeof key === "object" && key) {
				if (!visited.has(key)) deepReferenceClear(objToClear, key, visited);
			}
			if (typeof val === "object" && val) {
				if (!visited.has(val)) deepReferenceClear(objToClear, val, visited);
			}
		}
	}
	//If it's an array, iterate and deep search through the array.
	if (obj instanceof Array) {
		for (let i = 0; i < obj.length; i++) {
			if (obj[i] === objToClear) obj[i] = null;
			if (typeof obj[i] === "object" && obj[i]){
				if(!visited.has(obj[i])) deepReferenceClear(objToClear, obj[i], visited);
			}
		}
	}
	//If it's an object
	else {
		for (const key in obj) {
			if (!obj.hasOwnProperty(key)) continue;
			if (obj[key] === objToClear) obj[key] = null;
			if (typeof obj[key] === "object" && obj[key]){
				if(!visited.has(obj[key])) deepReferenceClear(objToClear, obj[key], visited);
			}
		}
	}
}

function deepReferenceClear_Set(objToClear: object, set: Set<AnyObject>){
	//Deep Clear all reachable objects.
	const visited = new Set<object>();
	for (const obj of set) {
		deepReferenceClear(objToClear, obj, visited);
	}
}

//Destroy Functions
function deepReferenceClearWithSet(obj: AnyObject, visited: Set<object>) {
	//Sanity check for null is object bug.
	if(obj === null) return;

	//Some object types should not be processed.
	if(ObjectTypeBlacklisted(obj, blacklist)){ visited.add(obj); return;}
	
	//Just in case...
	if(visited.has(obj)) return;

	//Earlier, when deleting a transform, it could find its gameobject and encounter a cycle. Double-check this.
  visited.add(obj);

  //If it's a Set, iterate and deep search its contents
  if (obj instanceof Set) {
    for (const item of obj.values()) {
      if (DestroySet.has(item)) obj.delete(item);
      if (typeof item === "object" && item) {
        if (!visited.has(item)) deepReferenceClearWithSet(item, visited);
      }
    }
  }
  //If it's a Map, iterate and deep search its keys and values
  else if (obj instanceof Map) {
    for (const key of obj.keys()) {
      const val = obj.get(key);
      if (DestroySet.has(key)) obj.delete(key);
      if (DestroySet.has(val)) obj.set(key, null);
      if (typeof key === "object" && key) {
        if (!visited.has(key)) deepReferenceClearWithSet(key, visited);
      }
      if (typeof val === "object" && val) {
        if (!visited.has(val)) deepReferenceClearWithSet(val, visited);
      }
    }
  }
  //If it's an array, iterate and deep search through the array.
  else if (obj instanceof Array) {
    for (let i = 0; i < obj.length; i++) {
      if (DestroySet.has(obj[i])) obj[i] = null;
      if (typeof obj[i] === "object" && obj[i]) {
        if (!visited.has(obj[i])) deepReferenceClearWithSet(obj[i], visited);
      }
    }
  }
  //If it's an object
  else {
    for (const key in obj) {
      if (!obj.hasOwnProperty(key)) continue;
      if (DestroySet.has(obj[key])) obj[key] = null;
      if (typeof obj[key] === "object" && obj[key]) {
        if (!visited.has(obj[key]))
          deepReferenceClearWithSet(obj[key], visited);
      }
    }
  }
}

function deepReferenceClearWithSet_Set(set: Set<AnyObject>) {
	//Deep Clear all reachable objects.
	const visited = new Set<object>();
	for (const obj of set) { deepReferenceClearWithSet(obj, visited) };

	//Download the visited list
	//DownloadVisitedList(visited);
}

const blacklist = [
	Box2D.Dynamics.b2Body,
	Box2D.Dynamics.b2Fixture,
	Box2D.Dynamics.b2World,
	Vector
];

function DownloadVisitedList(visited: Set<object>) {
	if (Keyboard.GetKey(Key.Shift)) {
		const data = Array.from(visited).filter(obj => !ObjectTypeBlacklisted(obj, blacklist));

		let element = null;
		let textFile = null;
		try{
			const datastr = StringifyCircularObject(data);
			const datatext = new Blob([datastr], { type: "application/json" });
			textFile = window.URL.createObjectURL(datatext);
			
			element = document.createElement('a');
			element.setAttribute("href", textFile); //"data:text/plain;charset=utf-8," + encodeURIComponent(datastr));
			element.setAttribute('download', "Visited.txt");
			element.style.display = 'none';
			document.body.appendChild(element);
			
			if (Keyboard.GetKey(Key.Alt))
				element.click();
		}
		catch(e){
			console.error(e.message, e);
		}
		finally{
			if(textFile) window.URL.revokeObjectURL(textFile);
			if(element) document.body.removeChild(element);
		}
		
	}
}

function ObjectTypeBlacklisted(obj: object, blacklist: any[]){
	return blacklist.find(forbiddenType => obj instanceof forbiddenType);
}

function StringifyCircularObject(obj: object){
	const cache = new Set<any>();
	return JSON.stringify(obj, function (key, value) {
		if (typeof value === 'object' && value !== null) {
			if (cache.has(value)) {
				// Circular reference found, discard key
				return undefined;
			}
			// Store value in our collection
			cache.add(value);
		}
		return value;
	}, "\t");
}

//Experimental Destroy
function shallowReferenceClearWithSet(obj: AnyObject){
	//If it's a Set, iterate and deep search its contents
	if (obj instanceof Set) {
		for (const item of obj.values()) {
			if (DestroySet.has(item)) obj.delete(item);
		}
	}
	//If it's a Map, iterate and deep search its keys and values
	else if (obj instanceof Map) {
		for (const key of obj.keys()) {
			const val = obj.get(key);
			if (DestroySet.has(key)) obj.delete(key);
			if (DestroySet.has(val)) obj.set(key, null);
		}
	}
	//If it's an array, iterate and deep search through the array.
	else if (obj instanceof Array) {
		for (let i = 0; i < obj.length; i++) {
			if (DestroySet.has(obj[i])) obj[i] = null;
		}
	}
	//If it's an object
	else {
		for (const key in obj) {
			if (!obj.hasOwnProperty(key)) continue;
			if (DestroySet.has(obj[key])) obj[key] = null;
		}
	}
}

function shallowReferenceClearWithSet_Set(set: Set<AnyObject>) {
  //Remove objects from set
  DestroySet.forEach(objToClear => set.delete(objToClear));

  //Deep Clear all reachable objects.
  const visited = new Set<object>();
  for (const obj of set) {
    deepReferenceClearWithSet(obj, visited);
  }
}

//Utilities
function BaseObjectAssign(clone: any, source: any) {
	//Replace this with a BaseObject specific implementation... or don't.
	
	//Object assign is not needed. The constructor will handle the clone's properties.
	//Object.assign(clone, source);
	
	//When an object is created, clone the source's "pass-by-value" properties.
  clonePassByValueTypes(clone, source);
}

function clonePassByValueTypes(target: any, source: any) {
	for (const key in target) {
		if (target.hasOwnProperty(key)) {
			const element = target[key];
			if (element && element.PassByValue === true && typeof element.Clone === "function") {
				target[key] = source[key].Clone();
			}
		}
	}
}
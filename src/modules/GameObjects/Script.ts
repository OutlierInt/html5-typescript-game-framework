import { Component } from "./Component";
import { GameObject } from "./GameObject";
import { Collider2D } from "../Physics2D/Collider2D";

const StartQueue:				Map<{(): void}, Script> = new Map();
const FixedUpdateQueue:	Map<{(): void}, Script> = new Map();
const UpdateQueue:			Map<{(): void}, Script> = new Map();
const LateUpdateQueue:	Map<{(): void}, Script> = new Map();

export class Script extends Component implements IScriptUpdatable, ITogglable{
	private _enabled: boolean;
	collidersSubscribed: Set<Collider2D>;

	constructor(gObj: GameObject, name: string = ""){
		super(gObj, name);
		this.collidersSubscribed = new Set();
		this._enabled = true;
	}

	//Identify Class by Name
	GetClassName() { return "Script"; }

	_Initialize(){
		Script.AddScriptToQueues(this);
	}

	static AddScriptToQueues(script: Script){
		Script.AddScriptHandler(script, "Start", StartQueue);
		Script.AddScriptHandler(script, "FixedUpdate", FixedUpdateQueue);
		Script.AddScriptHandler(script, "Update", UpdateQueue);
		Script.AddScriptHandler(script, "LateUpdate", LateUpdateQueue);
		Script.SubscribeToColliders(script);
	}

	static RemoveScriptFromQueues(script: Script){
		Script.RemoveScriptHandler(script, StartQueue);
		Script.RemoveScriptHandler(script, UpdateQueue);
		Script.RemoveScriptHandler(script, LateUpdateQueue);
		Script.RemoveScriptHandler(script, FixedUpdateQueue);
		Script.UnsubscribeToColliders(script);
	}

	private static RemoveScriptHandler(script: Script, Queue: Map<{ (): void }, Script>){
		for (const ScriptPair of Queue) {
			const boundScript = ScriptPair[0];
			const caller = ScriptPair[1];
			if (caller === script) {
				Queue.delete(boundScript);
			}
		}
	}

	private static AddScriptHandler(script: Script, funcName: string, Queue: Map<{ (): void }, Script>){
		///@ts-ignore
		const scriptFunction 		= script[funcName] as Function;
		///@ts-ignore
		const prototypeFunction = Script.prototype[funcName] as Function;
		if(typeof scriptFunction === "function" && scriptFunction !== prototypeFunction){
			const boundFunction = scriptFunction.bind(script);
      Queue.set(boundFunction, script);
    }
	} 

	//Toggle
	get isEnabled() { return this._enabled; }

	Enable() {
		if (!this._enabled) {
			this._enabled = true;
			Script.AddScriptToQueues(this);
		}
	}

	Disable() {
		if (this._enabled) {
			this._enabled = false;
			Script.RemoveScriptFromQueues(this);
		}
	}

	BeforeDestroyImmediate() {
		super.BeforeDestroyImmediate();
		Script.RemoveScriptFromQueues(this);
	}

	Start()				{ console.log("No Start Method Found"); }
	Update()			{ console.log("No Update Method Found"); }
	FixedUpdate() { console.log("No Fixed Update Method Found"); }
	LateUpdate()	{ console.log("No Late Update Method Found"); }

	OnCollisionStart() 	{ console.log("No Collision Start Handler"); }
	OnCollisionStay() 	{ console.log("No Collision Stay Handler"); }
	OnCollisionEnd() 		{ console.log("No Collision End Handler"); }

	OnSensorStart() { console.log("No Sensor Start Handler"); }
	OnSensorStay() 	{ console.log("No Sensor Stay Handler"); }
	OnSensorEnd() 	{ console.log("No Sensor End Handler"); }

	static SubscribeToColliders(script: Script){
		for (const component of script.gameObject.components) {
			if (component instanceof Collider2D) {
				Script.AddScriptHandler(script, "OnCollisionStart", component.OnCollisionStartQueue as any);
				Script.AddScriptHandler(script, "OnCollisionStay", component.OnCollisionStayQueue as any);
				Script.AddScriptHandler(script, "OnCollisionEnd", component.OnCollisionEndQueue as any);
				Script.AddScriptHandler(script, "OnSensorStart", component.OnSensorStartQueue as any);
				Script.AddScriptHandler(script, "OnSensorStay", component.OnSensorStayQueue as any);
				Script.AddScriptHandler(script, "OnSensorEnd", component.OnSensorEndQueue as any);
				script.collidersSubscribed.add(component);
			}
		}
	}

	static UnsubscribeToColliders(script: Script){
		for (const component of script.gameObject.components) {
			if (component instanceof Collider2D) {
				Script.RemoveScriptHandler(script, component.OnCollisionStartQueue as any);
				Script.RemoveScriptHandler(script, component.OnCollisionStayQueue as any);
				Script.RemoveScriptHandler(script, component.OnCollisionEndQueue as any);
				Script.RemoveScriptHandler(script, component.OnSensorStartQueue as any);
				Script.RemoveScriptHandler(script, component.OnSensorStayQueue as any);
				Script.RemoveScriptHandler(script, component.OnSensorEndQueue as any);
				script.collidersSubscribed.delete(component);
			}
		}
	}
}

interface IScriptUpdatable{
	Start:				{ (): void } | null;
	FixedUpdate:	{ (): void } | null;
	Update: 			{ (): void } | null;
	LateUpdate: 	{ (): void } | null;
}

export function HandleStarts()				{ HandleUpdateQueue(StartQueue); StartQueue.clear(); }
export function HandleFixedUpdates() 	{ HandleUpdateQueue(FixedUpdateQueue); }
export function HandleUpdates() 			{ HandleUpdateQueue(UpdateQueue); }
export function HandleLateUpdates() 	{ HandleUpdateQueue(LateUpdateQueue); }

function HandleUpdateQueue(queue: Map<{ (): void }, Script>) {
  if (queue.size <= 0) return;
  for (const updateFunction of queue.keys()) { updateFunction(); }
}
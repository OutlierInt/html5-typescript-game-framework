interface ITogglable{
	readonly isEnabled: boolean;
	Enable(): void;
	Disable(): void;
}
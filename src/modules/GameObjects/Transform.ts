import { Vector } from "../Math/Vector";
import { Component } from "./Component";
import { GameObject } from "./GameObject";
import { EventObserver } from "../System/EventObserver";

export class Transform extends Component{
	private _position: Vector;
	private _rotation: Vector;
	private _scale: Vector;
	
	//Hierarchy
	private parent: Transform | null;
	private children: Set<Transform>;
	
	private _localPosition: Vector;
	private _localRotation: Vector;
	private _localScale: Vector;

	//Update things when Transform changes.
	_onTransformUpdate: EventObserver;
	//For more specific changes
	_onPositionUpdate: EventObserver;
	_onRotationUpdate: EventObserver;
	_onScaleUpdate: EventObserver;


	constructor(gObj: GameObject, name: string = ""){
		super(gObj, name);
		this._position 	= Vector.zero;
		this._rotation 	= Vector.zero;
		this._scale 		= Vector.one;
		this._localPosition = Vector.zero;
		this._localRotation = Vector.zero;
		this._localScale 		= Vector.one;
		this._onTransformUpdate = new EventObserver();
		this._onPositionUpdate = new EventObserver();
		this._onRotationUpdate = new EventObserver();
		this._onScaleUpdate = new EventObserver();
		this.parent = null;
		this.children = new Set();
	}

	//Identify Class by Name
	GetClassName() { return "Transform"; }

	get position() 			{ return this._position; }
	get rotation() 			{ return this._rotation; }
	get scale() 				{ return this._scale; }
	get getAngle2d() 			{ return this._rotation.z; }

	//When changing the transform, perform an Update to any relevant physics bodies.
	set position(p: Vector){
		this._position = p;
		this.UpdateChildren();
		this._onPositionUpdate.FireEvent();
		this._onTransformUpdate.FireEvent();
	}
	set rotation(r: Vector){
		this._rotation = r;
		this.UpdateChildren();
		this._onRotationUpdate.FireEvent();
		this._onTransformUpdate.FireEvent();
	}
	set scale(s: Vector){
		this._scale = s;
		this.UpdateChildren();
		this._onScaleUpdate.FireEvent();
		this._onTransformUpdate.FireEvent();
	}

	Reset(){
		this.position = Vector.zero;
    this.rotation = Vector.zero;
    this.scale = Vector.one;
	}

	Destroy(delay: number = 0) {
		super.Destroy(delay);
		for (const child of this.children) {
			child.gameObject.Destroy(delay);
		}		
		//Transforms are basically inseperable from their game object.
		//But this leads to an infinite Destroy() loop: GObj -> Transform -> GObj
		//this.gameObject.Destroy(delay);
	}

	DestroyImmediate() {
		super.DestroyImmediate();
    for (const child of this.children) {
			child.gameObject.DestroyImmediate();
		}
		this.children.clear();
		//Transforms are basically inseperable from their game object.
		//But this leads to an infinite Destroy() loop: GObj -> Transform -> GObj
		//this.gameObject.DestroyImmediate();
	}

	//Hierarchy
	GetChildren(){ return this.children.values(); }

	GetParent(){
		return this.parent;
	}

	SetParent(parent: Transform | null){
		if(parent)
			Transform.CreateParentChildRelationship(parent, this);
		else
			Transform.RemoveParentChildRelationship(this.parent, this);
	}

	SendMessageThroughHierarchy(message: string){
		const childQueue: Transform[] = [this];
		do {
			//Get the current child
			const currentChild = childQueue.pop() as Transform;
			currentChild.gameObject.SendMessage(message);

			//Populate queue with your children
			for (const child of currentChild.children.values()) {
				childQueue.unshift(child);
			}
		} while(childQueue.length > 0);
	}

	static GetChildGameObjectByPath(obj: GameObject, path: string) {
		//Split the accessor string. e.g:"transform.position.x" => ["transform", "position", "x"]
		//If it starts with a special character it is a component. "SpriteRenderer.colorizer.color" : "#FFF"
		const children = path.split("/");

		let i = 0;
		let currentChild = obj.transform;

		nameSearch:
		do {
			const childName = children[i++];
			for (const child of currentChild.GetChildren()) {
				if (child.gameObject.name === childName) {
					currentChild = child;
					continue nameSearch;
				}
			}
			return null;
		}
		while (i < children.length)

		return currentChild.gameObject;
	}

	GetChildGameObject(path: string){
		return Transform.GetChildGameObjectByPath(this.gameObject, path);
	}

	private static CreateParentChildRelationship(parent: Transform, child: Transform){
		parent.children.add(child);
		child.parent = parent;
		child.WorldToLocalTransform(parent);
	}

	private static RemoveParentChildRelationship(parent: Transform | null, child: Transform) {
		if(parent) parent.children.delete(child);
		child.parent = null;
		child.ResetLocalTransform();
	}

	//Child Positioning
	get localPosition() { return this.parent ? this._localPosition 	: this.position; }
	get localRotation() { return this.parent ? this._localRotation 	: this.rotation; }
	get localScale() 		{ return this.parent ? this._localScale 		: this.scale; }
	
	set localPosition(p: Vector) {
		this._localPosition = p;
		if (this.parent) this.parent.LocalToWorldTransform(this);
		else this.position = p;
	}
	set localRotation(r: Vector) {
		this._localRotation = r;
		if (this.parent) this.parent.LocalToWorldTransform(this);
    else this.rotation = r;
	}
	set localScale(s: Vector) {
		this._localScale = s;
		if (this.parent) this.parent.LocalToWorldTransform(this);
    else this.scale = s;
	}

	private UpdateChildren(){
		for (const child of this.children) {
			this.LocalToWorldTransform(child);
		}
	}

	//More Specific
	//private LocalToWorldScale		(child: Transform) { child.scale 	 = this.scale.scale(child.localScale); }
	//private LocalToWorldRotation(child: Transform) { child.rotation = this.rotation.add(child.localRotation);	}
	//private LocalToWorldPosition(child: Transform) { child.position = this.position.add(child.localPosition); }

	//private WorldToLocalScale		(parent: Transform) { this._localScale 		= this.scale.invScale(parent.scale);}
	//private WorldToLocalRotation(parent: Transform) { this._localRotation = this.rotation.subtract(parent.rotation); }
	//private WorldToLocalPosition(parent: Transform) { this._localPosition = this.position.subtract(parent.position); }

	//Update Everything
	private LocalToWorldTransform(child: Transform){
		child.scale = this.scale.scale(child.localScale);
		child.rotation = this.rotation.add(child.localRotation);
		
		const offset = child
		.localPosition
		.Clone()
		.scaleVolatile(child.scale)
		.rotate2DVolatile(child.getAngle2d);
		child.position = this.position.add(offset);
	}

	private WorldToLocalTransform(parent: Transform){
    this._localScale 		= this.scale.invScale(parent.scale);
		this._localRotation = this.rotation.subtract(parent.rotation);
		this._localPosition = this.position.subtract(parent.position);
	}

	private ResetLocalTransform(){
		this._localPosition = Vector.zero;
		this._localRotation = Vector.zero;
		this._localScale 		= Vector.one;
	}
}

import { GameObject, MarkObjectAsDirty } from "./GameObject";
import { BaseObject } from "./BaseObject";

export class Component extends BaseObject{
	gameObject: GameObject;

	constructor(gObj: GameObject, name: string = ""){
		super(name);
		this.gameObject = gObj;
	}

	//Identify Class by Name
	GetClassName() { return "Component"; }

	_Initialize()	{ console.log("No Awake Method Found"); }

	BeforeDestroyImmediate(){
		MarkObjectAsDirty(this.gameObject);
	}

	/*
	RemoveComponent(){
		const compIndex = this.gameObject.components.indexOf(this);
		if(compIndex !== -1){
			const component = this.gameObject.components[compIndex];
			this.gameObject.components.splice(compIndex, 1);
			component.Destroy();
		}
	}
	*/
}

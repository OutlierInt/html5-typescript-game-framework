import { Vector } from "../Math/Vector";
import { Physics2D, box2d } from "./Physics2D";
import { Collider2D } from "./Collider2D";
import { Transform } from "../GameObjects/Transform";
import { ICollider2DData } from "./ICollider2DData";

export class Physics2DUtil {
	static CreateBodyDefinition(transform: Transform, bodyType: number) {
		let bodyDef = new box2d.b2BodyDef();
		bodyDef.type = bodyType;
		bodyDef.position = Physics2DUtil.pxVec2worldVec(transform.position);
		return bodyDef;
	}

	static SetBodyDefinition(bodyDef: Box2D.Dynamics.b2BodyDef, data?: BodyDefProperties) {
		if (data) {
			bodyDef.angle = data.angle || 0;
			bodyDef.bullet = data.bullet || false;
			bodyDef.fixedRotation = data.fixedRotation || false;
		} else {
			//Defaults
			bodyDef.angle = 0;
			bodyDef.bullet = false;
			bodyDef.fixedRotation = false;
		}
	}

	static SetupFixture(fixDef: Box2D.Dynamics.b2FixtureDef, data?: ICollider2DData) {
		if (data) {
			fixDef.friction = data.Friction || 0.5;
			fixDef.restitution = data.Restitution || 0.5;
		}
		//Default
		else {
			fixDef.friction = 0.5;
			fixDef.restitution = 0.5;
		}
	}

	static SetupFixtureDefinitionShape(fixDef: Box2D.Dynamics.b2FixtureDef, shape?: Box2D.Collision.Shapes.b2Shape | null, collider?: Collider2D) {
		if (shape) fixDef.shape = shape;
		if (collider) fixDef.isSensor = collider.IsSensor;
		fixDef.density = 1;
	}

	static CreateShape(collider: Collider2D) {
		let shape = collider.getColliderShape();
		if (!shape) throw new Error("Invalid Shape");
		return shape;
	}

	static SetupCollisionFilter(filter: Box2D.Dynamics.b2FilterData, categoryBits?: number, maskBits?: number, groupIndex?: number) {
		if (categoryBits) filter.categoryBits = categoryBits;
		if (maskBits) filter.maskBits = maskBits;
		if (groupIndex) filter.groupIndex = groupIndex;
	}

	static SetUserData(physObj: Box2D.Dynamics.b2Body | Box2D.Dynamics.b2Fixture, key: string, value: any) {
		const data = physObj.GetUserData() as any;
		if(data){
			if (data[key]) data[key] = value;
			physObj.SetUserData(data);
		}
		else{
			physObj.SetUserData({ [key]: value });
		}
	}

	static RemoveBodyFromWorld(body: Box2D.Dynamics.b2Body) {
		Physics2D.world.DestroyBody(body);
	}

	static px2world(x: number) {
		return x / Physics2D.SCALE;
	}

	static world2px(x: number) {
		return x * Physics2D.SCALE;
	}

	static pxVec2worldVec(v: Vector) {
		return new box2d.b2Vec2(this.px2world(v.x), this.px2world(v.y));
	}

	static worldVec2pxVec(v: Box2D.Common.Math.b2Vec2) {
		return new Vector(this.world2px(v.x), this.world2px(v.y));
	}

	static pxArray2worldArray(vecArray: Vector[]) {
		const vec2Array = [];
		for (const v of vecArray) {
			vec2Array.push(new box2d.b2Vec2(this.px2world(v.x), this.px2world(v.y)));
		}
		return vec2Array;
	}

	static UpdateGameTransforms(body: Box2D.Dynamics.b2Body, transform: Transform) {
		let position = body.GetPosition();
		let angle = body.GetAngle();
		transform.position = Physics2DUtil.worldVec2pxVec(position);
		transform.rotation = new Vector(0,0,angle);
	}
}

interface BodyDefProperties {
	angle?: number;
	bullet?: boolean;
	fixedRotation?: boolean;
}

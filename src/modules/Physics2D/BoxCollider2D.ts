import { Collider2D } from "./Collider2D";
import { GameObject } from "../GameObjects/GameObject";
import { Physics2DUtil } from "./Physics2DUtil";

export class BoxCollider2D extends Collider2D {
	private width: number;
	private height: number;

	constructor(gObj: GameObject, name: string = "") {
		super(gObj, name);
		this.width = 1;
		this.height = 1;
	}

	//Identify Class by Name
	GetClassName() { return "BoxCollider2D"; }

	getColliderShape(){
		let shape = new Box2D.Collision.Shapes.b2PolygonShape();
		shape.SetAsOrientedBox(
			Physics2DUtil.px2world(this.width / 2 || 1),
			Physics2DUtil.px2world(this.height / 2 || 1),
			Physics2DUtil.pxVec2worldVec(this.Offset),
			0
		);
    return shape;
	}

	get Width() { return this.width; }
	get Height(){ return this.height; }

	set Width(w: number){
		this.width = w;
		this.setSize();
	}

	set Height(h: number){
		this.height = h;
		this.setSize();
	}

	protected offsetHandler() {
		this.setSize();
	}

	private setSize(){
		let fixture = this.fixture as Box2D.Dynamics.b2Fixture;
		let shape = fixture.GetShape() as Box2D.Collision.Shapes.b2PolygonShape;
		shape.SetAsOrientedBox(
			Physics2DUtil.px2world(this.width / 2),
			Physics2DUtil.px2world(this.height / 2),
			Physics2DUtil.pxVec2worldVec(this.Offset),
			0
		);
	}
}
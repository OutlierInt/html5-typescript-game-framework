import { BoxCollider2D } from "./BoxCollider2D";
import { SpriteRenderer } from "../Render2D/SpriteRenderer";
import { GameObject } from "../GameObjects/GameObject";
import { Vector } from "../Math/Vector";
import { Sprite } from "../Render2D/Sprite";
import { SpriteTintInfo, SpriteTintType } from "../Render2D/SpriteTintInfo";
import { CircleCollider2D } from "./CircleCollider2D";

export class Physics2DPrimitive{
	static MakeStaticBox(x: number, y: number, w: number, h: number) {
		//A ground plane
		const box = new GameObject("Static Box");
		box.transform.position = new Vector(x, y);

		//Add a collider
		let boxCollider = box.AddComponent(BoxCollider2D);
		boxCollider.Width = w;
		boxCollider.Height = h;
		if (boxCollider.body) {
			boxCollider.body.SetType(Box2D.Dynamics.b2Body.b2_staticBody);
		}

		//Renderer
		let renderer = box.AddComponent(SpriteRenderer);
		renderer.sprite = new Sprite();
		renderer.scale = new Vector(w, h);
		(renderer.colorizer as SpriteTintInfo).color = "#0FF";
		(renderer.colorizer as SpriteTintInfo).tintType = SpriteTintType.Colorize;

		return box;
	}

	static MakeStaticBoxCircle(x: number, y: number, w: number, h: number) {
		//A ground plane
		const circle = new GameObject("Static Box");
		circle.transform.position = new Vector(x, y);

		//Add a collider
		let boxCollider = circle.AddComponent(BoxCollider2D);
		boxCollider.Width = w;
		boxCollider.Height = h;
		boxCollider.Offset = new Vector(0, 0);
		if (boxCollider.body) {
			boxCollider.body.SetType(Box2D.Dynamics.b2Body.b2_staticBody);
		}

		//Add a collider
		let circleCollider = circle.AddComponent(CircleCollider2D);
		circleCollider.Radius = Math.max(w, h) / 2;
		circleCollider.Offset = new Vector(0, 0);
		if (circleCollider.body) {
			circleCollider.body.SetType(Box2D.Dynamics.b2Body.b2_staticBody);
		}

		//Renderer
		let renderer = circle.AddComponent(SpriteRenderer);
		renderer.sprite = new Sprite();
		renderer.scale = new Vector(w, h);
		(renderer.colorizer as SpriteTintInfo).color = "#0FF";
		(renderer.colorizer as SpriteTintInfo).tintType = SpriteTintType.Colorize;

		return circle;
	}	
}


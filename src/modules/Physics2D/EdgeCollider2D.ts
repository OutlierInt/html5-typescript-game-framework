import { Collider2D } from "./Collider2D";
import { GameObject } from "../GameObjects/GameObject";
import { Vector } from "../Math/Vector";

export class EdgeCollider2D extends Collider2D {
	vertices: Vector[];	

	constructor(gObj: GameObject, name: string = "") {
		super(gObj, name);
		this.vertices = [];
	}

	//Identify Class by Name
	GetClassName() { return "EdgeCollider2D"; }

}
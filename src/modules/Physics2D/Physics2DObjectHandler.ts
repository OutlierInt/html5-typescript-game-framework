import { Rigidbody2D } from "./Rigidbody2D";
import { Collider2D } from "./Collider2D";
import { ICollider2DData } from "./ICollider2DData";
import { GameObject } from "../GameObjects/GameObject";
import { Physics2D } from "./Physics2D";
import { CollisionHandler } from "./ContactHandler";
import { Physics2DUtil } from "./Physics2DUtil";

export const PhysicsGameObjectMap = new Map<GameObject, Box2D.Dynamics.b2Body>();
export const ColliderIgnoreMap = new Map<Box2D.Dynamics.b2Fixture, Set<Box2D.Dynamics.b2Fixture>>();
const PhysicsGameObjectRemoveSet = new Set<GameObject>();

export class PhysicsObjectHandler {
	static AddRigidbodyToGameObject(rigidbody: Rigidbody2D) {
		const body = PhysicsGameObjectMap.get(rigidbody.gameObject);
		if (body) {
			PhysicsObjectHandler.AddRigidbody(rigidbody, body);
		}
		else {
			PhysicsObjectHandler.AddRigidbody_NoPhysBody(rigidbody);
		}
	}

	static AddColliderToGameObject(collider: Collider2D) {
		const body = PhysicsGameObjectMap.get(collider.gameObject);
		if (body) {
			PhysicsObjectHandler.AddCollider(collider, body);
		}
		else {
			PhysicsObjectHandler.AddCollider_NoPhysBody(collider);
		}
	}

	private static AddRigidbody_NoPhysBody(rigidbody: Rigidbody2D) {
		//Create a body definition
		const bodyDef = Physics2DUtil.CreateBodyDefinition(rigidbody.gameObject.transform, rigidbody.bodyType);
		Physics2DUtil.SetBodyDefinition(bodyDef);

		//Create a body from the definition. Give it the fixture.
		let body = Physics2D.world.CreateBody(bodyDef);

		//Add the collider for game interactions.
		Physics2DUtil.SetUserData(body, "rigidbody", rigidbody);
		rigidbody.body = body;

		//Track this new body
		PhysicsGameObjectMap.set(rigidbody.gameObject, body);

		return body;
	}

	private static AddCollider_NoPhysBody(collider: Collider2D) {
		//Create a body definition
		const bodyDef = Physics2DUtil.CreateBodyDefinition(collider.gameObject.transform, Box2D.Dynamics.b2Body.b2_kinematicBody);
		Physics2DUtil.SetBodyDefinition(bodyDef);

		//Create a Shape. Attach it to the fixture.
		let shape = collider.getColliderShape();		
		//let shape = Physics2DUtil.CreateShapeFromCollider(collider);

		//Create a fixture definition
		const fixDef = new Box2D.Dynamics.b2FixtureDef();
		Physics2DUtil.SetupFixture(fixDef);
		Physics2DUtil.SetupFixtureDefinitionShape(fixDef, shape, collider);

		//Collision Filtering
		Physics2DUtil.SetupCollisionFilter(fixDef.filter, 1, 0xFFFF);

		//Create a body from the definition. Give it the fixture.
		let body = Physics2D.world.CreateBody(bodyDef);
		let fixture = body.CreateFixture(fixDef);

		//Add the collider for game interactions.
		//Physics2DUtil.SetUserData(body, collider, undefined);
		Physics2DUtil.SetUserData(fixture, "collider", collider);
		collider.body = body;
		collider.fixture = fixture;

		//Track this new body
		PhysicsGameObjectMap.set(collider.gameObject, body);

		return body;
	}

	private static AddRigidbody(rigidbody: Rigidbody2D, body: Box2D.Dynamics.b2Body) {
		//Set mass here.
		//Enable Gravity here.
		body.SetFixedRotation(rigidbody.fixedRotation);
		body.SetType(rigidbody.bodyType);
		body.SetBullet(rigidbody.isBullet);

		Physics2DUtil.SetUserData(body, "rigidbody", rigidbody);
		rigidbody.body = body;
	}

	private static AddCollider(collider: Collider2D, body: Box2D.Dynamics.b2Body) {
		//Create a Shape. Attach it to the fixture.
		let shape = collider.getColliderShape();
		//let shape = Physics2DUtil.CreateShapeFromCollider(collider);

		//Create a fixture definition
		const fixDef = new Box2D.Dynamics.b2FixtureDef();
		Physics2DUtil.SetupFixture(fixDef, collider.physicsData as ICollider2DData);
		Physics2DUtil.SetupFixtureDefinitionShape(fixDef, shape, collider);

		//Collision Filtering
		Physics2DUtil.SetupCollisionFilter(fixDef.filter, 1, 0xFFFF);

		//Create the correct fixture.
		let fixture = body.CreateFixture(fixDef);

		//Add the collider for game interactions.
		Physics2DUtil.SetUserData(fixture, "collider", collider);

		//Track this new body
		PhysicsGameObjectMap.set(collider.gameObject, body);

		collider.body = body;
    collider.fixture = fixture;
	}

	static RemoveCollider(collider: Collider2D) {
		const body = collider.body as Box2D.Dynamics.b2Body;
		if (collider.fixture){
			//If this collider is ignoring any other colliders, clean this up.
			PhysicsObjectHandler._removeFixtureFromIgnoreMap(collider);

			//Clean up this collider object.
			collider.fixture.SetUserData(null);
			body.DestroyFixture(collider.fixture);
			collider.fixture = null;
		}

		if (!PhysicsObjectHandler.IsPhysicsObject(body))
			PhysicsGameObjectRemoveSet.add(collider.gameObject);
	}

	static RemoveRigidbody(rigidbody: Rigidbody2D) {
		const body = rigidbody.body as Box2D.Dynamics.b2Body;
		body.SetType(Box2D.Dynamics.b2Body.b2_staticBody);
		body.SetUserData(null);

		if (!PhysicsObjectHandler.IsPhysicsObject(body))
			PhysicsGameObjectRemoveSet.add(rigidbody.gameObject);
	}

	static IgnoreCollision(colliderA: Collider2D, colliderB: Collider2D, ignore: boolean) {
		const fixtureA = colliderA.fixture;
		const fixtureB = colliderB.fixture;
		if(!fixtureA || !fixtureB) return;

		if(ignore){
			PhysicsObjectHandler._addColliderIgnoreMapping(fixtureA, fixtureB);
			PhysicsObjectHandler._addColliderIgnoreMapping(fixtureB, fixtureA);
		}
		else{
			PhysicsObjectHandler._removeColliderIgnoreMapping(fixtureA, fixtureB);
			PhysicsObjectHandler._removeColliderIgnoreMapping(fixtureB, fixtureA);
		}
	}
	
	private static _addColliderIgnoreMapping(fixture: Box2D.Dynamics.b2Fixture, fixtureToIgnore: Box2D.Dynamics.b2Fixture) {
		if (!ColliderIgnoreMap.has(fixture))
			ColliderIgnoreMap.set(fixture, new Set([fixtureToIgnore]));
		else
			(ColliderIgnoreMap.get(fixture) as Set<object>).add(fixtureToIgnore);
	}
	
	private static _removeColliderIgnoreMapping(fixture: Box2D.Dynamics.b2Fixture, fixtureToCollideWith: Box2D.Dynamics.b2Fixture) {
		if (ColliderIgnoreMap.has(fixture)){
			const set = ColliderIgnoreMap.get(fixture) as Set<object>;
			set.delete(fixtureToCollideWith);
			if(set.size === 0) ColliderIgnoreMap.delete(fixture);
		}
	}
	
	private static _removeFixtureFromIgnoreMap(collider: Collider2D) {
		if(!collider.fixture) return;
		const fixtureToDelete = collider.fixture;

		//Remove it and its associations from the ignore map
		ColliderIgnoreMap.delete(fixtureToDelete);
		
		//Go through the other colliders. Remove it from their associations.
		ColliderIgnoreMap.forEach(colliderSet => { colliderSet.delete(fixtureToDelete); });

		//Clear any empty sets
		for(const key of ColliderIgnoreMap.keys()){
			if((ColliderIgnoreMap.get(key) as Set<object>).size === 0) ColliderIgnoreMap.delete(key);
		}
	}

	static IsPhysicsObject(body: Box2D.Dynamics.b2Body) {
		//Does the body have any user data?
		const hasUserData = body.GetUserData() != null;
		//Does it have any fixtures?
		const hasFixtures = body.GetFixtureList() != null;
		return hasUserData && hasFixtures;
		
		// const rigidbody = gObj.GetComponent(Rigidbody2D);
		// const collider = gObj.GetComponent(Collider2D);
		// return (!!rigidbody) || (!!collider);
	}

	static CleanPhysicsObjectsMap() {
		if(!Physics2D.world) return;
		if(PhysicsGameObjectRemoveSet.size <= 0) return;

		for (const obj of PhysicsGameObjectRemoveSet) {
			const body = PhysicsGameObjectMap.get(obj) as Box2D.Dynamics.b2Body;
			Physics2D.world.DestroyBody(body);
			PhysicsGameObjectMap.delete(obj);
		}
		PhysicsGameObjectRemoveSet.clear();
	}
}
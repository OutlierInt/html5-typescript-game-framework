export interface ICollider2DData {
  Restitution: number;
  Friction: number;
}
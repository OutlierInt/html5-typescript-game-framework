import { GameObject } from "../GameObjects/GameObject";
import { Physics2DUtil } from "./Physics2DUtil";
import { AssetManager } from "../System/AssetManager";
import { Collider2D } from "./Collider2D";
import { CircleCollider2D } from "./CircleCollider2D";
import { BoxCollider2D } from "./BoxCollider2D";

export class CollisionFilter{
	static NO_LAYERS	= 0x0000;
	static ALL_LAYERS = 0xFFFF;

	static LayerNames = [
		"Layer 1",
		"Layer 2",
		"Layer 3",
		"Layer 4",
		"Layer 5",
		"Layer 6",
		"Layer 7",
		"Layer 8",
		"Layer 9",
		"Layer 10",
		"Layer 11",
		"Layer 12",
		"Layer 13",
		"Layer 14",
		"Layer 15",
		"Layer 16",
	];

	static CollisionMatrix = new Map([
		[1 << 0, 	0xFFFF],
		[1 << 1, 	0xFFFF],
		[1 << 2, 	0xFFFF],
		[1 << 3, 	0xFFFF],
		[1 << 4, 	0xFFFF],
		[1 << 5, 	0xFFFF],
		[1 << 6, 	0xFFFF],
		[1 << 7, 	0xFFFF],
		[1 << 8, 	0xFFFF],
		[1 << 9, 	0xFFFF],
		[1 << 10, 0xFFFF],
		[1 << 11, 0xFFFF],
		[1 << 12, 0xFFFF],
		[1 << 13, 0xFFFF],
		[1 << 14, 0xFFFF],
		[1 << 15, 0xFFFF],
	]);

	static SetupCollisionMaskFromData(data: any){
		//Store the Collision Mask Map
		this.CollisionMatrix = new Map(data.CollisionMatrix);

		//Store the Layer Names Mask
		this.LayerNames = data.LayerNames;
	}

	static UpdatePhysicsBodyFromPosition(body: Box2D.Dynamics.b2Body, gameObject: GameObject){
		const pos = gameObject.transform.position;
    body.SetPosition(Physics2DUtil.pxVec2worldVec(pos));
	}

	static UpdatePhysicsBodyFromRotation(body: Box2D.Dynamics.b2Body, gameObject: GameObject) {
		const angle = gameObject.transform.getAngle2d;
    body.SetAngle(angle);	
	}

	static UpdatePhysicsBodyFromScale(body: Box2D.Dynamics.b2Body, gameObject: GameObject) {
		const scale = gameObject.transform.scale;

		let fixture = body.GetFixtureList()
		while(fixture){
			const shape = fixture.GetShape();

			//If the scale is negative, disable this body.
			const collider = fixture.GetUserData().collider as Collider2D;
			if(collider.isEnabled){
				const scaleIsPositive = Math.min(scale.x, scale.y) > 0;
				(collider.body as Box2D.Dynamics.b2Body).SetActive(scaleIsPositive)
			}
			
			if (shape instanceof Box2D.Collision.Shapes.b2CircleShape){
				const circleCol = collider as CircleCollider2D;
				//Circles cannot be turned into ellipsoids. Use the largest scale axis.
				shape.SetRadius( Physics2DUtil.px2world(circleCol.Radius * Math.max(scale.x, scale.y)) );
				//If offset is non-zero, its local position will be different.
				shape.SetLocalPosition( Physics2DUtil.pxVec2worldVec(circleCol.Offset.scale(scale)) );
			}
			else if (shape instanceof Box2D.Collision.Shapes.b2PolygonShape) {
				const polyCol = collider as BoxCollider2D;
				shape.SetAsOrientedBox(
					Physics2DUtil.px2world(polyCol.Width / 2 * scale.x),
					Physics2DUtil.px2world(polyCol.Height / 2 * scale.y),
					Physics2DUtil.pxVec2worldVec(polyCol.Offset.scale(scale)),
					0
				);
			}

			fixture = fixture.GetNext();
		}
	}

	static UpdatePhysicsBodyFromTransform(body: Box2D.Dynamics.b2Body, gameObject: GameObject) {
		CollisionFilter.UpdatePhysicsBodyFromPosition(body, gameObject);
		CollisionFilter.UpdatePhysicsBodyFromRotation(body, gameObject);
		CollisionFilter.UpdatePhysicsBodyFromScale(body, gameObject);	
	}

	static UpdatePhysicsBodyCollisionFilter(body: Box2D.Dynamics.b2Body, gameObject: GameObject) {
		let fixture = body.GetFixtureList();
		while (fixture != null) {
			let filter = fixture.GetFilterData()
			filter.categoryBits = gameObject.layer;
			filter.maskBits = CollisionFilter.GetMaskBitsFromLayer(gameObject.layer);
			filter.groupIndex = 0;
			fixture.SetFilterData(filter);
			fixture = fixture.GetNext();
		}
	}

	static GetMaskBitsFromLayer(layer: number){
		return this.CollisionMatrix.get(layer) || 0xFFFF;
	}
}
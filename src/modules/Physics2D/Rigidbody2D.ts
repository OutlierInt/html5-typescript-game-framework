import { Component } from "../GameObjects/Component";
import { GameObject } from "../GameObjects/GameObject";
import { PhysicsObjectHandler } from "./Physics2DObjectHandler";
import { Physics2DUtil } from "./Physics2DUtil";
import { CollisionFilter } from "./CollisionFilter";

export class Rigidbody2D extends Component implements ITogglable{
	mass: number;
	isKinematic: boolean;
	enableGravity: boolean;
	fixedRotation: boolean;
	isBullet: boolean;
	interpolation: RigidbodyInterpolation;
	body: Box2D.Dynamics.b2Body | null;
	bodyType: RigidbodyType;
	_enabled: boolean;
	
	constructor(gObj: GameObject, name: string = ""){
		super(gObj, name);
		this.mass = 1;
		this.enableGravity = true;
		this.fixedRotation = false;
		this.isKinematic = false;
		this.isBullet = false;
		this.body = null;
		this.bodyType = RigidbodyType.Dynamic;
		this.interpolation = 0;
		this._enabled = true;
	}

	//Identify Class by Name
	GetClassName() { return "Rigidbody2D"; }

	_Initialize(){
		this.AddThisRigidbodyToWorld();
	}

	private AddThisRigidbodyToWorld() {
		this.AddRigidbodyToSimulation();
		this.gameObject.transform._onTransformUpdate.Subscribe(this,
			CollisionFilter.UpdatePhysicsBodyFromTransform.bind(null, this.body, this.gameObject)
		);
		this.gameObject._onLayerChange.Subscribe(this.gameObject,
			CollisionFilter.UpdatePhysicsBodyCollisionFilter.bind(null, this.body, this.gameObject)
		);
	}

	BeforeDestroyImmediate(){
		super.BeforeDestroyImmediate();
		this.RemoveThisRigidbodyFromWorld();
	}

	private RemoveThisRigidbodyFromWorld() {
		this.RemoveRigidbodyFromSimulation();
		this.gameObject.transform._onTransformUpdate.Unsubscribe(this);
		this.gameObject._onLayerChange.Unsubscribe(this.gameObject);
	}

	//Toggle
	get isEnabled() { return this._enabled; }

	Enable() {
		if (!this._enabled) {
			this._enabled = true;
			if (this.body) this.body.SetActive(true);
			//this.AddThisRigidbodyToWorld();
		}
	}

	Disable() {
		if (this._enabled) {
			this._enabled = false;
			if (this.body) this.body.SetActive(false);
			//this.RemoveThisRigidbodyFromWorld();
		}
	}

	AddRigidbodyToSimulation()			{ PhysicsObjectHandler.AddRigidbodyToGameObject(this); }
	RemoveRigidbodyFromSimulation()	{ PhysicsObjectHandler.RemoveRigidbody(this); }
}

export enum RigidbodyType {
	Dynamic = Box2D.Dynamics.b2Body.b2_dynamicBody,
	Static = Box2D.Dynamics.b2Body.b2_staticBody,
	Kinematic = Box2D.Dynamics.b2Body.b2_kinematicBody,
}

export enum RigidbodyInterpolation {
	None = 0,
	Interpolation = 1,
	Extrapolation = 2,
}
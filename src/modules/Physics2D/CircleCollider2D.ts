import { Collider2D } from "./Collider2D";
import { GameObject } from "../GameObjects/GameObject";
import { Physics2DUtil } from "./Physics2DUtil";

export class CircleCollider2D extends Collider2D{
	private radius: number;
	
	constructor(gObj: GameObject, name: string = ""){
		super(gObj, name);
		this.radius = 0.5;
	}

	//Identify Class by Name
	GetClassName() { return "CircleCollider2D"; }

	getColliderShape(){
		const circleshape = new Box2D.Collision.Shapes.b2CircleShape(Physics2DUtil.px2world(this.radius || 0.5));
		circleshape.SetLocalPosition(Physics2DUtil.pxVec2worldVec(this.Offset));
		return circleshape;
	}

	get Radius(){ return this.radius; }
	set Radius(r: number){
		this.radius = r;
		this.UpdateShape();
	}

	protected offsetHandler(){
		this.UpdateShape();
	}

	private UpdateShape() {
		let fixture = this.fixture as Box2D.Dynamics.b2Fixture;
		let shape = fixture.GetShape() as Box2D.Collision.Shapes.b2CircleShape;
		shape.SetRadius(Physics2DUtil.px2world(this.radius));
		shape.SetLocalPosition(Physics2DUtil.pxVec2worldVec(this.Offset));
	}
}
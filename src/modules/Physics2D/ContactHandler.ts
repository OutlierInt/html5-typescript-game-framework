import { Collider2D } from "./Collider2D";
import { Physics2D } from "./Physics2D";
import { ColliderIgnoreMap } from "./Physics2DObjectHandler";

//Handle Collision Responses
let ContactsDetected: Set<Box2D.Dynamics.Contacts.b2Contact> = new Set();
let ContactsStarted: Set<Box2D.Dynamics.Contacts.b2Contact> = new Set();
let ContactsEnded: Set<Box2D.Dynamics.Contacts.b2Contact> = new Set();

function _contactStarted(contact: Box2D.Dynamics.Contacts.b2Contact) {
	ContactsDetected.add(contact);
	ContactsStarted.add(contact);
}

function _contactEnded(contact: Box2D.Dynamics.Contacts.b2Contact) {
	ContactsDetected.delete(contact);
	ContactsEnded.add(contact);
}

function _presolve(contact: Box2D.Dynamics.Contacts.b2Contact, oldManifold: Box2D.Collision.b2Manifold){
	//console.log("PreSolve\n");
	//Get the fixtures colliders
	let fixtureA = contact.GetFixtureA();
	let fixtureB = contact.GetFixtureB();
	if (!fixtureA || !fixtureB) return;

	//If these colliders are in the ignore map, ignore this contact.
	const fixtureAIgnoreSet = ColliderIgnoreMap.get(fixtureA);
	if(fixtureAIgnoreSet){
		const ignore = !fixtureAIgnoreSet.has(fixtureB);
		contact.SetEnabled(ignore);
	}
}

function _postsolve(contact: Box2D.Dynamics.Contacts.b2Contact, impulse: Box2D.Dynamics.b2ContactImpulse){
	//console.log("PostSolve\n");
}

export class CollisionHandler {
	static ContactListener: Box2D.Dynamics.b2ContactListener

	static ListenForCollisions() {
		if(CollisionHandler.ContactListener) return;

		const contactListener = new Box2D.Dynamics.b2ContactListener();
		contactListener.BeginContact = _contactStarted;
		contactListener.EndContact = _contactEnded;
		contactListener.PreSolve = _presolve;
		//contactListener.PostSolve = _postsolve;
		
		Physics2D.world.SetContactListener(contactListener);
	}

	//When the game updates, remove the pressed and released keys for the next frame.
	static UpdateContactSets() {
		if (ContactsStarted) ContactsStarted.clear();
		if (ContactsEnded) ContactsEnded.clear();
	}

	static HandleCollisionEvents() {
		//On Collision Start
		for (const contact of ContactsStarted) {
			CollisionHandler.HandleContactEvent(contact, "OnCollisionStart", "OnSensorStart");
		}
		//On Collision Stay
		for (const contact of ContactsDetected) {
			CollisionHandler.HandleContactEvent(contact, "OnCollisionStay", "OnSensorStay");
		}
		//On Collision End
		for (const contact of ContactsEnded) {
			CollisionHandler.HandleContactEvent(contact, "OnCollisionEnd", "OnSensorEnd");
		}

		//Update the contacts.
		CollisionHandler.UpdateContactSets();
	}

	static HandleContactEvent(contact: Box2D.Dynamics.Contacts.b2Contact, funcName: string, sensorFuncName: string) {
		//Does the fixtures still exist?
		let fixtureA = contact.GetFixtureA();
		let fixtureB = contact.GetFixtureB();
		if (!fixtureA || !fixtureB || typeof fixtureA != "object" || typeof fixtureB != "object") return;
		
		//Sensors cannot collide with sensors. Only one can be a sensor.
		const FixtureASensor = fixtureA.IsSensor();
		const FixtureBSensor = fixtureB.IsSensor();
		if(FixtureASensor && FixtureBSensor) return;

		//Check if these bodies are connected to their gameobjects.
		let dataA = fixtureA.GetUserData();
		let dataB = fixtureB.GetUserData();
		if (!dataA || !dataB || typeof dataA != "object" || typeof dataB != "object") return;

		//Do they have colliders? A gameobject might only have a Rigidbody2D.
		let colliderA = dataA.collider;
		let colliderB = dataB.collider;
		if (!(colliderA instanceof Collider2D) || !(colliderB instanceof Collider2D))return;

		//Gauntlet complete! Execute their functions.
		//Typical Solid-Solid Interaction
		if(!FixtureASensor && !FixtureBSensor){
			///@ts-ignore
			let colliderAFunction = colliderA[funcName] as Function;
			///@ts-ignore
			let colliderBFunction = colliderB[funcName] as Function;

			colliderAFunction.call(colliderA);
			colliderBFunction.call(colliderB);
		}
		//Sensor-Solid Interactions
		else if (FixtureASensor && !FixtureBSensor){
			///@ts-ignore
			let colliderAFunction = colliderA[sensorFuncName] as Function;
			colliderAFunction.call(colliderA);
		}
		else if (!FixtureASensor && FixtureBSensor) {
			///@ts-ignore
			let colliderBFunction = colliderB[funcName] as Function;
			colliderBFunction.call(colliderB);
		}
	}
}
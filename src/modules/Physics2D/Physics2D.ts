import { canvas, ctx } from "../System/Canvas";
import { Time } from "../System/Time";
import { Vector } from "../Math/Vector";
import { MathX } from "../Math/MathX";
import { CollisionHandler } from "./ContactHandler";
import { Rigidbody2D, RigidbodyInterpolation } from "./Rigidbody2D";
import { PhysicsGameObjectMap, PhysicsObjectHandler } from "./Physics2DObjectHandler";
import { Physics2DUtil } from "./Physics2DUtil";
import { Debug } from "../System/Debug";
import { Transform } from "../GameObjects/Transform";
import { Camera2D } from "../Render2D/Camera2D";
import { Collider2D } from "./Collider2D";

export const box2d = {
	b2Vec2 					: Box2D.Common.Math.b2Vec2,
	b2BodyDef 			: Box2D.Dynamics.b2BodyDef,
	b2Body 					: Box2D.Dynamics.b2Body,
	b2FixtureDef 		: Box2D.Dynamics.b2FixtureDef,
	b2Fixture 			: Box2D.Dynamics.b2Fixture,
	b2World 				: Box2D.Dynamics.b2World,
	b2MassData 			: Box2D.Collision.Shapes.b2MassData,
	b2PolygonShape	: Box2D.Collision.Shapes.b2PolygonShape,
	b2CircleShape 	: Box2D.Collision.Shapes.b2CircleShape,
	b2EdgeShape			: Box2D.Collision.Shapes.b2EdgeShape,
	b2EdgeChainDef	: Box2D.Collision.Shapes.b2EdgeChainDef,
	b2DebugDraw 		: Box2D.Dynamics.b2DebugDraw,
}

const PrePhysicsActions: Function[] = [];
const debugDraw = new Box2D.Dynamics.b2DebugDraw();
export class Physics2D {
	static SCALE = 300;
	static DEFAULT_SCALE = 30;
	static world: Box2D.Dynamics.b2World;
	static box2d = box2d;
	static DebugDrawObjects: DebugDrawCanvasInfo;
	static velocityIterations = 6;
	static positionIterations = 2;

	private static _DrawDebugShapes = true;
	static get DrawDebugShapes() { return Physics2D._DrawDebugShapes; }
	static set DrawDebugShapes(enabled: boolean){
		Physics2D._DrawDebugShapes = enabled;
		if(!enabled){
			const debugCanvas = Physics2D.DebugDrawObjects.debugCanvas;
			Physics2D.DebugDrawObjects.debugCtx.clearRect(0, 0, debugCanvas.width, debugCanvas.height);
		}
	}

	static Raycast(origin: Vector, endPoint: Vector, mask = 0xFFFF, hitSensors = false){
		//Transform
		const worldOrigin = Physics2DUtil.pxVec2worldVec(origin)
		const worldEndPoint = Physics2DUtil.pxVec2worldVec(endPoint);

		//Check the center of this point
		let originBlocked = false;
		this.world.QueryPoint(function(fixture){
			if(fixture.IsSensor() && !hitSensors) return true;

			const colliderLayer = fixture.GetFilterData().categoryBits;
			if ((mask & colliderLayer) === 0) {
				return true;
			}
			originBlocked = true;
			return false;
		}, worldOrigin);

		//Skip if origin blocked
		if(originBlocked) return null;

		//Best Raycast Data
		let rFixture: Box2D.Dynamics.b2Fixture | null = null;
		let rPoint: Box2D.Common.Math.b2Vec2 | null = null;
		let rNormal: Box2D.Common.Math.b2Vec2 | null = null;
		let rFraction: number = 1;

		this.world.RayCast(function(fixture, point, normal, fraction){
			//Ignore sensors if desired
			if (fixture.IsSensor() && !hitSensors) return rFraction;

			//This fixture can't collide with the mask. Keep looking.
			const colliderLayer = fixture.GetFilterData().categoryBits;
			if((mask & colliderLayer) === 0){
				return rFraction;
			}

			//Only use the smallest fraction
			if(fraction >= rFraction){
				return rFraction;
			}

			//This fixture is better. Store this data as the best one.
			rFixture = fixture;
			rPoint = point;
			rNormal = normal;
			rFraction = fraction;
			return rFraction;
		}, worldOrigin, worldEndPoint);

		if(rFixture && rPoint && rNormal){
			return {
				///@ts-ignore
				collider: rFixture.GetUserData().collider,
				point: Physics2DUtil.worldVec2pxVec(rPoint),
				normal: Physics2DUtil.worldVec2pxVec(rNormal).normalizeVolatile(),
				distance: rFraction,
			} as RaycastOutput;
		}

		return null;
	}

	static IgnoreCollision(colliderA: Collider2D, colliderB: Collider2D, ignore: boolean){
		return PhysicsObjectHandler.IgnoreCollision(colliderA, colliderB, ignore);
	}
}

interface RaycastOutput{
	collider: Collider2D,
	point: Vector,
	normal: Vector,
	distance: number
}

interface DebugDrawCanvasInfo{
	debugCanvas: HTMLCanvasElement;
	debugCtx: CanvasRenderingContext2D;
}

export function SetupPhysics(gravity: Vector = new Vector(0, 50), allowSleep: boolean = true) {
	const b2gravity = new box2d.b2Vec2(gravity.x, gravity.y);
	
	const scaleFactor = Physics2D.DEFAULT_SCALE / Physics2D.SCALE;
	b2gravity.Multiply(scaleFactor);

	Physics2D.world = new box2d.b2World(b2gravity, allowSleep);
	CollisionHandler.ListenForCollisions();
	CreateDebugCanvas();
}

function CreateDebugCanvas(){
	//if(debugReady) return;
	
	const debugCanvas = document.createElement("canvas");
	const debugCtx = debugCanvas.getContext("2d") as CanvasRenderingContext2D;
	debugCanvas.width = canvas.width;
	debugCanvas.height = canvas.height;
	debugCanvas.setAttribute("style", `pointer-events: none; background-color: #0000; position: absolute; top: 0; left: 0;`);
	//
	
	//document.body.appendChild(debugCanvas);
	(canvas.parentNode as Node).appendChild(debugCanvas);

	// const interval = setInterval(() => {
	// 	if(!debugCanvas || !canvas){
	// 		console.log("Ending Box2d draw canvas update interval.")
	// 		clearInterval(interval);
	// 	}
	// 	debugCanvas.width = canvas.width;
	// 	debugCanvas.height = canvas.height;
	// },0);

	debugDraw.SetSprite(debugCtx);
	debugDraw.SetDrawScale(Physics2D.SCALE);
	debugDraw.SetFillAlpha(0.5);
	debugDraw.SetFlags(box2d.b2DebugDraw.e_shapeBit | box2d.b2DebugDraw.e_jointBit);

	Physics2D.world.SetDebugDraw(debugDraw);
	Physics2D.DebugDrawObjects = {
		debugCanvas,
		debugCtx
	};
}

export function PhysicsUpdate() {
	if (!Physics2D.world) return;

	//Handle any pre-physics actions
	FirePrePhysicsActions();

	//Update the physics bodies according to the game
	for (const pair of PhysicsGameObjectMap.entries()) {
		const gameObject = pair[0];
		const body = pair[1];

		if(!gameObject || !body) continue;

		body.SetPosition(Physics2DUtil.pxVec2worldVec(gameObject.transform.position));
		body.SetAngle(gameObject.transform.rotation.z);
		NegateGravity(body);
	}

	//Physics Engine Update
	Physics2D.world.Step(Time.fixedDeltaTime, Physics2D.velocityIterations, Physics2D.positionIterations);
	Physics2D.world.ClearForces();

	//Update Transforms according to the Physics Engine.
	for (const pair of PhysicsGameObjectMap.entries()){
		const gameObject = pair[0];
		const body = pair[1];
		
		if (!gameObject || !body) continue;

		Physics2DUtil.UpdateGameTransforms(body, gameObject.transform);
	}

	//Collision Events are handled here
	CollisionHandler.HandleCollisionEvents();
}

function NegateGravity(body: Box2D.Dynamics.b2Body){
	const isDynamicBody = body.GetType() === box2d.b2Body.b2_dynamicBody;
	const userData = body.GetUserData();
	if(!userData) return;

	const rigidbody = userData.rigidbody as Rigidbody2D;
	if (isDynamicBody && rigidbody && !rigidbody.enableGravity) {
    const reverseGravity = Physics2D.world.GetGravity().Copy();
    reverseGravity.NegativeSelf();
    reverseGravity.Multiply(body.GetMass());
    body.ApplyForce(reverseGravity, body.GetWorldCenter());
  }
}

export function DrawPhysicsDebugData(){
	if (Physics2D.DrawDebugShapes){
		const ctx = Physics2D.DebugDrawObjects.debugCtx;
		const canvas = Physics2D.DebugDrawObjects.debugCanvas;
		ctx.clearRect(0, 0, canvas.width, canvas.height);

		ctx.save();
		
		//Rotate and Scale Camera from the center
		Camera2D.RotateScaleCanvas(canvas, ctx);

		const pos = Camera2D.cameraPosition;
		ctx.translate(-pos.x, -pos.y);
		
		Physics2D.world.DrawDebugData();
		
		ctx.restore();
	}
}

export function PhysicsInterpolation(){

	for (const pair of PhysicsGameObjectMap.entries()){
		const gameObject = pair[0];
		const body = pair[1];
		const bodyData = body.GetUserData();

		if(body.GetType() !== box2d.b2Body.b2_dynamicBody || !bodyData) continue;

		const rigidbody = bodyData.rigidbody as Rigidbody2D | null;
		if(!rigidbody) continue;
			
		switch(rigidbody.interpolation){
			case RigidbodyInterpolation.Extrapolation:
				ExtrapolateRigidbody(rigidbody, gameObject.transform);
				break;
			case RigidbodyInterpolation.Interpolation:
				InterpolateRigidbody(rigidbody, gameObject.transform);
				break;
			default: break;
		}
	}

	/*
	//console.log("Frame", Time.frameCount);
	let i = 0;
	for (const body of PhysicsGameObjectMap.values()) {
		let veloLength = body.GetLinearVelocity().Length().toFixed(3);
		//console.log(i, veloLength);
		Debug.print(veloLength, new Vector(100, 100 + 20 * ++i));
	}
	*/


	/*
	let alpha = Time.fixedDeltaTimeAlpha;
	for (const pair of PhysicsGameObjectMap.entries()) {
		const gameObject = pair[0];
		const body = pair[1];

		if(body.GetType() === box2d.b2Body.b2_staticBody)
			continue;

		const transform = gameObject.transform;
		const transformPosition = transform.position;

		const physBodyPosition 	= Physics2DUtil.worldVec2pxVec(body.GetPosition());
		const interpolatedPosition = Vector.Lerp(physBodyPosition, transformPosition, alpha);
		
		//Draw the positions
		if(Physics2D.DrawDebugShapes){
			ctx.save();
			ctx.translate(transformPosition.x, transformPosition.y);
			ctx.fillStyle = "#0F0";
			ctx.beginPath();
			ctx.arc(0,0, 5, 0, MathX.PI2);
			ctx.fill();
			ctx.restore();

			// ctx.save();
			// ctx.translate(prevPosition.x, prevPosition.y);
			// ctx.fillStyle = "#F00";
			// ctx.beginPath();
			// ctx.arc(0, 0, 5, 0, MathX.PI2);
			// ctx.fill();
			// ctx.restore();

			ctx.save();
			ctx.translate(physBodyPosition.x, physBodyPosition.y);
			ctx.fillStyle = "#0FF";
			ctx.beginPath();
			ctx.arc(0, 0, 5, 0, MathX.PI2);
			ctx.fill();
			ctx.restore();
		}
	}
	*/
}

function ExtrapolateRigidbody(rigidbody: Rigidbody2D, transform: Transform){
	
}

function InterpolateRigidbody(rigidbody: Rigidbody2D, transform: Transform){

}

export function AddPrePhysicsAction(func: Function){
	PrePhysicsActions.push(func);
}

function FirePrePhysicsActions(){
	if(PrePhysicsActions.length <= 0) return;
	for (const func of PrePhysicsActions) func();
	PrePhysicsActions.length = 0;
}
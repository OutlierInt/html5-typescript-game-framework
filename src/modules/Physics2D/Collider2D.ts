import { Component } from "../GameObjects/Component";
import { GameObject } from "../GameObjects/GameObject";
import { Script } from "../GameObjects/Script";
import { Vector } from "../Math/Vector";
import { ICollider2DData } from "./ICollider2DData";
import { PhysicsObjectHandler } from "./Physics2DObjectHandler";
import { Physics2DUtil } from "./Physics2DUtil";
import { CollisionFilter } from "./CollisionFilter";

export class Collider2D extends Component implements ITogglable{
	private _offset: Vector;
	private _isSensor: boolean;
	private _enabled: boolean
	body: Box2D.Dynamics.b2Body | null;
	fixture: Box2D.Dynamics.b2Fixture | null;
	physicsData: ICollider2DData | null;

	OnCollisionStartQueue: 	Map<Function, Script>
	OnCollisionStayQueue: 	Map<Function, Script>
	OnCollisionEndQueue: 		Map<Function, Script>

	OnSensorStartQueue: Map<Function, Script>
	OnSensorStayQueue: 	Map<Function, Script>
	OnSensorEndQueue: 	Map<Function, Script>

	constructor(gObj: GameObject, name: string = ""){
		super(gObj, name);
		this._offset = Vector.zero;
		this.body = null;
		this.fixture = null;
		this._isSensor = false;
		this.physicsData = null;
		this._enabled = true;

		this.OnCollisionStartQueue = new Map();
    this.OnCollisionStayQueue = new Map();
		this.OnCollisionEndQueue = new Map();
		
		this.OnSensorStartQueue = new Map();
    this.OnSensorStayQueue = new Map();
    this.OnSensorEndQueue = new Map();	
	}

	//Identify Class by Name
	GetClassName() { return "Collider2D"; }

	_Initialize(){
		this.AddThisColliderToWorld();
	}

	private AddThisColliderToWorld() {
		this.AddColliderToSimulation();
		Collider2D.AddAvailableSubscribers(this);
		this.gameObject.transform._onTransformUpdate.Subscribe(this,
			CollisionFilter.UpdatePhysicsBodyFromTransform.bind(null, this.body, this.gameObject)
		);
		this.gameObject._onLayerChange.Subscribe(this.gameObject, 
			CollisionFilter.UpdatePhysicsBodyCollisionFilter.bind(null, this.body, this.gameObject)
		);
	}

	BeforeDestroyImmediate(){
		super.BeforeDestroyImmediate();
		this.RemoveThisColliderFromWorld();
	}

	private RemoveThisColliderFromWorld() {
		this.OnCollisionStartQueue.clear();
		this.OnCollisionStayQueue.clear();
		this.OnCollisionEndQueue.clear();

		this.OnSensorStartQueue.clear();
		this.OnSensorStayQueue.clear();
		this.OnSensorEndQueue.clear();

		//This weird sequence is used to avoid a circular reference with Physics2D.
		// if(this.body){
		// 	this.body.GetWorld().DestroyBody(this.body);
		// }
		//This is a circular reference.
		//Collider2D.ts imports Physics2D.ts imports Collider2D.ts imports...
		//if(this.body) Physics2DUtil.RemoveBodyFromWorld(this.body)

		this.gameObject.transform._onTransformUpdate.Unsubscribe(this);
		this.gameObject._onLayerChange.Unsubscribe(this.gameObject);
		this.RemoveColliderFromSimulation();
	}

	//Toggle
	get isEnabled() { return this._enabled; }

	Enable() {
		if (!this._enabled) {
			this._enabled = true;
			if (this.body) this.body.SetActive(true);
			//this.AddThisColliderToWorld();
		}
	}

	Disable() {
		if (this._enabled) {
			this._enabled = false;
			if (this.body) this.body.SetActive(false);
			//this.RemoveThisColliderFromWorld();
		}
	}

	AddColliderToSimulation()				{ PhysicsObjectHandler.AddColliderToGameObject(this); }
	RemoveColliderFromSimulation() 	{ PhysicsObjectHandler.RemoveCollider(this); }

	OnCollisionStart()	{ Collider2D.FireMessageToComponents(this.OnCollisionStartQueue)}
	OnCollisionStay()		{ Collider2D.FireMessageToComponents(this.OnCollisionStayQueue)}
	OnCollisionEnd() 		{ Collider2D.FireMessageToComponents(this.OnCollisionEndQueue)}

	OnSensorStart()		{ Collider2D.FireMessageToComponents(this.OnSensorStartQueue)}
	OnSensorStay()		{ Collider2D.FireMessageToComponents(this.OnSensorStayQueue)}
	OnSensorEnd() 		{ Collider2D.FireMessageToComponents(this.OnSensorEndQueue)}
	
	//Getter/Setters
	//offset;
	//physicsData;

	get IsSensor()	{ return this._isSensor; }
	get Offset()		{ return this._offset; }

	set IsSensor(b: boolean) {
		this._isSensor = b;
		(this.fixture as Box2D.Dynamics.b2Fixture).SetSensor(this._isSensor);
	}
	set Offset(v: Vector){
		this._offset = v.Clone();
		this.offsetHandler();
	}

	protected offsetHandler(){
		//Does nothing...
	}

	//Helpers
	getColliderShape(): Box2D.Collision.Shapes.b2Shape | null	{
		return null;
	}

	static AddAvailableSubscribers(col: Collider2D){
		for (const component of col.gameObject.components) {
			if (component instanceof Script && !component.collidersSubscribed.has(col)) {
				Script.SubscribeToColliders(component);
      }
		}
	}

	static FireMessageToComponents(queue: Map<Function, Script>){
		for (const eventFunction of queue.keys()) {
			eventFunction();
		}
	}
}
export class MathX{
	static RAD2DEG: number = 57.29577951308232;
	static DEG2RAD: number = 0.017453292519943295;
	static PI2: number = 2 * Math.PI;

	static Lerp(a: number, b: number, t: number): number { return a + (b - a) * MathX.Clamp01(t); }
	static LerpUnclamped(a: number, b: number, t: number): number { return a + (b - a) * t; }
	static Clamp(v: number, min: number, max: number): number { return Math.max(Math.min(v, max), min); }
	static Clamp01(v: number): number { return MathX.Clamp(v, 0, 1); }

	static InverseLerp(a: number, b: number, value: number): number {
		if (a !== b) return MathX.Clamp01((value - a) / (b - a));
		return 0;
	}

	static SmoothStep(a: number, b: number, t: number): number {
		t = MathX.Clamp01(t);
		t = -2.0 * t * t * t + 3.0 * t * t;
		return b * t + a * (1.0 - t);
	}

	static CubicBezier(p0: number, p1: number, p2: number, p3: number, t: number){
		t = MathX.Clamp01(t);

		const t2 = t * t;
		const t3 = t2 * t;
		const mt = 1 - t;
		const mt2 = mt * mt;
		const mt3 = mt2 * mt;

		return (p0 * mt3) + (3 * p1 * mt2 * t) + (3 * p2 * mt * t2) + (p3 * t3);
	}

	static CatmullRom(v0: number, v1: number, v2: number, v3: number, t: number){
		const t2 = t * t;
		const t3 = t2 * t;
		const a = 0.5 * (2.0 * v1);
		const b = 0.5 * (-v0 + v2);
		const c = 0.5 * (2.0 * v0 - 5.0 * v1 + 4 * v2 - v3);
		const d = 0.5 * (-v0 + 3.0 * v1 - 3.0 * v2 + v3);

		return a + b * t + c * t2 + d * t3;
	}

	static RandomRange(min: number, max: number): number {
		return MathX.Lerp(min, max, Math.random());
	}

	static RandomColor(){
		return `rgb(${MathX.RandomRange(0, 255)},${MathX.RandomRange(0, 255)},${MathX.RandomRange(0, 255)})`;
	}

	static RandomToMax(max: number){
		return Math.random() * max;
	}

	static ChooseRandomValue(...args: number[]){
		return args[Math.floor(MathX.RandomToMax(args.length))];
	}

	static GetNearestLeftNeighborIndex(array: any[], value: number, property?: string){
		if(array.length <= 0) return -1;

		let start = 0;
		let end = array.length - 1;

		//Clamp value to be within range
		const startValue: number = property ? array[start][property] : array[start];
		if(value < startValue) return start;

		const endValue: number = property ? array[end][property] : array[end];
		if(value > endValue) return end;

		//Begin Binary Search
		while( start < end && start != end-1 ){
			//Get middle position
			const mid = Math.floor((end + start) / 2);
			const midValue: number = property ? array[mid][property] : array[mid];

			//Found it in the array!
			if(value === midValue)
				return mid;
			//Nearest neighbor is on the left
			else if (value < midValue)
				end = mid;
			//Nearest neighbor is on the right
			else
				start = mid;
		}

		//Didn't find it in the array but the start and end have converged
		return start;
	}
}

import { MathX } from "./MathX";
import { IClonable } from "../GameObjects/IClonable";
import { IPassByValue } from "./IPassByValue";
import { IClassIdentifiable } from "../GameObjects/IClassIdentifiable";

export class Vector implements IClonable, IPassByValue, IClassIdentifiable{
	private vX: number;
	private vY: number;
	private vZ: number;
	private vW: number;
	private static EPSILON: number = 0.000001;
	private static LOWP_EPSILON: number = 0.001;
	PassByValue = true;
	
	constructor(x: number = 0, y: number = 0, z: number = 0, w: number = 1) {
		this.vX = x;
		this.vY = y;
		this.vZ = z;
		this.vW = w;
	}

	*[Symbol.iterator]() {
		yield this.vX;
		yield this.vY;
		yield this.vZ;
		yield this.vW;
	};

	//Identify Class by Name
	GetClassName() { return "Vector"; }

	//private static _zero 			= Object.freeze(new Vector(0, 0, 0));
	//private static _one 			= Object.freeze(new Vector(1, 1, 1));
	//private static _right 		= Object.freeze(new Vector(1, 0, 0));
	//private static _left 			= Object.freeze(new Vector(-1, 0, 0));
	//private static _up 				= Object.freeze(new Vector(0, 1, 0));
	//private static _down 			= Object.freeze(new Vector(0, -1, 0));
	//private static _forward 	= Object.freeze(new Vector(0, 0, 1));
	//private static _backward 	= Object.freeze(new Vector(0, 0, -1));

	toArray(): number[] 			{ return [this.vX, this.vY, this.vZ, this.vW]; }
	static fromArray(nums: number[]) { return new Vector(...nums); }

	static Clone(v: Vector) { return new Vector(v.x, v.y, v.z, v.w); }
	Clone() { return Vector.Clone(this); }

	get x(): number { return this.vX; }
	get y(): number { return this.vY; }
	get z(): number { return this.vZ; }
	get w(): number { return this.vW; }

	set x(x: number) { this.vX = x; }
	set y(y: number) { this.vY = y; }
	set z(z: number) { this.vZ = z; }
	set w(w: number) { this.vW = w; }

	Set(x?: number, y?: number, z?: number, w?: number){
		this.vX = (x != null) ? x : this.vX;
		this.vY = (y != null) ? y : this.vY;
		this.vZ = (z != null) ? z : this.vZ;
		this.vW = (w != null) ? w : this.vW;
		return this;
	} 

	//static get zero()			{ return Vector._zero; }
	//static get one()			{ return Vector._one; }
	//static get right()		{ return Vector._right; }
	//static get left()			{ return Vector._left; }
	//static get up()				{ return Vector._up; }
	//static get down()			{ return Vector._down; }
	//static get forward()	{ return Vector._forward; }
	//static get backward()	{ return Vector._backward; }

	static get zero()			{ return new Vector( 0,  0,  0); }
	static get one()			{ return new Vector( 1,  1,  1); }
	static get right()		{ return new Vector( 1,  0,  0); }
	static get left()			{ return new Vector(-1,  0,  0); }
	static get up()				{ return new Vector( 0,  1,  0); }
	static get down()			{ return new Vector( 0, -1,  0); }
	static get forward()	{ return new Vector( 0,  0,  1); }
	static get backward()	{ return new Vector( 0,  0, -1); }

	toString() { return `[${this.x.toFixed(3)}, ${this.y.toFixed(3)}, ${this.z.toFixed(3)}, ${this.w.toFixed(3)}]`; }

	//Vector Arithmetic
	negate() 						{ return new Vector(-this.x, -this.y, -this.z); }
	add(v: Vector) 			{ return new Vector(this.x + v.x, this.y + v.y, this.z + v.z); }
	subtract(v: Vector)	{ return new Vector(this.x - v.x, this.y - v.y, this.z - v.z); }	
	multiply(n: number) { return new Vector(this.x * n, this.y * n, this.z * n); }
	divide(n: number) 	{ return new Vector(this.x / n, this.y / n, this.z / n); }
	scale(v: Vector)		{ return new Vector(this.x * v.x, this.y * v.y, this.z * v.z); }
	invScale(v: Vector)	{ return new Vector(this.x / v.x, this.y / v.y, this.z / v.z); }

	//Vector Equality
	Equals(v: Vector)		{
		return Math.abs(this.vX - v.vX) < Vector.EPSILON &&
		Math.abs(this.vY - v.vY) < Vector.EPSILON &&
		Math.abs(this.vZ - v.vZ) < Vector.EPSILON;
	}

	//Vector Magnitudes
	get SqrMagnitude()	{ return this.x * this.x + this.y * this.y + this.z * this.z; }
	get Magnitude()	 		{ return Math.sqrt(this.x * this.x + this.y * this.y + this.z * this.z); }
	get Normalized()	{
		let magnitude = this.Magnitude;
		if(magnitude >= Vector.EPSILON) return new Vector(this.x / magnitude, this.y / magnitude, this.z / magnitude);
		return Vector.zero;
	}

	//Other Methods
	Rotate2D(angleRads: number) { return Vector.Rotate2D(this, angleRads); }

	//Static Methods
	//Vector Linear Operations
	static Dot(a: Vector, b: Vector) { return a.x*b.x + a.y*b.y + a.z*b.z; }

	static Cross(a: Vector, b: Vector) {
		return new Vector(
			a.y * b.z - a.z * b.y,
			a.z * b.x - a.x * b.z,
			a.x * b.y - a.y * b.x );
	}
	
	static Angle(a: Vector, b: Vector) {
		return Math.acos(Vector.Dot(a.Normalized, b.Normalized)) * MathX.RAD2DEG;
	}

	static AngleRad(a: Vector, b: Vector) {
		return Math.acos(Vector.Dot(a.Normalized, b.Normalized));
	}
	
	static Project(v: Vector, normal: Vector) {
		let normalSqrLength = Vector.Dot(normal, normal);
		if(normalSqrLength < Vector.EPSILON) return Vector.zero;
		return normal.multiply(Vector.Dot(v, normal) / normalSqrLength);
	}
	
	static ProjectOnPlane(v: Vector, normal: Vector) {
		return v.subtract(Vector.Project(v, normal));
	}
	
	static Reflect(v: Vector, normal: Vector) {
		return normal.add(v).multiply(-2 * Vector.Dot(normal, v));
	}

	static Rotate2D(v: Vector, angleRads: number) {
		return new Vector(
			Math.cos(angleRads) * v.x - Math.sin(angleRads) * v.y,
			Math.sin(angleRads) * v.x + Math.cos(angleRads) * v.y,
			v.z,
			v.w
		);
	}

	//Other Operations
	static Lerp(a: Vector, b: Vector, t: number) { 
		return new Vector(MathX.Lerp(a.x, b.x, t), MathX.Lerp(a.y, b.y, t), MathX.Lerp(a.z, b.z, t));
	}
	
	static SmoothStep(a: Vector, b: Vector, t: number) {
		return new Vector(MathX.SmoothStep(a.x, b.x, t), MathX.SmoothStep(a.y, b.y, t), MathX.SmoothStep(a.z, b.z, t));
	}
	
	static CubicBezier(v0: Vector, v1: Vector, v2: Vector, v3: Vector, t: number) {
		return new Vector(
			MathX.CubicBezier(v0.x, v1.x, v2.x, v3.x, t),
			MathX.CubicBezier(v0.y, v1.y, v2.y, v3.y, t),
			MathX.CubicBezier(v0.z, v1.z, v2.z, v3.z, t),
		);
	}

	static CatmullRom(v0: Vector, v1: Vector, v2: Vector, v3: Vector, t: number){
		return new Vector(
			MathX.CatmullRom(v0.x, v1.x, v2.x, v3.x, t),
			MathX.CatmullRom(v0.y, v1.y, v2.y, v3.y, t),
			MathX.CatmullRom(v0.z, v1.z, v2.z, v3.z, t),
		);
	}
	
	static Distance(a: Vector, b: Vector) {
		return a.subtract(b).Magnitude; 
	}

	static ClampMagnitude(v: Vector, maxLength: number) {
		return (v.SqrMagnitude > maxLength * maxLength) ? v.Normalized.multiply(maxLength) : v;
	}

	//Volatile Versions
	negateVolatile() {
		this.x = -this.x;
		this.y = -this.y;
		this.z = -this.z;
		this.w = this.w;
		return this;
	}
	addVolatile(v: Vector) {
		this.x += v.x;
		this.y += v.y;
		this.z += v.z;
		this.w = this.w;
		return this;
	}
	subtractVolatile(v: Vector) {
		this.x -= v.x;
		this.y -= v.y;
		this.z -= v.z;
		this.w = this.w;
		return this;
	}
	multiplyVolatile(n: number) {
		this.x *= n;
		this.y *= n;
		this.z *= n;
		this.w = this.w;
		return this;
	}
	divideVolatile(n: number) {
		this.x /= n;
		this.y /= n;
		this.z /= n;
		this.w = this.w;
		return this;
	}
	scaleVolatile(v: Vector) {
		this.x *= v.x;
		this.y *= v.y;
		this.z *= v.z;
		this.w = this.w;
		return this;
	}
	invScaleVolatile(v: Vector) {
		this.x /= v.x;
		this.y /= v.y;
		this.z /= v.z;
		this.w = this.w;
		return this;
	}

	normalizeVolatile() {
		let magnitude = this.Magnitude;
		if (magnitude >= Vector.EPSILON) {
			return this.divideVolatile(magnitude);
		}
		return this.multiplyVolatile(0);
	}

	rotate2DVolatile(angleRads: number) {
		const x = Math.cos(angleRads) * this.x - Math.sin(angleRads) * this.y;
		const y = Math.sin(angleRads) * this.x + Math.cos(angleRads) * this.y;
		this.x = x;
		this.y = y;
		this.z = this.z;
		this.w = this.w;
		return this;
	}

	clampMagnitudeVolatile(maxLength: number) {
		return (this.SqrMagnitude > maxLength * maxLength) ? this.normalizeVolatile().multiplyVolatile(maxLength) : this;
	}
}

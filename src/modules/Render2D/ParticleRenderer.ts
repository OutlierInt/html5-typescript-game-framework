import { Renderer } from "./Renderer";
import { GameObject } from "../GameObjects/GameObject";
import { ctx } from "../System/Canvas";
import { Sprite } from "./Sprite";
import { Camera2D } from "./Camera2D";
import { Vector } from "../Math/Vector";
import { Debug } from "../System/Debug";

export class ParticleSystem extends Renderer{
	sprite: Sprite | null = null;
	particles: Particle[];

	constructor(gObj: GameObject, name: string = ""){
		super(gObj, name);
		this.particles = [];
	}

	//Identify Class by Name
	GetClassName() { return "ParticleSystem"; }

	Draw(){
		if(this.sprite){
			if (!this.IsVisible()) return;

			const sprite = this.sprite;

			const transform = this.gameObject.transform;
			const position = Camera2D.GetTransformRenderPosition(transform);

			ctx.imageSmoothingEnabled = this.imageSmoothingEnabled;
			ctx.translate(position.x, position.y);
			ctx.rotate(transform.getAngle2d);
			ctx.scale(transform.scale.x, transform.scale.y);
			
			const spriteWidth = sprite.width / 2;
			const spriteHeight = sprite.height / 2;

			for (const particle of this.particles) {
				if(!particle || particle.lifetime <= 0) continue;
				ctx.save();

				ctx.globalAlpha = particle.opacity;
				ctx.translate(particle.x, particle.y)
				ctx.scale(particle.scaleX, particle.scaleY);
				ctx.rotate(particle.angle);

				ctx.drawImage(sprite.texture, -spriteWidth, -spriteHeight);
				ctx.restore();
			}
		}
	}

	GetBounds(){
		const xBounds = this.getMinAndMax(this.particles, "x");
		const yBounds = this.getMinAndMax(this.particles, "y");
	}

	private getMinAndMax(arr: any, propName: string){
		let maximum = Number.NEGATIVE_INFINITY;
		let minimum = Number.POSITIVE_INFINITY;
		for (let i = 0; i < arr.length; i++) {
			minimum = Math.min(arr[i][propName]);
			maximum = Math.max(arr[i][propName]);
		}
		return {
			min: maximum !== Number.NEGATIVE_INFINITY ? maximum : 0,
			max: minimum !== Number.POSITIVE_INFINITY ? minimum : 0,
		}
	}
}

export class Particle {
	x: number = 0;
	y: number = 0;
	angle: number = 0;
	scaleX: number = 1;
	scaleY: number = 1;
	velocityX: number = 0;
	velocityY: number = 0;
	angularVelocity: number = 0;
	opacity: number = 0;
	lifetime: number = 0;
	lifetimeMax: number = 0;
}
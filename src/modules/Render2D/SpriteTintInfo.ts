import { Vector } from "../Math/Vector";

export interface SpriteTintInfo {
	color: string | Vector;
	tintType: SpriteTintType;
	operation: string;
	opacity: number;
}

export enum SpriteTintType {
	Colorize,
	Tint
}

import { Vector } from "../Math/Vector";
import { AssetManager } from "../System/AssetManager";
import { IClassIdentifiable } from "../GameObjects/IClassIdentifiable";

export class Sprite implements IClassIdentifiable {
	texture: HTMLImageElement;
	textureSrc: string;
	name: string;
	sourceX: number;
	sourceY: number;
	width: number;
	height: number;
	originX: number;
	originY: number;
	imagePoints: Vector[];

	constructor(texSrc?: string){//, callback?: (ev: Event) => any) {
		this.name = "";
    this.sourceX = 0;
    this.sourceY = 0;
    this.width = 0;
    this.height = 0;
    this.originX = 0.5;
    this.originY = 0.5;
		this.imagePoints = [];
		
		if(texSrc){
			this.textureSrc = texSrc;
			this.texture = AssetManager.TryGetTexture(texSrc);//AssetManager.LoadTexture(texSrc, callback);
		}
		else{
			this.textureSrc = "";
			this.texture = AssetManager.nullImage;
			this.width = 1;
			this.height = 1;
		}
	}

	//Identify Class by Name
	GetClassName() { return "Sprite"; }
}

import { Renderer } from "./Renderer";
import { GameObject } from "../GameObjects/GameObject";
import { Sprite } from "./Sprite";
import { ctx } from "../System/Canvas";
import { Vector } from "../Math/Vector";
import { Camera2D } from "./Camera2D";

export class TiledRenderer extends Renderer{
	texture: HTMLImageElement | HTMLCanvasElement | HTMLVideoElement | null;
	tiled: "no-repeat" | "repeat" | "repeat-x" | "repeat-y";
	scale: Vector;

	constructor(gObj: GameObject, name: string = "") {
		super(gObj, name);
		this.texture = null;
		this.tiled = "repeat";
		this.scale = Vector.one;
	}

	//Identify Class by Name
	GetClassName() { return "TiledRenderer"; }

	Draw(){
		if (this.texture) {
			if(!this.IsVisible()) return;
			const transform = this.gameObject.transform;
			const position = Camera2D.GetTransformRenderPosition(transform);
			ctx.imageSmoothingEnabled = this.imageSmoothingEnabled;

			ctx.translate(position.x, position.y);
			ctx.rotate(transform.getAngle2d);
			ctx.scale(transform.scale.x, transform.scale.y);
			
			const pattern = ctx.createPattern(this.texture, this.tiled);
			ctx.fillStyle = pattern;
			ctx.fillRect(0, 0, this.scale.x, this.scale.y);
		}
		else {
			Renderer.DefaultDraw(this.gameObject);
		}
	}
}
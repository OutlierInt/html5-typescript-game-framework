import { Renderer } from "./Renderer";
import { GameObject } from "../GameObjects/GameObject";
import { ctx } from "../System/Canvas";
import { Vector } from "../Math/Vector";
import { AssetManager } from "../System/AssetManager";
import { Sprite } from "./Sprite";
import { SpriteTintInfo, SpriteTintType } from "./SpriteTintInfo";
import { Camera2D } from "./Camera2D";

// Buffer used for tinting sprites
const bufferCanvas = document.createElement("canvas");
bufferCanvas.width = 1024;
bufferCanvas.height = 1024;

const bufferCtx = bufferCanvas.getContext("2d") as CanvasRenderingContext2D;
bufferCtx.imageSmoothingEnabled = false;

export class SpriteRenderer extends Renderer{
	sprite: Sprite | null;
	colorizer: SpriteTintInfo | null;
	scale: Vector;

	constructor(gObj: GameObject, name: string = ""){
		super(gObj, name);
		this.sprite = null;
		this.colorizer = { color: "#FFF", tintType: SpriteTintType.Tint, operation: "multiply", opacity: 1 };
		this.scale = Vector.one;
	}

	//Identify Class by Name
	GetClassName() { return "SpriteRenderer"; }

	debugSetup(){
		let sprite = new Sprite("images/testsheet.png");
		sprite.width = 36;
    sprite.height = 18;
    sprite.sourceX = 166;
    sprite.sourceY = 9;
		this.sprite = sprite;
	}

	Draw(){
		if (this.sprite && this.sprite.texture){
			if (!this.IsVisible()) return;

			const transform = this.gameObject.transform;
			const position = Camera2D.GetTransformRenderPosition(transform);
			ctx.imageSmoothingEnabled = this.imageSmoothingEnabled;

			ctx.translate(position.x, position.y);
			ctx.rotate(transform.getAngle2d);
			ctx.scale(transform.scale.x, transform.scale.y);

			if(this.colorizer) ctx.globalAlpha = this.colorizer.opacity;
			
			const sprite = this.sprite;
			if (this.colorizer && this.colorizer.color != "#FFF"){
				this.DrawTintedImage(sprite, this.colorizer);
			}
			else{
				ctx.drawImage(
					sprite.texture,
					sprite.sourceX,
					sprite.sourceY,
					sprite.width,
					sprite.height,
					sprite.originX * -sprite.width * this.scale.x,
					sprite.originY * -sprite.height * this.scale.y,
					sprite.width * this.scale.x,
					sprite.height * this.scale.y
				);
			}
		}
		else{
			Renderer.DefaultDraw(this.gameObject);
		}
	}

	private DrawTintedImage(sprite: Sprite, colorizer: SpriteTintInfo) {
		//1. Prepare the tint buffer canvas
		bufferCtx.save();

		//2. Clear the tint buffer. Fill it with the tint color
		bufferCanvas.width = sprite.width || 1;
		bufferCanvas.height = sprite.height || 1;
		//bufferCtx.clearRect(0, 0, bufferCanvas.width, bufferCanvas.height);
		
		if(colorizer.color instanceof Vector){
			const color = colorizer.color;
			bufferCtx.fillStyle = `rgb(${color.x}, ${color.y}, ${color.z})`;
		}
		else if(typeof colorizer.color === "string"){
			bufferCtx.fillStyle = colorizer.color;
		}
		else{
			bufferCtx.fillStyle = "#FFF";
		}

		bufferCtx.fillRect(0, 0, bufferCanvas.width, bufferCanvas.height);
		
		//3. Multiply the image with the drawn tint color
		if (colorizer.tintType === SpriteTintType.Tint) {
			bufferCtx.globalCompositeOperation = colorizer.operation;
			bufferCtx.drawImage(
				sprite.texture,
				sprite.sourceX,
				sprite.sourceY,
				sprite.width,
				sprite.height,
				0,
				0,
				sprite.width,
				sprite.height);
		}
		
		//4. Mask out the parts we actually want. 
		bufferCtx.globalCompositeOperation = "destination-atop";
		bufferCtx.drawImage(
			sprite.texture,
			sprite.sourceX,
			sprite.sourceY,
			sprite.width,
			sprite.height,
			0,
			0,
			sprite.width,
			sprite.height
		);
		
		//The tinted image is ready. Draw it from the canvas.
		ctx.drawImage(bufferCanvas, sprite.originX * -sprite.width * this.scale.x, sprite.originY * -sprite.height * this.scale.y, sprite.width * this.scale.x, sprite.height * this.scale.y);
		
		bufferCtx.restore();
	}
}
import { Component } from "../GameObjects/Component";
import { ctx } from "../System/Canvas";
import { MathX } from "../Math/MathX";
import { Camera2D } from "./Camera2D";
import { GameObject } from "../GameObjects/GameObject";
import { Vector } from "../Math/Vector";

export class Renderer extends Component implements ITogglable{
	imageSmoothingEnabled: boolean;
	_enabled: boolean;

	constructor(gObj: GameObject, name: string = ""){
		super(gObj, name);
		this.imageSmoothingEnabled = false;
		this._enabled = true;
	}

	//Identify Class by Name
	GetClassName() { return "Renderer"; }

	_Initialize(){
		Camera2D.AddRenderer(this);
	}

	Draw(){
		Renderer.DefaultDraw(this.gameObject);
	}

	IsVisible(){
		const scale = this.gameObject.transform.scale;
		const tooSmall = Math.abs(Math.min(scale.x, scale.y)) <= 0.0001 * Camera2D.cameraScale;
		const invisible = tooSmall;
		//if(invisible) console.log(`Object too small to render`, this);
		return !invisible;
	}

	BeforeDestroyImmediate(){
		super.BeforeDestroyImmediate();
		Camera2D.RemoveRenderer(this);
	}

	//Toggle
	get isEnabled() { return this._enabled; }

	Enable() {
		if (!this._enabled) {
			this._enabled = true;
			//Camera2D.AddRenderer(this);
		}
	}

	Disable() {
		if (this._enabled) {
			this._enabled = false;
			//Camera2D.RemoveRenderer(this);
		}
	}

	static DefaultDraw(gObj: GameObject){
		ctx.save();

		const position = Camera2D.GetTransformRenderPosition(gObj.transform);
		let size = 50;
		ctx.translate(position.x, position.y);

		let x = -size / 2;
		let y = -size / 2;

		//Draw a red circle and X
		ctx.strokeStyle = "#F00";
		ctx.lineWidth = 1;
		ctx.beginPath();
		ctx.arc(0, 0, size / 4, 0, MathX.PI2);
		ctx.moveTo(x, y);
		ctx.lineTo(-x, -y);
		ctx.moveTo(-x, y);
		ctx.lineTo(x, -y);
		ctx.stroke();

		//Stroke a white box around the circle and X.
		ctx.strokeStyle = "#FFF";
		ctx.strokeRect(x, y, size, size);

		ctx.restore();
	}
}

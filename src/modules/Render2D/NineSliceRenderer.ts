import { Renderer } from "./Renderer";
import { GameObject } from "../GameObjects/GameObject";
import { Sprite } from "./Sprite";
import { Camera2D } from "./Camera2D";
import { ctx } from "../System/Canvas";
import { Vector } from "../Math/Vector";
import { Debug } from "../System/Debug";
import { Transform } from "../GameObjects/Transform";

export class NineSliceRenderer extends Renderer{
	texture: HTMLImageElement | null;
	spriteWidth: number;
	spriteHeight: number;

	topEdgeSize: number;
	bottomEdgeSize: number;
	leftEdgeSize: number;
	rightEdgeSize: number;

	originX: number;
	originY: number;

	drawCorners: boolean;
	drawSides: boolean;
	drawCenter: boolean;

	drawDebugLines: boolean;

	constructor(gObj: GameObject, name: string = "") {
		super(gObj, name);
		this.texture = null;
		this.spriteWidth = 0;
		this.spriteHeight = 0;
		this.originX = 0;
		this.originY = 0;
		this.topEdgeSize = this.bottomEdgeSize = this.leftEdgeSize = this.rightEdgeSize = 0;
		this.drawDebugLines = false;
		this.drawCorners = this.drawSides = this.drawCenter = true;
	}

	//Identify Class by Name
	GetClassName() { return "NineSliceRenderer"; }

	Draw(){
		if(this.texture){
			if (!this.IsVisible()) return;

			const transform = this.gameObject.transform;
			const position = Camera2D.GetTransformRenderPosition(transform);
			ctx.imageSmoothingEnabled = this.imageSmoothingEnabled;

			ctx.translate(position.x - this.spriteWidth * this.originX, position.y - this.spriteHeight * this.originY);
			ctx.rotate(transform.getAngle2d);
			ctx.scale(transform.scale.x, transform.scale.y);

			const texture = this.texture;
			
			//Local data
			const width = texture.width;
			const height = texture.height;
			const dU = Math.max(0, this.topEdgeSize);
			const dD = Math.max(0, this.bottomEdgeSize);
			const dL = Math.max(0, this.leftEdgeSize);
			const dR = Math.max(0, this.rightEdgeSize);

			const widthdR = Math.max(0, width - dR);
			const heightdD = Math.max(0, height - dD);

			const widthdLR = Math.max(0, width - dL - dR);
			const heightdUD = Math.max(0, height - dU - dD);

			//Canvas
			const widthX = Math.max(0, this.spriteWidth);
			const heightX = Math.max(0, this.spriteHeight);

			const widthdRX = Math.max(0, widthX - dR);
			const heightdDX = Math.max(0, heightX - dD);

			const widthdLRX = Math.max(0, widthX - dL - dR);
			const heightdUDX = Math.max(0, heightX - dU - dD);
			
			//Corners
			this.DrawBoxCorners(dU, dL, texture, dR, widthdR, widthdRX, dD, heightdD, heightdDX);
			
			//Edges
			this.DrawBoxSides(texture, dL, widthdLR, dU, widthdLRX, heightdD, dD, heightdDX, heightdUD, heightdUDX, widthdR, dR, widthdRX);

			//Center
			this.DrawBoxCenter(texture, dL, dU, widthdLR, heightdUD, widthdLRX, heightdUDX);

			//Debug Lines
			this._DrawDebugLines(transform, dU, widthX, heightdDX, dL, heightX, widthdRX);
			
		}
		else{
			Renderer.DefaultDraw(this.gameObject);
		}
	}

	private _DrawDebugLines(transform: Transform, dU: number, widthX: number, heightdDX: number, dL: number, heightX: number, widthdRX: number) {
		if (this.drawDebugLines) {
			ctx.save();
			ctx.strokeStyle = "#0F0";
			ctx.lineWidth = 1 / transform.scale.x;
			ctx.beginPath();
			ctx.moveTo(0, dU);
			ctx.lineTo(widthX, dU);
			ctx.moveTo(0, heightdDX);
			ctx.lineTo(widthX, heightdDX);
			ctx.moveTo(dL, 0);
			ctx.lineTo(dL, heightX);
			ctx.moveTo(widthdRX, 0);
			ctx.lineTo(widthdRX, heightX);
			ctx.rect(0, 0, widthX, heightX);
			ctx.closePath();
			ctx.stroke();
			ctx.restore();
		}
	}

	private DrawBoxCenter(texture: HTMLImageElement, dL: number, dU: number, widthdLR: number, heightdUD: number, widthdLRX: number, heightdUDX: number) {
		if (this.drawCenter) {
			//Center
			ctx.drawImage(texture, dL, dU, widthdLR, heightdUD, dL, dU, widthdLRX, heightdUDX);
		}
	}

	private DrawBoxSides(texture: HTMLImageElement, dL: number, widthdLR: number, dU: number, widthdLRX: number, heightdD: number, dD: number, heightdDX: number, heightdUD: number, heightdUDX: number, widthdR: number, dR: number, widthdRX: number) {
		if (this.drawSides) {
			//Top Edge
			ctx.drawImage(texture, dL, 0, widthdLR, dU, dL, 0, widthdLRX, dU);
			//Bottom Edge
			ctx.drawImage(texture, dL, heightdD, widthdLR, dD, dL, heightdDX, widthdLRX, dD);
			//Left Edge
			ctx.drawImage(texture, 0, dU, dL, heightdUD, 0, dU, dL, heightdUDX);
			//Right Edge
			ctx.drawImage(texture, widthdR, dU, dR, heightdUD, widthdRX, dU, dR, heightdUDX);
		}
	}

	private DrawBoxCorners(dU: number, dL: number, texture: HTMLImageElement, dR: number, widthdR: number, widthdRX: number, dD: number, heightdD: number, heightdDX: number) {
		if (this.drawCorners) {
			//Top Left Corner
			if (dU > 0 && dL > 0) {
				ctx.drawImage(texture, 0, 0, dL, dU, 0, 0, dL, dU);
			}
			//Top Right Corner
			if (dU > 0 && dR > 0) {
				ctx.drawImage(texture, widthdR, 0, dR, dU, widthdRX, 0, dR, dU);
			}
			//Bottom Left Corner
			if (dD > 0 && dL > 0) {
				ctx.drawImage(texture, 0, heightdD, dL, dD, 0, heightdDX, dL, dD);
			}
			//Bottom Right Corner
			if (dD > 0 && dR > 0) {
				ctx.drawImage(texture, widthdR, heightdD, dR, dD, widthdRX, heightdDX, dR, dD);
			}
		}
	}
}
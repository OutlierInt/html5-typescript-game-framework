import { Renderer } from "./Renderer";
import { canvas, ctx } from "../System/Canvas";
import { Vector } from "../Math/Vector";
import { Transform } from "../GameObjects/Transform";

export let renderers: Array<Renderer> = [];

export class Camera2D {
	static cameraPosition = Vector.zero;
	static cameraRotation = 0;
	static cameraScale = 1;

	static Draw() {
		ctx.save();

		//Rotate and Scale Camera from the center
		this.RotateScaleCanvas(canvas, ctx);

		//Draw everything at once.
		for (let obj of renderers) {
			ctx.save();
			if(obj.isEnabled){
				obj.Draw();
			}
			ctx.restore();
		}

		ctx.restore();
	}

	static RotateScaleCanvas(canvas: HTMLCanvasElement, ctx: CanvasRenderingContext2D){
		ctx.translate(canvas.width / 2, canvas.height / 2);
    ctx.rotate(-this.cameraRotation);
    ctx.scale(1 / this.cameraScale, 1 / this.cameraScale);
    ctx.translate(canvas.width / -2, canvas.height / -2);
	}

	static ClearCanvas(clearColor: string = "#000") {
		ctx.fillStyle = clearColor;
		ctx.fillRect(0, 0, canvas.width, canvas.height);
	}

	static AddRenderer(rend: Renderer) {
		renderers.push(rend);
	}

	static RemoveRenderer(rend: Renderer) {
		renderers = renderers.filter(r => r != rend);
	}

	static GetTransformRenderPosition(transform: Transform){
		return transform.position.subtract(Camera2D.cameraPosition);
	}

	static TransformPosition(v: Vector){
		/*
		const offset = child
			.localPosition
			.Clone()
			.scaleVolatile(child.scale)
			.rotate2DVolatile(child.getAngle2d);
			child.position = this.position.add(offset);
		*/

		/*
			ctx.translate(canvas.width / 2, canvas.height / 2);
			ctx.rotate(-this.cameraRotation);
			ctx.scale(1 / this.cameraScale, 1 / this.cameraScale);
			ctx.translate(canvas.width / -2, canvas.height / -2);
		*/

		//return v.Clone()
		//.subtractVolatile(Camera2D.cameraPosition)

		return v
      .Clone()
      .subtractVolatile(Camera2D.cameraPosition)
      .subtractVolatile(new Vector(canvas.width / 2, canvas.height / 2))
      .divideVolatile(this.cameraScale)
      .rotate2DVolatile(-this.cameraRotation)
      .addVolatile(new Vector(canvas.width / 2, canvas.height / 2));
	}
}

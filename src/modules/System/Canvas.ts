export let canvasContainer: HTMLElement;
export let canvas: HTMLCanvasElement;
export let ctx: CanvasRenderingContext2D;

let initialized: boolean = false;

/**
 * @description Sets up a canvas and rendering context.
 * @returns False if an error occurred. Otherwise, True.
 */
export function initializeCanvas(){
	//If the canvas is ready, quit.
	if(initialized) return;

	//Set up a canvas
	canvasContainer = document.getElementById("gameContainer") as HTMLElement;

	//const canvasContainer = document.createElement("div");
	//canvasContainer.setAttribute("style", "position:relative; overflow: hidden;");
	//document.body.appendChild(canvasContainer);

	//canvas = document.createElement("canvas");

	//TODO: Add canvas generating code here if this is null
	canvas = document.getElementById("gameCanvas") as HTMLCanvasElement;
	const context = canvas.getContext("2d");
	if (context) ctx = context;

	//Set up canvas
	canvas.width = 1280;
	canvas.height = 720;
	canvasContainer.setAttribute("style", `width:${canvas.width}px; height:${canvas.height}px`);

	ctx.fillStyle = "#303";
	ctx.fillRect(0, 0, canvas.width, canvas.height);

	//Add the canvas
	//canvasContainer.appendChild(canvas);

	//This canvas is ready!
	initialized = true;
}

//Side Effect
initializeCanvas();

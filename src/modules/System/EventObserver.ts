export class EventObserver{
	private subscribers: Map<object, IVoidFunction>;

	constructor(){
		this.subscribers = new Map();
	}

	FireEvent(){
		for (const event of this.subscribers.values()) {
			event();
		}
	}

	Subscribe(obj: object, cb: IVoidFunction)	{
		this.subscribers.set(obj, cb);
	}
	
	Unsubscribe(obj: object) {
		this.subscribers.delete(obj);
	}
}

interface IVoidFunction{
	(): void;
}
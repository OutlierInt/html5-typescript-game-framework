export const assetCache = new Map<string, any>();

export class AssetManager {
	static assetCache = assetCache;
	
	private static _nullImage: any = null;
	private static _nullAudio: any = null;

	//Null Image is a single white pixel.
	static get nullImage() {
		if (this._nullImage) return this._nullImage;

		//Lazy Generate.
		const nullImage = new Image();
		nullImage.src = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAIAAACQd1PeAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAMSURBVBhXY/j//z8ABf4C/qc1gYQAAAAASUVORK5CYII=";
		this._nullImage = nullImage;
		return this._nullImage;
	}

	//Null Audio is a wav file of 0.01sec of silence.
	static get nullAudio() {
		if (this._nullAudio) return this._nullAudio;

		//Lazy Generate
		const nullAudio = document.createElement("audio");
		nullAudio.src = "data:audio/wav;base64,UklGRpYDAABXQVZFZm10IBAAAAABAAEARKwAAIhYAQACABAAZGF0YXIDAAAAAAEA/v8CAP//AAABAP//AAAAAP//AgD+/wMA+/8GAPr/BAD//wAAAQD//wAAAAACAPz/BQD8/wEAAwD5/wgA+v8CAAIA/f8BAAEA/v8DAP3/AwD+/wEAAAD+/wIAAAD//wIA/P8EAPz/BAD+/wAAAAAAAAAAAAAAAAAAAAAAAAEA/f8EAPv/BQD9/wEAAAD//wEAAAD//wIA/f8EAPz/AwD/////AQAAAAAAAAABAP7/AQACAP3/AgD/////AwD9/wIA/v8BAP//AwD8/wMA/v8BAAEA/v8CAP7/AgD+/wMA/P8EAP3/AgD//wAAAQD+/wMA/f8CAAAA/f8EAP7/AAABAP7/AgABAP3/BAD7/wYA+/8EAP3/AQAAAAEA/v8CAP7/AQAAAP//AQAAAAEA/v8BAAAAAAAAAAEA/f8EAP3/AQAAAAAAAAAAAAEA/f8GAPn/BgD7/wQA/f8DAP3/AwD9/wIA/v8CAAAA/v8DAP3/AQACAPz/AwD//wAAAAAAAP//AgD+/wIA/v8BAAEA/f8EAPz/BQD6/wYA+f8IAPr/BQD6/wYA+/8FAPv/BQD5/wkA9/8HAPv/AwD//wAAAQD8/wUA/f8BAAEA/f8DAP3/BAD7/wYA+v8FAP3/AQABAP3/AwD///7/BgD3/wkA+P8FAP///v8DAP3/AgD//wEA/v8DAPz/BQD8/wMA/v8BAP//AQAAAP//AQD//wEA//8BAP//AAACAP3/AgAAAP//AQAAAP//AQD//wEAAAAAAP//AQD+/wMA/P8FAPv/AwD//wAAAQD+/wMA/P8FAPz/AgD//wAA//8DAP3/AgD+/wEAAAAAAAAA//8BAP//AQD/////AwD8/wMA/v8BAAAAAAD//wEAAQD+/wEAAAD+/wQA/P8CAAAA/f8FAPv/AwAAAP7/AgAAAP3/BAD9/wEAAQD//wAA//8BAAAAAAABAP3/AwD+/wEA//8BAP//AQD//wEA//8BAP//AQD//wEA//8BAAAA/v8DAP3/AgD//wAAAAABAP7/AgD//wAAAQD+/wMA/P8FAPr/BQD+//7/BQD6/wUA/v///wIA//8AAAEAAAD+/wMA/f8CAP//AQAAAP3/BAD8/wMAAAD+/wIA//8AAAEAAAD//wIA/f8CAAAA//8CAP7/AAAAAAEA//8=";
		this._nullAudio = nullAudio;
		return this._nullAudio;
	}

	//Batched Loading
	static LoadThingsPromise(urls: string[], batchCallback?: (batch: BatchData) => void) {
		return new Promise(function(resolve){
			if(urls.length <= 0) resolve();

			console.log("Loading a batch of assets!");
			const loadBatch = new BatchData(urls.length, resolve);
			
			//Use this callback to give access to the batch data object
			if(batchCallback) batchCallback(loadBatch);

			for (let i = 0; i < urls.length; i++) {
				const url = urls[i];

				if (!assetCache.has(url)) {
					const fileType = AssetManager.getAssetTypeFromExtension(url);
					switch (fileType) {
						case AssetType.Image: AssetManager._loadTextureBatched(loadBatch, url); break;
						case AssetType.Audio: AssetManager._loadAudioBatched(loadBatch, url); break;
						case AssetType.Script: AssetManager._loadScriptBatched(loadBatch, url); break;
						case AssetType.Json: AssetManager._loadJSONBatched(loadBatch, url); break;

						case AssetType.Error:
						default:
							AssetManager._onBatchedThingLoadFailureCallback(loadBatch, url); break;
					}
				} else {
					AssetManager._onBatchedThingLoadedCallback(assetCache.get(url), loadBatch, url);
				}
			}
		});
	}

	private static _onBatchedThingLoadedCallback(asset: any, batch: BatchData, url: string) {
		batch.ItemLoadSuccess(url);
		assetCache.set(url, asset);
		//console.log("Loaded '%s'... Progress... %d / %d (%d%)", url, batch.count, batch.total, batch.count / batch.total * 100);
		AssetManager._onBatchComplete(batch);
	}

	private static _onBatchedThingLoadFailureCallback(batch: BatchData, url: string) {
		batch.ItemLoadFailure(url);
		console.error("Failure to load %s", url);
		
		AssetManager._onBatchComplete(batch);
	}

	private static _onBatchComplete(batch: BatchData) {
		if (batch.count === batch.total) {
			console.log("Loading Complete in %d ms", Date.now() - batch.startTime);
			if(batch.failedUrls.length > 0) console.warn("Failed to load:\n", batch.failedUrls);
			batch.callback();
		}
	}

	//Batched Asset Loading Implementations
	private static _loadTextureBatched(loadBatch: BatchData, url: string) {
		const img = new Image();
		img.onload = ev => AssetManager._onBatchedThingLoadedCallback(img, loadBatch, url);
		img.onerror = ev => AssetManager._onBatchedThingLoadFailureCallback(loadBatch, url);
		img.src = url;
	}

	private static _loadAudioBatched(loadBatch: BatchData, url: string) {
		const audio = new Audio();
		audio.onloadeddata = ev => AssetManager._onBatchedThingLoadedCallback(audio, loadBatch, url);
		audio.onerror = ev => AssetManager._onBatchedThingLoadFailureCallback(loadBatch, url);
		audio.src = url;
	}

	private static _loadScriptBatched(loadBatch: BatchData, url: string) {
		const fileref = document.createElement("script");
		fileref.setAttribute("type", "text/javascript");
		fileref.onload = ev => AssetManager._onBatchedThingLoadedCallback(fileref, loadBatch, url);
		fileref.onerror = ev => AssetManager._onBatchedThingLoadFailureCallback(loadBatch, url);
		fileref.setAttribute("src", url);
		document.head.appendChild(fileref);
	}

	private static _loadJSONBatched(loadBatch: BatchData, url: string) {
		const xhr = new XMLHttpRequest();
		xhr.open("GET", url, true);
		xhr.responseType = "json";
		xhr.onload = function (ev) {
			if (this.status === 200) {
				AssetManager._onBatchedThingLoadedCallback(this.response, loadBatch, url);
			}
		};
		xhr.onerror = ev => AssetManager._onBatchedThingLoadFailureCallback(loadBatch, url);
		xhr.send();
	}

	//Single Asset Loading
	private static _loadTexture(url: string, onSuccess: Function, onError: Function) {
		const img = new Image();
		img.onload = ev => onSuccess(img);
		img.onerror = ev => onError(ev);
		img.src = url;
	}

	private static _loadAudio(url: string, onSuccess: Function, onError: Function) {
		const audio = new Audio();
		audio.onloadeddata = ev => onSuccess(audio);
		audio.onerror = ev => onError(ev);
		audio.src = url;
	}

	private static _loadScript(url: string, onSuccess: Function, onError: Function) {
		const fileref = document.createElement("script");
		fileref.setAttribute("type", "text/javascript");
		fileref.onload = ev => onSuccess(fileref);
		fileref.onerror = ev => onError(ev);
		fileref.setAttribute("src", url);
		document.head.appendChild(fileref);
	}

	private static _loadJSON(url: string, onSuccess: Function, onError: Function) {
		const xhr = new XMLHttpRequest();
		xhr.open("GET", url, true);
		xhr.responseType = "json";
		xhr.onload = function (ev) {
			if (this.status === 200) {
				onSuccess(this.response);
			}
		};
		xhr.onerror = ev => onError(ev);
		xhr.send();
	}

	//Helpers
	static getAssetTypeFromExtension(url: string) {
		if (url.indexOf(".jpg") != -1 || url.indexOf(".jpeg") != -1 || url.indexOf(".png") != -1) {
			return AssetType.Image;
		}
		if (url.indexOf(".wav") != -1 || url.indexOf(".mp3") != -1 || url.indexOf(".ogg") != -1) {
			return AssetType.Audio;
		}
		if (url.indexOf(".js") != -1) {
			return AssetType.Script;
		}
		if (url.indexOf(".json") != -1) {
			return AssetType.Json;
		}

		return AssetType.Error;
	}

	//Load Assets that were not initially loaded
	static LoadTexturePromise(url: string){
		return new Promise(function(resolve, reject){
			const img = new Image();
			img.onload = ev => resolve(img);
			img.onerror = ev => reject(ev);
			img.src = url;
		});
	}

	static LoadAudioPromise(url: string){
		return new Promise(function(resolve, reject){
			const audio = new Audio();
			audio.onloadeddata = ev => resolve(audio);
			audio.onerror = ev => reject(ev);
			audio.src = url;
		});
	} 

	static LoadJSONPromise(url: string) {
		return new Promise((resolve, reject) => {
			const xhr = new XMLHttpRequest();
			xhr.open("GET", url, true);
			xhr.responseType = "json";
			xhr.onload = function (ev) {
				if (this.status === 200) {
					resolve(this.response);
				}
			};
			xhr.onerror = function (ev) {
				reject(ev.error);
			}
			xhr.send();
		});
	}

	//Load Cached Assets
	static HasAsset(url: string){
		return assetCache.has(url);
	}

	static TryGetTexture(url: string): HTMLImageElement{
		return AssetManager.HasAsset(url) ? assetCache.get(url) : this.nullImage;
	}

	static TryGetAudio(url: string): HTMLAudioElement {
		return AssetManager.HasAsset(url) ? assetCache.get(url) : this.nullAudio;
	}

	static TryGetJSON(url: string) {
		return AssetManager.HasAsset(url) ? assetCache.get(url) : {};
	}

	static TryGetScript(url: string) {
		return AssetManager.HasAsset(url) ? assetCache.get(url) : null;
	}

	static TryGetAsset(url: string){
		const hasAsset = assetCache.has(url);
		if(hasAsset) return assetCache.get(url)
		
		//Asset not available. Take this default asset.
		const type = this.getAssetTypeFromExtension(url);
		return AssetManager.GetDefaultAsset(type);
	}

	static GetDefaultAsset(type: AssetType){
		switch (type) {
			case AssetType.Image: return AssetManager.nullImage;
			case AssetType.Audio: return AssetManager.nullAudio;
			case AssetType.Script: return null;
			case AssetType.Json: return {};
			default: return null;
		}
	}
}

enum AssetType{
	Image,
	Audio,
	Script,
	Json,
	Error,
}

export class BatchData {
  count: number;
  failedUrls: string[];
  total: number;
	callback: Function;
  startTime: number;

  constructor(total: number, callback: Function) {
    this.count = 0;
    this.failedUrls = [];
    this.total = total;
    this.callback = callback;
    this.startTime = Date.now();
	}
	
	ItemLoadSuccess(url?: string){
		this.count++;
	}

	ItemLoadFailure(url: string){
		this.count++;
		this.failedUrls.push(url);
	}
}

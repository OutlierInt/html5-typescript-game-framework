import { EventObserver } from "./EventObserver";

document.addEventListener("visibilitychange", function(ev){
	if(document.hidden){
		console.log("[Game Suspended]");
		WindowVisibilityManager.onWindowHidden.FireEvent();
	}	
	else{
		console.log("[Game Resumed]");
		WindowVisibilityManager.onWindowVisible.FireEvent();
	}
});

export class WindowVisibilityManager {
	static onWindowHidden = new EventObserver();
	static onWindowVisible = new EventObserver();
}

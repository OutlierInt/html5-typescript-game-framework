let prevTimeStamp = 0;
let fps = 1;

let deltaTimeAccumulator = 0;

export class Time {
	static frameCount: number = 0;
	static time: number = 0;
	static deltaTime: number = 0;
	static fixedDeltaTime: number = 1/50;
	static readonly defaultFixedTime = 1/50;
	static realTime: number = 0;
	static realDeltaTime: number = 0;
	static timescale: number = 1;
	static MAXIMUM_DELTATIME = 1/3;

	static get fps() { return fps; }
	static get fixedDeltaTimeAlpha() { return Math.min(Math.max(deltaTimeAccumulator / Time.fixedDeltaTime, 0), 1); }
	/**
	 * @description Calculates time values when the next frame is starts. Run this at the beginning of the update loop.
	 * @param timestamp 
	 */
	static calculateNextFrameTime(timestamp: number) {
		Time.realDeltaTime = (timestamp - prevTimeStamp) / 1000;
		let clampedDeltaTime = Math.min(Math.max(Time.realDeltaTime, 0), Time.MAXIMUM_DELTATIME);
		Time.deltaTime = clampedDeltaTime * Time.timescale;
		deltaTimeAccumulator += clampedDeltaTime * Time.timescale;//Time.deltaTime;
		//console.log(`%cFrame:   ${deltaTimeAccumulator}`, "background: #0F0; color: #000");
		Time.frameCount++;
		Time.time += Time.deltaTime;
		Time.realTime = timestamp / 1000;//Time.getRealTimeSinceStartup();
		fps = (0.9 * fps) + (0.1 * (1 / (Time.realDeltaTime || 1000)));
		prevTimeStamp = timestamp;
	}
}

export function FixedUpdateLoop(FixedUpdateCallback: Function){
	if(Time.fixedDeltaTime <= 0) return;
	while (deltaTimeAccumulator >= Time.fixedDeltaTime) {
		//console.log(`%cRunning: ${deltaTimeAccumulator.toFixed(4)}`, "background: #0FF; color: #000");
		FixedUpdateCallback();
		deltaTimeAccumulator -= Time.fixedDeltaTime;
	}
	//console.log(`%cReset:   ${deltaTimeAccumulator.toFixed(4)}`, "background: #F00; color: #000");
}

import { Vector } from "../Math/Vector";
import { ctx, canvas } from "./Canvas";
import { Camera2D } from "../Render2D/Camera2D";

const defaultPosition = new Vector(5, 25);
const printCommands: Function[] = [];
const drawLineCommands: Function[] = [];

export class Debug{
	static enablePrint = true;
	static enableDrawLine = true;

	static print(text: string, position: Vector = defaultPosition) {
		if (this.enablePrint) printCommands.push(printHandler.bind(this, text, position));
	}

	static DrawLine(p0: Vector, p1: Vector, strokeStyle = "#FFF") {
		if (this.enableDrawLine) drawLineCommands.push(drawlineHandler.bind(this, p0, p1, strokeStyle));
	}

	static handleDebugCommands(){
		if (!this.enablePrint) printCommands.length = 0;
		while(printCommands.length > 0){
			(printCommands.pop() as Function)();
		}

		if (!this.enableDrawLine) drawLineCommands.length = 0;
		while(drawLineCommands.length > 0){
			(drawLineCommands.pop() as Function)();
		}
	}
}

function printHandler(text: string, position: Vector = defaultPosition) {
	ctx.save();
	ctx.font = "20px Arial";
	ctx.fillStyle = "#FFF";
	ctx.fillText(text, position.x, position.y);
	ctx.restore();
}

function drawlineHandler(p0: Vector, p1: Vector, strokeStyle = "#FFF"){
	ctx.save();
	
	ctx.beginPath();
	ctx.strokeStyle = strokeStyle;
	ctx.lineWidth = 1;

	p0 = Camera2D.TransformPosition(p0);
	p1 = Camera2D.TransformPosition(p1);

	ctx.moveTo(p0.x, p0.y);
	ctx.lineTo(p1.x, p1.y);
	
	ctx.stroke();
	ctx.closePath();

	ctx.restore();
}
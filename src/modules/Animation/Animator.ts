import { Component } from "../GameObjects/Component";
import { GameObject } from "../GameObjects/GameObject";

export const AnimatorQueue: Set<any> = new Set();

export class Animator extends Component{
	constructor(gObj: GameObject, name: string = ""){
		super(gObj, name);
	}

	//Identify Class by Name
	GetClassName() { return "Animator"; }

	_Initialize(){
		Animator.AddAnimatorToSet(this);
	}

	BeforeDestroyImmediate(){
		super.BeforeDestroyImmediate();
		Animator.RemoveAnimatorFromSet(this);
	}

	UpdateAnimations(){

	}
	
	static AddAnimatorToSet(anim: any){
		AnimatorQueue.add(anim);
	}

	static RemoveAnimatorFromSet(anim: any) {
		AnimatorQueue.delete(anim);
	}
}

export function HandleAnimationUpdates(){
	for (const animator of AnimatorQueue) {
		animator.UpdateAnimations();
	}
}
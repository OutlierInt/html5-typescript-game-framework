let _keysDown = new Set<number>();
let _pressed = new Set<number>();
let _released = new Set<number>();

//When a key is pressed, add it to the input dictionary
function _onKeyDown(event: KeyboardEvent) {
	//event.preventDefault();
	if(!event.repeat){
		_keysDown.add(event.keyCode);//_keysDown.[event.keyCode] = true;
		_pressed.add(event.keyCode);//_pressed.[event.keyCode] = true;
	}
}

//When a key is released, remove it from the dictionary.
function _onKeyUp(event: KeyboardEvent) {
	//event.preventDefault();
	_keysDown.delete(event.keyCode);//delete _keysDown[event.keyCode];
	_released.add(event.keyCode);//_released[event.keyCode] = true;
}

//Setup Keyboard Listener
document.addEventListener("keydown", _onKeyDown, false);
document.addEventListener("keyup", _onKeyUp, false);

export class Keyboard{
	static GetKey(keyCode: number): boolean { return _keysDown.has(keyCode) || false; }
	static GetKeyDown(keyCode: number): boolean { return _pressed.has(keyCode) || false; }
	static GetKeyUp(keyCode: number): boolean { return _released.has(keyCode) || false; }

	//When the game updates, remove the pressed and released keys for the next frame.
	static LateUpdate() {
		_pressed.clear(); //if (_pressed) _pressed = {};
		_released.clear(); //if (_released) _released = {};
	}
}

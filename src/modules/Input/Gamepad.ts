//Gamepad Detection
window.addEventListener("gamepadconnected", function(ev: any){
	const gamepads = navigator.getGamepads();
	const gamepad = gamepads[ev.gamepad.index] as Gamepad;

	console.log(`Gamepad Connected!
	Index: %s
	ID: %s
	Axes: %d
	Buttons: %d
	Mapping: %s
	Display Id: %d`,
	gamepad.index, gamepad.id, gamepad.axes.length, gamepad.buttons.length, gamepad.mapping, gamepad.displayId );

	console.dir(gamepad);
	console.dir(gamepads);
});

window.addEventListener("gamepaddisconnected", function (ev: any) {
	console.log(`Gamepad Disconnected! Index: %s, ID: %s`, ev.gamepad.id, ev.gamepad.index);
	const gamepads = navigator.getGamepads();
	console.dir(gamepads);
});

//Updating
export function LateUpdate() {

}

export class GamePadInput {
	//Buttons
	GetButton() {

	}
	GetButtonDown() {

	}
	GetButtonUp() {

	}

	//Axes
	GetAxis() {

	}
}

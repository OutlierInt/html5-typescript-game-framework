import { canvas } from "../System/Canvas";
import { Vector } from "../Math/Vector";

let _mouseButtonsDown = new Set<number>();
let _mouseButtonsPressed = new Set<number>();
let _mouseButtonsReleased = new Set<number>();

let mousePrev = new Vector(0, 0);
let mousePosition = new Vector(0, 0);
let mouseDelta = new Vector(0, 0);

function _onMouseOver(event: MouseEvent) {
	const rect = canvas.getBoundingClientRect();
	mousePrev = mousePosition;
	mousePosition.Set(event.clientX - rect.left, event.clientY - rect.top, 0, 0);
	mouseDelta.Set(mousePrev.x - mousePosition.x, mousePrev.y - mousePosition.y, 0, 0);
}

function _onMouseDown(event: MouseEvent) {
	_mouseButtonsDown.add(event.button);//_mouseButtonsDown[event.button] = true;
	_mouseButtonsPressed.add(event.button);//_mouseButtonsPressed[event.button] = true;
}

function _onMouseUp(event: MouseEvent) {
	_mouseButtonsDown.delete(event.button);//delete _mouseButtonsDown[event.button];
	_mouseButtonsReleased.add(event.button);//_mouseButtonsReleased[event.button] = true;
}

//Setup Mouse Listener
canvas.addEventListener("mousemove", _onMouseOver, false);
canvas.addEventListener("mousedown", _onMouseDown, false);
canvas.addEventListener("mouseup", _onMouseUp, false);

export class Mouse {
	static get mousePosition()	{ return mousePosition.Clone(); }
	static get mouseDelta()			{ return mouseDelta.Clone(); }

	static GetMouse(button: number): boolean { return _mouseButtonsDown.has(button) || false; }
	static GetMouseDown(button: number): boolean { return _mouseButtonsPressed.has(button) || false; }
	static GetMouseUp(button: number): boolean { return _mouseButtonsReleased.has(button) || false; }

	//When the game updates, remove the pressed and released mouse buttons for the next frame.
	static LateUpdate() {
		_mouseButtonsPressed.clear();//if (_mouseButtonsPressed) _mouseButtonsPressed = {};
		_mouseButtonsReleased.clear();//if (_mouseButtonsReleased) _mouseButtonsReleased = {};
	}
}

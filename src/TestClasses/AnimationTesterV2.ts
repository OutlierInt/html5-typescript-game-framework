import { GameObject } from "../Modules/GameObjects/GameObject";
import { AnimationController, AnimationType } from "../Modules/Animation/AnimationController";
import { AnimationClip } from "../Modules/Animation/AnimationClip";
import { Script } from "../Modules/GameObjects/Script";
import { AnimationCache } from "../Modules/Animation/AnimationCache";

export class AnimationTesterV2 extends Script {
	animGameObject: GameObject | null = null;
	animController = new AnimationController();
	cache: any;

	Setup(gObj: GameObject){
		this.animGameObject = gObj;
    this.animController = new AnimationController();

		AnimationClip.CreateAnimationClipFromFile("test-animation.json", "Test Animation", (clip) =>{
			const controller = this.animController;
			controller.clip = clip;
			controller.gameObject = this.animGameObject as GameObject;
			controller.animationType = AnimationType.PingPong;
			//controller.repeatCount = 3;
			//controller.speed = 0.25;

			//Test Caching System
			controller.cache = AnimationCache.GenerateAnimationCache(controller.gameObject, clip);

			controller.Play();
		});
	}

	UpdateAnimations(){
		this.animController.UpdateAnimationState();
	}
}

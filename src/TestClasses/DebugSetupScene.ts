import { GameObject } from "../Modules/GameObjects/GameObject";
import { DebugUpdate } from "./DebugUpdate";
import { Vector } from "../Modules/Math/Vector";
import { TiledRenderer } from "../Modules/Render2D/TiledRenderer";
import { AssetManager } from "../Modules/System/AssetManager";
import { ScrollingBackground } from "./ScrollingBackground";
import { SpriteRenderer } from "../Modules/Render2D/SpriteRenderer";
import { Sprite } from "../Modules/Render2D/Sprite";
import { canvas } from "../Modules/System/Canvas";
import { Player } from "./Player";
import { SpriteTintInfo } from "../Modules/Render2D/SpriteTintInfo";
import { BoxCollider2D } from "../Modules/Physics2D/BoxCollider2D";
import { QuickParent } from "./Parenter";
import { Key } from "../Modules/Input/Keys";
import { ParticleSystem } from "../Modules/Render2D/ParticleRenderer";
import { ParticleUpdater } from "./ParticleUpdater";
import { Physics2DPrimitive } from "../Modules/Physics2D/Physics2DPrimitive";
import { PhysicsController } from "./PhysicsController";
import { Collider2D } from "../Modules/Physics2D/Collider2D";
import { Animator } from "../Modules/Animation/Animator";
import { BaseObject } from "../Modules/GameObjects/BaseObject";
import { AnimationTesterV2 } from "./AnimationTesterV2";

export function SetupDebugScene() {
	//Debug Update
	const debugUpdater = new GameObject();
	debugUpdater.AddComponent(DebugUpdate);

	//Background
	const background = new GameObject("BG");
	background.transform.position = Vector.zero;
	const tiledRenderer = background.AddComponent(TiledRenderer);
	const bgTex = AssetManager.TryGetTexture("images/space-bg.jpg");
	tiledRenderer.texture = bgTex;
	tiledRenderer.scale = new Vector(bgTex.width * 10, bgTex.height * 2);
	background.AddComponent(ScrollingBackground);

	//Barrier
	const barrier = new GameObject("Barrier");
	//barrier.transform.position = new Vector(300, canvas.height / 2);

	const barrierSpriteRenderer = barrier.AddComponent(SpriteRenderer);
	barrierSpriteRenderer.scale = new Vector(3, 3);
	//(spriteRenderer.colorizer as SpriteTintInfo).color = "#0FF";
	//(spriteRenderer.colorizer as SpriteTintInfo).operation = "luminosity";
	const bSprite = new Sprite("images/testsheet.png");
	bSprite.sourceX = 149;
	bSprite.sourceY = 335;
	bSprite.width = 73;
	bSprite.height = 32;
	bSprite.originX = 0.6;
	barrierSpriteRenderer.sprite = bSprite;

	//Testing Behaviors
	const player1 = new GameObject("Player");
	player1.transform.position = new Vector(100, canvas.height / 2);
	const playerComp = player1.AddComponent(Player);
	let spriteRenderer = player1.AddComponent(SpriteRenderer);
	spriteRenderer.debugSetup();
	spriteRenderer.scale = new Vector(3, 3);
	(spriteRenderer.colorizer as SpriteTintInfo).color = "#0FF";
	(spriteRenderer.colorizer as SpriteTintInfo).operation = "luminosity";

	//Add a collider
	let col = player1.AddComponent(BoxCollider2D);
	let sprite = spriteRenderer.sprite as Sprite;
	col.Width = sprite.width * spriteRenderer.scale.x;
	col.Height = sprite.height * spriteRenderer.scale.y;
	//col.bodyType = 2;
	//Physics2DUtil.AddBodyToWorld(col);
	//spriteRenderer.Destroy();

	//Parent the barrier
	barrier.transform.SetParent(player1.transform);
	barrier.transform.localPosition = Vector.zero;

	//Options
	{
		const option1 = new GameObject("Option 1");
		option1.transform.SetParent(player1.transform);
		option1.transform.localPosition = new Vector(-50, 75);

		const option2 = new GameObject("Option 2");
		option2.transform.SetParent(player1.transform);
		option2.transform.localPosition = new Vector(-50, -75);

		const option1SpriteRenderer = option1.AddComponent(SpriteRenderer);
		const option2SpriteRenderer = option2.AddComponent(SpriteRenderer);
		option1SpriteRenderer.scale = new Vector(3, 3);
		option2SpriteRenderer.scale = new Vector(3, 3);

		const sprite = new Sprite("images/testsheet.png");
		sprite.sourceX = 230;
		sprite.sourceY = 202;
		sprite.width = 16;
		sprite.height = 11;

		option1SpriteRenderer.sprite = sprite;
		option2SpriteRenderer.sprite = sprite;

		const quickParent = option1.AddComponent(QuickParent);
		quickParent.parent = player1.transform;
		quickParent.key = Key.P;

		const quickParent2 = option2.AddComponent(QuickParent);
		quickParent2.parent = player1.transform;
		quickParent2.key = Key.O;

		playerComp.fireLocations.push(option1.transform);
		playerComp.fireLocations.push(option2.transform);
	}

	//Drawing far away stuff
	/*
	{
		const farDraw = new GameObject("FarDraw");
		farDraw.transform.position = new Vector(canvas.width / 2, canvas.height / 2);
		let spriteRenderer = farDraw.AddComponent(SpriteRenderer);
		spriteRenderer.debugSetup();
		//spriteRenderer.scale = new Vector(100, 100);
		(spriteRenderer.colorizer as SpriteTintInfo).color = "#0FF";
		(spriteRenderer.colorizer as SpriteTintInfo).operation = "luminosity";
		//farDraw.AddComponent(MatrixTester);
		//setInterval(function(){
		//	spriteRenderer.scale = new Vector(Math.sin(Time.realTime) * 10, Math.sin(Time.realTime * 1.5) * 10);
		//	//farDraw.transform.position = new Vector((canvas.width / 2) + 100000 * Math.sin(Time.realTime), canvas.height / 2);
		//}, 1000/60);
	}
	*/

	//Testing Particle Effects
	const particles = new GameObject("Particles");
	//particles.transform.position = new Vector(canvas.width / 2, canvas.height / 2);
	particles.transform.SetParent(player1.transform);
	particles.transform.localPosition = new Vector(50,0);
	particles.transform.localRotation = new Vector(0,0,Math.PI);
	let particleSystem = particles.AddComponent(ParticleSystem);

	let pSprite = new Sprite("images/testparticle.png");
	pSprite.width = 17;
	pSprite.height = 17;
	pSprite.sourceX = 9;
	pSprite.sourceY = 263;
	//pSprite.drawScaleX = 1;
	//pSprite.drawScaleY = 1;
	particleSystem.sprite = pSprite;

	particles.AddComponent(ParticleUpdater);

	//Make container
	Physics2DPrimitive.MakeStaticBox(canvas.width / 2, canvas.height, canvas.width, 20);
	//Physics2DPrimitive.MakeStaticBox(canvas.width/2, 0, canvas.width, 20);
	Physics2DPrimitive.MakeStaticBox(0, canvas.height / 2, 20, canvas.height);
	Physics2DPrimitive.MakeStaticBox(canvas.width, canvas.height / 2, 20, canvas.height);
	Physics2DPrimitive.MakeStaticBox(400, canvas.height - 35, 50, 50);

	//Physics Ball Thrower
	const physCon = new GameObject("Physics Controller")
	physCon.AddComponent(PhysicsController);

	//Animation Tester
	const boxy = Physics2DPrimitive.MakeStaticBox(250, 250, 100, 100);
	boxy.name = "Boxy";
	const boxyBod = (boxy.GetComponent(Collider2D) as Collider2D).body;
	(boxyBod as Box2D.Dynamics.b2Body).SetType(Box2D.Dynamics.b2Body.b2_kinematicBody);

	const boxyChild = Physics2DPrimitive.MakeStaticBox(250, 250, 100, 100);
	boxyChild.name = "BoxyChild";
	const boxyChildBod = (boxyChild.GetComponent(Collider2D) as Collider2D).body;
	(boxyChildBod as Box2D.Dynamics.b2Body).SetType(Box2D.Dynamics.b2Body.b2_kinematicBody);
	boxyChild.transform.SetParent(boxy.transform);
	boxyChild.transform.localPosition = new Vector(0, 0);
	boxyChild.transform.localScale = new Vector(1, 1/4, 1);

	const boxyGrandChild = Physics2DPrimitive.MakeStaticBox(250, 250, 100, 100);
	boxyGrandChild.name = "BoxyGrandChild";
	const boxyGrandChildBod = (boxyGrandChild.GetComponent(Collider2D) as Collider2D).body;
	(boxyGrandChildBod as Box2D.Dynamics.b2Body).SetType(Box2D.Dynamics.b2Body.b2_staticBody);
	boxyGrandChild.transform.SetParent(boxyChild.transform);
	boxyGrandChild.transform.localPosition = new Vector(0, -100);
	boxyGrandChild.transform.localScale = new Vector(1, 1, 1);
	((boxyGrandChild.GetComponent(SpriteRenderer) as SpriteRenderer).colorizer as any).color = "#FF0";

	//const animTester = new AnimationTester(boxy);
	const animTesterGameObject = new GameObject("Animation Tester");
	const animTester = animTesterGameObject.AddComponent(AnimationTesterV2);
	animTester.Setup(boxy);
	Animator.AddAnimatorToSet(animTester);

	//const particleFind = BaseObject.Find(particleSystem.name, ParticleSystem);
	//console.dir(particleFind);
}

import { Time } from "../Modules/System/Time";
import { Debug } from "../Modules/System/Debug";
import { Vector } from "../Modules/Math/Vector";
import { GameObject } from "../Modules/GameObjects/GameObject";
import { MathX } from "../Modules/Math/MathX";
import { Collider2D } from "../Modules/Physics2D/Collider2D";
import { Physics2DUtil } from "../Modules/Physics2D/Physics2DUtil";
import { Transform } from "../Modules/GameObjects/Transform";

enum AnimationType{
	Repeat,
	PingPong
}

export class AnimationTester{
	animationTimer = 0;
	animationTimer2 = 0;
	gameObject: GameObject;
	animType = AnimationType.PingPong;
	prevPosition: Vector;
	prevAngle: number;
	body: Box2D.Dynamics.b2Body | null;
	printPos = new Vector(5, 100);

	constructor(gObj: GameObject){
		this.gameObject = gObj;
		this.prevPosition = this.gameObject.transform.position;
		this.prevAngle = this.gameObject.transform.getAngle2d;
		this.body = this.getPhysBody();
	}

	keyframes = [
		{
			"time": 0.0,
			"transform.localPosition": new Vector(300, 700),
			"transform.localScale": Vector.zero,
			"transform.localRotation": new Vector(0, 0, 0),
			"SpriteRenderer.colorizer.color": new Vector(255, 255, 255, 1),
			"SpriteRenderer.colorizer.operation": "multiply",
			"SpriteRenderer.colorizer.opacity": 1,
			"BoxyChild/BoxyGrandChild|transform.localPosition": new Vector(0, -100),
		},
		{
			"time": 1.0,
			"transform.localPosition": new Vector(300, 700),
			"transform.localScale": Vector.zero,
			"transform.localRotation": new Vector(0, 0, 0),
			"SpriteRenderer.colorizer.color": new Vector(255, 0, 0, 1),
			"SpriteRenderer.colorizer.operation": "multiply",
			"SpriteRenderer.colorizer.opacity": 1/2,
			"BoxyChild/BoxyGrandChild|transform.localPosition": new Vector(100, -100),
		},
		{
			"time": 2.0,
			"transform.localPosition": new Vector(450, 500),
			"transform.localScale": Vector.one,
			"transform.localRotation": new Vector(0, 0, 90 * MathX.DEG2RAD),
			"SpriteRenderer.colorizer.color": new Vector(0, 255, 0, 1),
			"SpriteRenderer.colorizer.operation": "lighten",
			"SpriteRenderer.colorizer.opacity": 1/4,
			"BoxyChild/BoxyGrandChild|transform.localPosition": new Vector(-100, -100),
		},
		{
			"time": 3.0,
			"transform.localPosition": new Vector(600, 700),
			"transform.localScale": Vector.one.multiply(3),
			"transform.localRotation": new Vector(0, 0, -180 * MathX.DEG2RAD),
			"SpriteRenderer.colorizer.color": new Vector(255, 255, 255, 1),
			"SpriteRenderer.colorizer.operation": "multiply",
			"SpriteRenderer.colorizer.opacity": 1,
			"BoxyChild/BoxyGrandChild|transform.localPosition": new Vector(200, -100),
		},
		{
			"time": 4.0,
			"transform.localPosition": new Vector(650, 200),
			"transform.localScale": new Vector(5, 0.2, 1),
			"transform.localRotation": new Vector(0, 0, 0),
			"SpriteRenderer.colorizer.color": new Vector(0, 0, 255, 1),
			"SpriteRenderer.colorizer.operation": "lighten",
			"SpriteRenderer.colorizer.opacity": 1/2,
			"BoxyChild/BoxyGrandChild|transform.localPosition": new Vector(-300, -100),
		},
		{
			"time": 5.0,
			"transform.localPosition": new Vector(1300, 700),
			"transform.localScale": Vector.one,
			"transform.localRotation": new Vector(0, 0, 1080 * MathX.DEG2RAD),
			"SpriteRenderer.colorizer.color": new Vector(255, 255, 255, 1),
			"SpriteRenderer.colorizer.operation": "lighten",
			"SpriteRenderer.colorizer.opacity": 1/8,
			"BoxyChild/BoxyGrandChild|transform.localPosition": new Vector(0, -100),
		},
	];

	getPhysBody(){
		const col = this.gameObject.GetComponent(Collider2D);
		if(col) return col.body;
		return null;
	}

	static SetNestedProperty(obj: any, accessor: string, value: any){
		//Split the accessor string. e.g:"transform.position.x" => ["transform", "position", "x"]
		//If it starts with a special character it is a component. "SpriteRenderer.colorizer.color" : "#FFF"
		const properties = accessor.split(".");	
		let i = 0;
		let currentProperty = obj;

		//Dive into the properties of each subobject
		do {
			const key = properties[i++]
			//This property exists on this object. Use it.
			if(currentProperty[key]){
				currentProperty = (currentProperty as any)[key];
				continue;
			}
			//This property does not exist on this object. Check it's components for it.
			//TODO: Doesn't work when minified.
			if(currentProperty instanceof GameObject){
				const prop = currentProperty.components.find(comp => comp.name === key || comp.GetClassName() === key);
				if(prop){
					currentProperty = prop;
					continue;
				}
				else{
					currentProperty = null; break;
				}
			}
			
			//Found nothing. Exit.
			currentProperty = null; break;
		} while(i < properties.length - 1);
		
		//Once you find the correct subobject, set its value
		if(currentProperty) currentProperty[properties[i]] = value;
	}

	UpdateAnimations(){
		const halfTime = this.keyframes[this.keyframes.length - 1].time;

		if(this.animType === AnimationType.Repeat)
			this.animationTimer = (this.animationTimer + Time.deltaTime) % this.keyframes[this.keyframes.length - 1].time;
		else{
			this.animationTimer2 = (this.animationTimer2 + Time.deltaTime) % (halfTime * 2);
			this.animationTimer = (this.animationTimer2 <= halfTime) ? this.animationTimer2 % halfTime : halfTime - (this.animationTimer2 - halfTime);
		}

		let i = 0;
		for(; i < this.keyframes.length; i++){
			if(this.animationTimer > this.keyframes[i+1].time) {}
			else break;
		} 
		
		const dt = MathX.InverseLerp(this.keyframes[i].time, this.keyframes[i+1].time, this.animationTimer);

		//Mess with object properties
		const keyPrev = this.keyframes[Math.max(0, i-1)];
		const key0 = this.keyframes[i];
		const key1 = this.keyframes[i+1];
		const keyLast = this.keyframes[Math.min(i+2, this.keyframes.length - 1)];

		const xy = Vector.CatmullRom(keyPrev["transform.localPosition"], key0["transform.localPosition"], key1["transform.localPosition"], keyLast["transform.localPosition"], dt);
		const scale = Vector.CatmullRom(keyPrev["transform.localScale"], key0["transform.localScale"], key1["transform.localScale"], keyLast["transform.localScale"], dt);
		const rot = Vector.CatmullRom(keyPrev["transform.localRotation"], key0["transform.localRotation"], key1["transform.localRotation"], keyLast["transform.localRotation"], dt);

		//const colorVect = Vector.SmoothStep(key0["SpriteRenderer.colorizer.color"], key1["SpriteRenderer.colorizer.color"], dt);
		const colorVect = Vector.CatmullRom(keyPrev["SpriteRenderer.colorizer.color"], key0["SpriteRenderer.colorizer.color"], key1["SpriteRenderer.colorizer.color"], keyLast["SpriteRenderer.colorizer.color"], dt);
		const color = `rgba(${colorVect.x}, ${colorVect.y}, ${colorVect.z}, 1)`;

		const opacity = MathX.CatmullRom(keyPrev["SpriteRenderer.colorizer.opacity"], key0["SpriteRenderer.colorizer.opacity"], key1["SpriteRenderer.colorizer.opacity"], keyLast["SpriteRenderer.colorizer.opacity"], dt);

		const grandChildXY = Vector.CatmullRom(keyPrev["BoxyChild/BoxyGrandChild|transform.localPosition"], key0["BoxyChild/BoxyGrandChild|transform.localPosition"], key1["BoxyChild/BoxyGrandChild|transform.localPosition"], keyLast["BoxyChild/BoxyGrandChild|transform.localPosition"], dt);
		//const grandChildXY = Vector.Lerp(key0["BoxyChild/BoxyGrandChild|transform.localPosition"], key1["BoxyChild/BoxyGrandChild|transform.localPosition"], dt);

		//const xy = Vector.SmoothStep(key0["transform.localPosition"], key1["transform.localPosition"], dt);
		//const scale = Vector.SmoothStep(key0["transform.localScale"], key1["transform.localScale"], dt);
		//const rot = Vector.SmoothStep(key0["transform.localRotation"], key1["transform.localRotation"], dt);
		
		//this.gameObject.transform.position = new Vector(x, y);
		//AnimationTester.SetNestedProperty(this.gameObject, "transform.position.x", x);
		//AnimationTester.SetNestedProperty(this.gameObject, "transform.position.y", y);

		AnimationTester.SetNestedProperty(this.gameObject, "transform.localPosition", xy);
		AnimationTester.SetNestedProperty(this.gameObject, "transform.localScale", scale);
		AnimationTester.SetNestedProperty(this.gameObject, "transform.localRotation", rot);
		AnimationTester.SetNestedProperty(this.gameObject, "SpriteRenderer.colorizer.color", color);
		AnimationTester.SetNestedProperty(this.gameObject, "SpriteRenderer.colorizer.operation", key0["SpriteRenderer.colorizer.operation"]);
		AnimationTester.SetNestedProperty(this.gameObject, "SpriteRenderer.colorizer.opacity", opacity);

		const pathSplit = "BoxyChild/BoxyGrandChild|transform.localPosition".split("|");
		const childPath = pathSplit[0];
		const childProperty = pathSplit[1];
		const childObject = Transform.GetChildGameObjectByPath(this.gameObject, childPath);
		if(childObject){
			AnimationTester.SetNestedProperty(childObject, childProperty, grandChildXY);
		}
		///@ts-ignore
		//console.log(childObject.transform.position.toString());

		if(this.body){
			const velocity = this.gameObject.transform.position.subtract(this.prevPosition);
			const angle = this.gameObject.transform.getAngle2d - this.prevAngle;

			this.body.SetLinearVelocity(Physics2DUtil.pxVec2worldVec(velocity.multiply(100)));
			this.body.SetAngularVelocity(-angle * 10);

			this.prevPosition = this.gameObject.transform.position;
			this.prevAngle = this.gameObject.transform.getAngle2d;
		}

		Debug.print(`
			Anim: ${this.animationTimer.toFixed(3)} |
			CurFrame: ${i} |
			Keyframe Time: ${(dt * 100).toFixed(2)}% |
			Op: ${key0["SpriteRenderer.colorizer.operation"]} |
			Velocity: ${Physics2DUtil.worldVec2pxVec((this.body as Box2D.Dynamics.b2Body).GetLinearVelocity()).toString()}`, this.printPos);
	}
}

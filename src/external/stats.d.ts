interface Stats {
	dom: HTMLElement;
	REVISION: number;
	setMode: (mode: number) => void;

	begin(): void;
	end(): number;
	update(): void;

	showPanel(num: number): void;
}

interface StatsConstructor {
	new(): Stats;
}

declare var Stats: StatsConstructor;
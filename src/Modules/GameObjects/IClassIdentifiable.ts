export interface IClassIdentifiable {
	//Prototype Method
	GetClassName(): string;
}

/*
import { Component } from "./Component";
import { GameObject } from "./GameObject";
import { IComponentConstructor } from "./IComponentConstructor";

export class ComponentRecycling{
	static storage: Map<any, Component[]> = new Map();
	static protoStorage: Map<any, Component> = new Map();
	static dummyGameObject = {
		components: [] as Array<Component>
	} as GameObject;

	static RecycleComponent(comp: Component){
		const compKey = this.GetConstructor(comp);

		//Use the component's constructor as key.
		if(!this.storage.has(compKey)){
			this.storage.set(compKey, []);			
		}

		//TODO: Clean the object up. Reset it's properties.
		//this.CleanComponent(comp);

		//Add the component to the stack
		const compArray = this.storage.get(compKey) as Component[];
		compArray.push(comp);
	}

	static UnrecycleComponent<T extends Component>(constructorFn: IComponentConstructor<T>){
		if(this.storage.has(constructorFn)){
			const stack = this.storage.get(constructorFn) as Component[];
			return stack.pop() || null;
		}
		return null;
	}

	static CleanComponent(comp: Component){
		//const constructor = this.GetConstructor(comp);
		//Create a prototype of this component and store it.
		//if(!this.protoStorage.has(constructor)){
		//	this.protoStorage.set(constructor, new (constructor as any)());
		//}

		//Get the prototype and assign all of the properties of the recycled component.
		//const prototype = this.protoStorage.get(constructor) as Component;
		
		//Object.assign(comp, prototype);

		
		//for (const key in comp) {
		//	if (comp.hasOwnProperty(key)) {
		//		const property = (comp as any)[key];
		//	}
		//}

	}

	private static GetConstructor(comp: Component){ return comp.constructor; }
}
*/

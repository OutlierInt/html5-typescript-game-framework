import { GameObject } from "./GameObject";

export interface IComponentConstructor<T> {
	new(gObj: GameObject, name?: string): T;
}
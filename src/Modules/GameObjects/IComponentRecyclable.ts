export interface IComponentRecyclable {
	Recycle(): void;
}
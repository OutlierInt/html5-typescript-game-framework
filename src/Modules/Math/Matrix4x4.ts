import { Vector } from "./Vector";
import { IPassByValue } from "./IPassByValue";
import { IClonable } from "../GameObjects/IClonable";
import { IClassIdentifiable } from "../GameObjects/IClassIdentifiable";

export class Matrix4x4 implements IPassByValue, IClonable, IClassIdentifiable{
	PassByValue = true;

	m00: number; m01: number; m02: number; m03: number;
	m10: number; m11: number; m12: number; m13: number;
	m20: number; m21: number; m22: number; m23: number;
	m30: number; m31: number; m32: number; m33: number;

	constructor(
		m00 = 0, m01 = 0, m02 = 0, m03 = 0, 
		m10 = 0, m11 = 0, m12 = 0, m13 = 0, 
		m20 = 0, m21 = 0, m22 = 0, m23 = 0, 
		m30 = 0, m31 = 0, m32 = 0, m33 = 0,
	){
		this.m00 = m00;
		this.m01 = m01;
		this.m02 = m02;
		this.m03 = m03;
		this.m10 = m10;
		this.m11 = m11;
		this.m12 = m12;
		this.m13 = m13;
		this.m20 = m20;
		this.m21 = m21;
		this.m22 = m22;
		this.m23 = m23;
		this.m30 = m30;
		this.m31 = m31;
		this.m32 = m32;
		this.m33 = m33;
	}
	
	*[Symbol.iterator](){
		yield this.m00; yield this.m01; yield this.m02; yield this.m03;
		yield this.m10; yield this.m11; yield this.m12; yield this.m13;
		yield this.m20; yield this.m21; yield this.m22; yield this.m23;
		yield this.m30; yield this.m31; yield this.m32; yield this.m33;
	};

	//Identify Class by Name
	GetClassName() { return "Matrix4x4"; }

	Clone(){
		return new Matrix4x4(...this);
	}

	GetCell(index: number){
		switch(index){
			case 0:  return this.m00;
			case 1:  return this.m01;
			case 2:  return this.m02;
			case 3:  return this.m03;
			case 4:  return this.m10;
			case 5:  return this.m11;
			case 6:  return this.m12;
			case 7:  return this.m13;
			case 8:  return this.m20;
			case 9:  return this.m21;
			case 10: return this.m22;
			case 11: return this.m23;
			case 12: return this.m30;
			case 13: return this.m31;
			case 14: return this.m32;
			case 15: return this.m33;
			default: return null;
		}
	}

	SetCell(index: number, value: number) {
		switch (index) {
			case 0:  this.m00 = value; break;
			case 1:  this.m01 = value; break;
			case 2:  this.m02 = value; break;
			case 3:  this.m03 = value; break;
			case 4:  this.m10 = value; break;
			case 5:  this.m11 = value; break;
			case 6:  this.m12 = value; break;
			case 7:  this.m13 = value; break;
			case 8:  this.m20 = value; break;
			case 9:  this.m21 = value; break;
			case 10: this.m22 = value; break;
			case 11: this.m23 = value; break;
			case 12: this.m30 = value; break;
			case 13: this.m31 = value; break;
			case 14: this.m32 = value; break;
			case 15: this.m33 = value; break;
			default: return;
		}
	}

	GetCellFromCoords(row: number, col: number){
		return this.GetCell(row * 4 + col);
	}

	SetCellFromCoords(row: number, col: number, value: number) {
		return this.SetCell(row * 4 + col, value);
	}

	toString(){
		return `
		[${this.m00.toFixed(2)}, ${this.m01.toFixed(2)}, ${this.m02.toFixed(2)}, ${this.m03.toFixed(2)}]
		[${this.m10.toFixed(2)}, ${this.m11.toFixed(2)}, ${this.m12.toFixed(2)}, ${this.m13.toFixed(2)}]
		[${this.m20.toFixed(2)}, ${this.m21.toFixed(2)}, ${this.m22.toFixed(2)}, ${this.m23.toFixed(2)}]
		[${this.m30.toFixed(2)}, ${this.m31.toFixed(2)}, ${this.m32.toFixed(2)}, ${this.m33.toFixed(2)}]
		`;
	}

	static Identity(){
		return new Matrix4x4(
			1, 0, 0, 0,
			0, 1, 0, 0,
			0, 0, 1, 0,
			0, 0, 0, 1,
		);
	}

	static Zero(){
		return new Matrix4x4();
	}

	static Multiply(A: Matrix4x4, B: Matrix4x4 | Vector){
		if(B instanceof Matrix4x4) return this.MatrixMatrixMultiply(A, B);
		if(B instanceof Vector) return this.MatrixVectorMultiply(A, B);
		return null;
	}

	private static MatrixMatrixMultiply(A: Matrix4x4, B: Matrix4x4) {
		return new Matrix4x4(
			A.m00 * B.m00 + A.m01 * B.m10 + A.m02 * B.m20 + A.m03 * B.m30,
			A.m00 * B.m01 + A.m01 * B.m11 + A.m02 * B.m21 + A.m03 * B.m31,
			A.m00 * B.m02 + A.m01 * B.m12 + A.m02 * B.m22 + A.m03 * B.m32,
			A.m00 * B.m03 + A.m01 * B.m13 + A.m02 * B.m23 + A.m03 * B.m33,
			A.m10 * B.m00 + A.m11 * B.m10 + A.m12 * B.m20 + A.m13 * B.m30,
			A.m10 * B.m01 + A.m11 * B.m11 + A.m12 * B.m21 + A.m13 * B.m31,
			A.m10 * B.m02 + A.m11 * B.m12 + A.m12 * B.m22 + A.m13 * B.m32,
			A.m10 * B.m03 + A.m11 * B.m13 + A.m12 * B.m23 + A.m13 * B.m33,
			A.m20 * B.m00 + A.m21 * B.m10 + A.m22 * B.m20 + A.m23 * B.m30,
			A.m20 * B.m01 + A.m21 * B.m11 + A.m22 * B.m21 + A.m23 * B.m31,
			A.m20 * B.m02 + A.m21 * B.m12 + A.m22 * B.m22 + A.m23 * B.m32,
			A.m20 * B.m03 + A.m21 * B.m13 + A.m22 * B.m23 + A.m23 * B.m33,
			A.m30 * B.m00 + A.m31 * B.m10 + A.m32 * B.m20 + A.m33 * B.m30,
			A.m30 * B.m01 + A.m31 * B.m11 + A.m32 * B.m21 + A.m33 * B.m31,
			A.m30 * B.m02 + A.m31 * B.m12 + A.m32 * B.m22 + A.m33 * B.m32,
			A.m30 * B.m03 + A.m31 * B.m13 + A.m32 * B.m23 + A.m33 * B.m33,
		);
	}

	static MultiplyPoint(A: Matrix4x4, V: Vector) {
		return new Vector(
			(A.m00 * V.x) + (A.m01 * V.y) + (A.m02 * V.z) + A.m03,
			(A.m10 * V.x) + (A.m11 * V.y) + (A.m12 * V.z) + A.m13,
			(A.m20 * V.x) + (A.m21 * V.y) + (A.m22 * V.z) + A.m23,
			1,
		);
	}

	static MultiplyVector(A: Matrix4x4, V: Vector) {
		return new Vector(
			(A.m00 * V.x) + (A.m01 * V.y) + (A.m02 * V.z),
			(A.m10 * V.x) + (A.m11 * V.y) + (A.m12 * V.z),
			(A.m20 * V.x) + (A.m21 * V.y) + (A.m22 * V.z),
			0,
		);
	}

	private static MatrixVectorMultiply(A: Matrix4x4, V: Vector) {
		return new Vector(
			(A.m00 * V.x) + (A.m01 * V.y) + (A.m02 * V.z) + (A.m03 * V.w),
			(A.m10 * V.x) + (A.m11 * V.y) + (A.m12 * V.z) + (A.m13 * V.w),
			(A.m20 * V.x) + (A.m21 * V.y) + (A.m22 * V.z) + (A.m23 * V.w),
			(A.m30 * V.x) + (A.m31 * V.y) + (A.m32 * V.z) + (A.m33 * V.w),
		);
	}

	static GetTranslationMatrix(v: Vector){
		return new Matrix4x4(
			1, 0, 0, v.x,
			0, 1, 0, v.y,
			0, 0, 1, v.z,
			0, 0, 0, 1,
		);
	}

	static GetScaleMatrix(v: Vector) {
		return new Matrix4x4(
			v.x, 0, 0, 0,
			0, v.y, 0, 0,
			0, 0, v.z, 0,
			0, 0, 0, 1,
		);
	}

	static Get2DRotationMatrix(angle: number) {
		const cos = Math.cos(angle);
    const sin = Math.sin(angle);
		return new Matrix4x4(
			cos, -sin, 0, 0,
			sin, cos, 0, 0,
			0, 0, 1, 0,
			0, 0, 0, 1,
		);
	}

	static GetAxisAngleRotationMatrix(v: Vector, angle: number) {
		const cos = Math.cos(angle);
		const icos = (1 - cos);
		const sin = Math.sin(angle);
		v = v.Normalized;
		const x = v.x, y = v.y, z = v.z;
		const xx = x * x, yy = y * y, zz = z * z;
		const xy = x * y, xz = x * z, yz = y * z;

		return new Matrix4x4(
			cos + (icos * xx),
			(xy * icos) - (z * sin),
			(xz * icos) + (y * sin),
			0,
		
			(xy * icos) + (z * sin),
			cos + (icos * yy),
			(yz * icos) - (z * sin),
			0,
		
			(xz * icos) - (y * sin),
			(yz * icos) + (x * sin),
			cos + (icos * zz),
			0,
			
			0, 0, 0, 1,
		);
	}
}

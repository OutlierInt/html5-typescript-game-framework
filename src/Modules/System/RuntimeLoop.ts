import { Time, FixedUpdateLoop } from "./Time";
import { Camera2D } from "../Render2D/Camera2D";
import { Keyboard } from "../Input/Keyboard";
import { Mouse } from "../Input/Mouse";
import { Debug } from "./Debug";
import { ClearDestroyQueue } from "../GameObjects/BaseObject";
import { CleanObjectComponents } from "../GameObjects/GameObject";
import { DrawPhysicsDebugData, PhysicsInterpolation, PhysicsUpdate } from "../Physics2D/Physics2D";
import { HandleStarts, HandleUpdates, HandleLateUpdates, HandleFixedUpdates } from "../GameObjects/Script";
import { HandleAnimationUpdates } from "../Animation/Animator";

export function RuntimeLoop(timestamp: number = 0) {
	//Preparations
	requestAnimationFrame(RuntimeLoop);
	Time.calculateNextFrameTime(timestamp);
	Camera2D.ClearCanvas("#303");

	//Initialization
	HandleStarts();

	//Fixed Time Step here
	FixedUpdateLoop(function () {
		HandleFixedUpdates();
		PhysicsUpdate();
	});
	//Physics interpolation
	PhysicsInterpolation();

	//Update Game State
	HandleUpdates();
	HandleAnimationUpdates();
	HandleLateUpdates();

	//After Updating Actions
	ClearDestroyQueue();
	//Post Destroy Actions
	CleanObjectComponents();

	//Render Actions
	Camera2D.Draw();
	DrawPhysicsDebugData();

	//Prepare for next loop
	Keyboard.LateUpdate();
	Mouse.LateUpdate();

	Debug.handleDebugCommands();
}
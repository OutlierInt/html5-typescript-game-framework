import { BatchData } from "./AssetManager";
import { Camera2D } from "../Render2D/Camera2D";
import { ctx, canvas } from "./Canvas";

export class LoadingScreen {
	static loadingScreenEnabled = true;
	static batch: BatchData | null = null;
	static message = "Loading...";
	static progress = 0;
	static loadingScreenRAF: number | null = null;

	private static DrawLoadingScreen(timestamp = 0) {
		if (LoadingScreen.loadingScreenEnabled) {
			requestAnimationFrame(LoadingScreen.DrawLoadingScreen);
			Camera2D.ClearCanvas("#000");

			ctx.save();

			const circleRadius = 100;
			const lineWidth = 50;
			let progress = LoadingScreen.progress;

			//Loading progress outer ring
			if (LoadingScreen.batch) {
				const batch = LoadingScreen.batch;
        ctx.fillStyle = "#333";
        ctx.strokeStyle = "#00CDCD";
				progress = batch.count / batch.total;

        ctx.beginPath();
        ctx.arc(canvas.width / 2, canvas.height / 2, circleRadius, 0, 2 * Math.PI * progress);
        ctx.lineWidth = lineWidth;
        ctx.stroke();
        ctx.closePath();
      }

			//Center circle
			ctx.beginPath();
			ctx.arc(canvas.width / 2, canvas.height / 2, circleRadius, 0, 2 * Math.PI);
			ctx.fill();
			ctx.closePath();

			//Outer circle ring
			ctx.save();
			ctx.beginPath();
			ctx.arc(canvas.width / 2, canvas.height / 2, circleRadius + lineWidth / 2, 0, 2 * Math.PI);
			ctx.lineWidth = 2;
			ctx.strokeStyle = "#333";
			ctx.stroke();
			ctx.closePath();
			ctx.restore();

			//Orbiting circle
			ctx.save();
			ctx.translate(canvas.width / 2, canvas.height / 2);
			let circles = 8;
			for (let i = 0; i < circles * 2; i++) {
				ctx.save();

				ctx.rotate(-(timestamp / 2000) + (Math.PI * 2 * (i / (circles * 2))));
				ctx.translate(0, circleRadius + lineWidth / 2 * Math.abs(Math.sin(timestamp / 1000)));

				ctx.beginPath();
				ctx.lineWidth = 2;
				ctx.fillStyle = "#008F8F";
				ctx.strokeStyle = "#333";
				ctx.arc(0, 0, lineWidth / 16, 0, 2 * Math.PI)
				ctx.fill();
				ctx.stroke();
				ctx.closePath();

				ctx.restore();
			}
			for(let i = 0; i < circles; i++){
				ctx.save();
				
				ctx.rotate((timestamp / 1500) + (Math.PI * 2 * (i / circles)));
				ctx.translate(0, circleRadius + lineWidth / 4);

				ctx.beginPath();
				ctx.lineWidth = 2;
				ctx.fillStyle = "#0FF";
				ctx.strokeStyle = "#000";
				ctx.arc(0, 0, lineWidth / 4, 0, 2 * Math.PI)
				ctx.fill();
				ctx.stroke();
				ctx.closePath();

				ctx.restore();
			}
			ctx.restore();

			//Center text
			ctx.font = "40px Arial";
			ctx.fillStyle = "#FFF";
			const text = (progress * 100).toFixed(2) + "%";
			const textMeasurement = ctx.measureText(text);
			ctx.fillText(text, canvas.width / 2 - textMeasurement.width / 2, canvas.height / 2 + 15);

			//Loading message
			const textMeasurement2 = ctx.measureText(LoadingScreen.message);
			ctx.fillText(LoadingScreen.message, canvas.width / 2 - textMeasurement2.width / 2, canvas.height - 100);

			ctx.restore();
		}
	}

	static EnableLoadingScreen(){
		this.loadingScreenEnabled = true;
		this.loadingScreenRAF = requestAnimationFrame(this.DrawLoadingScreen);
	}

	static DisableLoadingScreen() {
		this.loadingScreenEnabled = false;
    this.batch = null;
		this.message = "";
		if(this.loadingScreenRAF){
			cancelAnimationFrame(this.loadingScreenRAF);
			this.loadingScreenRAF = null;
		}
	}
}

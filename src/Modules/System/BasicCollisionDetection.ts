import { Vector } from "../Math/Vector";
import { MathX } from "../Math/MathX";

interface Circle{
	center: Vector,
	radius: number,
}

interface AABB{
	x: number,
	y: number,
	width: number,
	height: number,
}

interface LineSegment{
	p1: Vector,
	p2: Vector,
}

interface Polygon{	
	points: Vector[]
}

function getRectCenter(rect: AABB){
	return new Vector(
		rect.x + rect.width / 2,
		rect.y + rect.height / 2,
	);
}

//Basic Collision Detection (Points/Circles/AABBs)
export function PointPointOverlap(p1: Vector, p2: Vector): boolean{ 
	return p1.Equals(p2);
}

export function PointCircleOverlap(p: Vector, c: Circle): boolean{
	return Vector.Distance(p, c.center) <= c.radius;
}

export function PointAABBOverlap(p: Vector, box: AABB): boolean {
	if (p.x >= box.x &&         		// right of the left edge AND
		p.x <= box.x + box.width &&   // left of the right edge AND
		p.y >= box.y &&         			// below the top AND
		p.y <= box.y + box.height) {  // above the bottom
		return true;
	}
	return false;
}

export function CircleCircleOverlap(c1: Circle, c2: Circle): boolean {
	return Vector.Distance(c1.center, c2.center) < (c1.radius + c2.radius);
}

export function CircleAABBOverlap(c: Circle, rect: AABB){
	//Is the edge of the circle touching the AABB's edge?
	//====================================================
	//Clamp the circle's center to be within the AABB.
	//(This is the closest point to the circle in the AABB.)
	//Is that point within the circle?
	const p = new Vector(
		MathX.Clamp(c.center.x, rect.x, rect.x + rect.width),
		MathX.Clamp(c.center.y, rect.y, rect.y + rect.height)
	);
	return PointCircleOverlap(p, c);
}

export function AABBAABBOverlap(rect1: AABB, rect2: AABB): boolean {
	//AABB Collision Detection is just the 1D overlap, but for their X and Y axes.
	if (rect1.x < rect2.x + rect2.width &&
		rect1.x + rect1.width > rect2.x &&
		rect1.y < rect2.y + rect2.height &&
		rect1.height + rect1.y > rect2.y) {
		return true;
	}
	return false;
}

/*
	A one-dimensional overlap check.
	L1---RECT1---R1
				L2---RECT2---R2
*/
export function SeperatingAxisCheck(l1: number, r1: number, l2: number, r2: number){
	return (l1 < r2) && (r1 > l2);
}

//Line Collisions
export function LinePointOverlap(l: LineSegment, p: Vector, buffer = 0.01) {
	//Get the distance of the point to each of the line's endpoints.
	//If these distances equal the line's length (within precision), the point lies on the line
	const dist1 = Vector.Distance(p, l.p1);
	const dist2 = Vector.Distance(p, l.p2);
	const lineLength = Vector.Distance(l.p1, l.p2);
	return Math.abs((dist1 + dist2) - lineLength) > buffer;
}

export function LineLineOverlap(l1: LineSegment, l2: LineSegment) {
	//Weird looking analytical solution based on line's "standard form"
	const x1 = l1.p1.x;
	const y1 = l1.p1.y;
	const x2 = l1.p2.x;
	const y2 = l1.p2.y;
	const x3 = l2.p1.x;
	const y3 = l2.p1.y;
	const x4 = l2.p2.x;
	const y4 = l2.p2.y;

	const denominator = (y4 - y3) * (x2 - x1) - (x4 - x3) * (y2 - y1);

	//A single solution can be found
	if(denominator !== 0){
		const uA = ((x4 - x3) * (y1 - y3) - (y4 - y3) * (x1 - x3)) / denominator;
		const uB = ((x2 - x1) * (y1 - y3) - (y2 - y1) * (x1 - x3)) / denominator;
		return uA >= 0 && uA <= 1 && uB >= 0 && uB <= 1;
	}
	//These lines are parallel or coincident
	//For simplicity, we will ignore these.
	else{
		return false;
	}
}

export function GetInfiniteLineInfiniteLineIntersection(l1: LineSegment, l2: LineSegment){
	//This is an analytical solution to this problem.
	//It's weird and difficult to explain in layman's terms.
	const p0 = l1.p1;
	const p1 = l1.p2;
	const p2 = l2.p1;
	const p3 = l2.p2;

	const A1 = p1.y - p0.y;
	const B1 = p0.x - p1.x;
	const C1 = A1 * p0.x + B1 * p0.y;

	const A2 = p3.y - p2.y;
	const B2 = p2.x - p3.x;
	const C2 = A2 * p2.x + B2 * p2.y;

	const denominator = A1*B2 - A2*B1;
	const xNumerator = B2*C1 - B1*C2;
	const yNumerator = A1*C2 - A2*C1;

	//Single Solution Found
	if (denominator !== 0) {
		const uA = xNumerator / denominator;
		const uB = yNumerator / denominator;
		return new Vector(uA, uB);
		//return uA >= 0 && uA <= 1 && uB >= 0 && uB <= 1;
	}
	//Special Cases
  else {
		//Colinear (Infinite Solutions)
		if (xNumerator === 0 && yNumerator === 0) return true;
		//Parallel (No solutions)
		//if (xNumerator !== 0 && yNumerator !== 0) return false;
		return false;
  }
}

export function LineCircleOverlap(l: LineSegment, c: Circle) {
	//Find the closest point on the line
	//Is it in the circle?
	const line = l.p2.subtract(l.p1);
	const dCircle = c.center.subtract(l.p1);
	const len = Vector.Distance(l.p1, l.p2);
	const dot = MathX.Clamp01( Vector.Dot(line, dCircle) / (len * len) );
	
	const closestPoint = l.p1.add( line.multiplyVolatile(dot) );
	return PointCircleOverlap(closestPoint, c);

}

export function LineAABBOverlap(l: LineSegment, rect: AABB) {
	//Simple solution: treat the AABB as 4 lines
	return (
		LineLineOverlap(l, {p1: new Vector(rect.x, rect.y), p2: new Vector(rect.x + rect.width, rect.y)}) ||
		LineLineOverlap(l, {p1: new Vector(rect.x, rect.y), p2: new Vector(rect.x, rect.y + rect.height)}) ||
		LineLineOverlap(l, {p1: new Vector(rect.x + rect.width, rect.y), p2: new Vector(rect.x + rect.width, rect.y + rect.height)}) ||
		LineLineOverlap(l, {p1: new Vector(rect.x, rect.y + rect.height), p2: new Vector(rect.x + rect.width, rect.y + rect.height)}) ||
		false
	);
}

//Polygon Collisions
export function PolygonPointOverlap(p: Polygon, v: Vector) {
	//Uses the Raycasting Solution to the Point-in-Polygon problem.
	let collision = false;

	//Iterates through each line in the polygon. Fires a horizontal ray from the point to the right.
	let next = 0;
	for(let i = 0; i < p.points.length; i++){
		next = i + 1;
		if (next === p.points.length) next = 0;

		const vc = p.points[i];
		const vn = p.points[next];

		//Two steps to this:
		//First: Is this segment crossing the point's horizontal axis?
		//Second: Find the x-intercept of this segment with the point's horizontal axis. (Ray-calculation)
		//Is the intercept to the rigt of the point.
		if( ((vc.y > v.y) !== (vn.y > v.y)) && (v.x < (vn.x-vc.x) * (v.y-vc.y) / (vn.y-vc.y) + vc.x) ){
			collision = !collision;
		}
	}

	return collision;
}

export function PolygonCircleOverlap(p: Polygon, c: Circle) {
	return PolygonPointOverlap(p, c.center) || polygonIterate(p, (p1, p2) => LineCircleOverlap({p1, p2}, c));
}

export function PolygonAABBOverlap(p: Polygon, rect: AABB) {
	return PolygonPointOverlap(p, getRectCenter(rect)) || polygonIterate(p, (p1, p2) => LineAABBOverlap({ p1, p2 }, rect));
}

export function PolygonLineOverlap(p: Polygon, l: LineSegment) {
	return PolygonPointOverlap(p, l.p1) || PolygonPointOverlap(p, l.p2) || polygonIterate(p, (p1, p2) => LineLineOverlap({ p1, p2 }, l));
}

export function PolygonPolygonOverlap(p1: Polygon, p2: Polygon) {
	return polygonIterate(p1, (point1, point2) => PolygonPointOverlap(p1, p2.points[0]) || PolygonLineOverlap(p2, { p1: point1, p2: point2 }));
}

//Iterates through each line of the polygon
function polygonIterate(p: Polygon, lineSegmentFunction: (p1: Vector, p2: Vector) => boolean) {
	let next = 0;
	for (let i = 0; i < p.points.length; i++) {
		next = i + 1;
		if (next === p.points.length) next = 0;

		const vc = p.points[i];
		const vn = p.points[next];

		if(lineSegmentFunction(vc, vn)) return true;
	}
	return false;
}

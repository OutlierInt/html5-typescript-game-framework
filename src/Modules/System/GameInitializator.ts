import { SetupPhysics } from "../Physics2D/Physics2D";
import { LoadingScreen } from "./LoadingScreen";
import { ServiceWorkerHandler } from "./ServiceWorkerHandler";
import { AssetManager } from "./AssetManager";
import { CollisionFilter } from "../Physics2D/CollisionFilter";
import { SetupDebugScene } from "../../TestClasses/DebugSetupScene";
import "./WindowVisibilityManager";

export function InitializeGame(){
	//Initial Setup
	LoadingScreen.EnableLoadingScreen();
	SetupPhysics();

	//Setup Service Worker
	return ServiceWorkerHandler.RegisterSW()
	.catch(err => console.error("Failed to start service worker.", err))

	//Load the assets
	.then(() => { LoadingScreen.message = "Loading Assets...";})
	.then(() => AssetManager.LoadJSONPromise("cfg/Assets.json"))
	.then(data => AssetManager.LoadThingsPromise((data as any).assetURLs, batch => {LoadingScreen.batch = batch;}))
	.catch(err => console.error("Failed to load assets.", err))

	//Setup the Collision Matrix
	.then(() => { LoadingScreen.message = "Initializing Collision Matrix..."; })
	.then(() => AssetManager.LoadJSONPromise("cfg/CollisionMatrix.json"))
	.then(data => CollisionFilter.SetupCollisionMaskFromData(data))
	.catch(err => console.error("Failed to load Collision Matrix.", err))

	//Load the scene
	.then(() => { LoadingScreen.message = "Initializing Scene..."; })
	.then(() => SetupDebugScene())
	.then(() => new Promise(resolve => setTimeout(() => resolve(), 250)))
	.catch(err => console.error("Failed to load scene.", err))

	//Remove the loading screen
	.then(() => LoadingScreen.DisableLoadingScreen())
};

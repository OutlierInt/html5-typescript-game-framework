import { AnimationCurve, Keyframe } from "./AnimationCurve";
import { Vector } from "../../Math/Vector";

export class VectorAnimationCurve extends AnimationCurve{
	Tween(key0: VectorKeyframe, key1: VectorKeyframe, t: number){
		//return Vector.Lerp(key0.value, key1.value, t);
		return Vector.CubicBezier(key0.value, key0.value, key1.value, key1.value, t);
	}
}

export interface VectorKeyframe extends Keyframe {
	value: Vector;
}

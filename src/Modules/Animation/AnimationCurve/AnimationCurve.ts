import { MathX } from "../../Math/MathX";

export abstract class AnimationCurve{
	keyframes: Keyframe[];
	length: number;

	constructor(){
		this.keyframes = [];
		this.length = 0;
	}

	ProcessKeys(){
		this.keyframes.sort((a,b) => a.time - b.time);
		this.length = this.keyframes[this.keyframes.length - 1].time;
	}

	Evaluate(time: number){
		time = MathX.Clamp(time, 0, this.length);
		const leftIndex = MathX.GetNearestLeftNeighborIndex(this.keyframes, time, "time");

    const key0 = this.keyframes[leftIndex];
    const key1 = this.keyframes[Math.min(leftIndex + 1, this.keyframes.length - 1)];
		const t = MathX.InverseLerp(key0.time, key1.time, time);
		
		return this.Tween(key0, key1, t);
	}

	abstract Tween(key0: Keyframe, key1: Keyframe, t: number): any;
}

export interface Keyframe{
	time: number;
	value: any;
}

import { AnimationCurve, Keyframe } from "./AnimationCurve";

export class NoTweenAnimationCurve extends AnimationCurve {
	Tween(key0: NoTweenKeyframe, key1: NoTweenKeyframe, t: number) {
		return key0.value;
	}
}

export interface NoTweenKeyframe extends Keyframe {
	value: any;
}

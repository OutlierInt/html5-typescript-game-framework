import { AnimationCurve, Keyframe } from "./AnimationCurve";
import { MathX } from "../../Math/MathX";

export class NumberAnimationCurve extends AnimationCurve{
	Tween(key0: NumberKeyframe, key1: NumberKeyframe, t: number){
		//return MathX.Lerp(key0.value, key1.value, t);
		return MathX.CubicBezier(key0.value, key0.value, key1.value, key1.value, t);
	}
}

export interface NumberKeyframe extends Keyframe {
	value: number;
}

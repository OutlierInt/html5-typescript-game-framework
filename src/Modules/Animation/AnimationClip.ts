import { AnimationCurve, Keyframe } from "./AnimationCurve/AnimationCurve";
import { MathX } from "../Math/MathX";
import { GameObject } from "../GameObjects/GameObject";
import { Transform } from "../GameObjects/Transform";
import { AssetManager } from "../System/AssetManager";
import { NumberAnimationCurve } from "./AnimationCurve/NumberAnimationCurve";
import { VectorAnimationCurve, VectorKeyframe } from "./AnimationCurve/VectorAnimationCurve";
import { NoTweenAnimationCurve } from "./AnimationCurve/NoTweenAnimationCurve";
import { Vector } from "../Math/Vector";
import { AnimationCache, IAnimationCache } from "./AnimationCache";

export class AnimationClip {
	name: string;
	tweens: Tween[];
	length: number;

	constructor(name = ""){
		this.name = name;
		this.tweens = [];
		this.length = 0;
	}

	SetCurve(property: string, curve: AnimationCurve, path: string = ""){
		//Just in case
		curve.ProcessKeys();

		//Update the length
		const longestTime = curve.keyframes[curve.keyframes.length - 1].time;
		this.length = Math.max(this.length, longestTime);
		
		//Add this curve to the clip
		this.tweens.push({
			curve,
			property,
			path,
		});
	}

	SampleAnimation(gObj: GameObject, time: number, cache?: IAnimationCache){
		time = MathX.Clamp(time, 0, this.length);
		for(let i = 0; i < this.tweens.length; i++){
			const tween = this.tweens[i];

			if(cache){
				//Use the cache to speed up this process.
				const cacheSuccess = AnimationCache.UseCache(tween, time, gObj, cache);
	
				//The cache failed. the object was modified or deleted or something.
				if(!cacheSuccess){
					try{
						//Do it manually.
						const manualSuccess = this.SampleAnimationManual(tween, gObj, time);
						//If it worked manually, update the cache accordingly.
						if(manualSuccess){
							AnimationCache.AddTweenToCache(tween, gObj, cache);
							cache.failedTweens.delete(tween);
						}
						//Doing it manually failed. Track the failed tween, if it hasn't been already.
						else if(!cache.failedTweens.has(tween)){
							cache.failedTweens.add(tween);
						}
					}
					catch(e){
						console.error(e);
					}
				}
			}
			//No cache available. Manually search each object for the correct property to change.
			else{
				this.SampleAnimationManual(tween, gObj, time);
			}			
		}
	}

	private SampleAnimationManual(tween: Tween, gObj: GameObject, time: number) {
		//Get the root/child object, if it exists.
		const obj = tween.path ? Transform.GetChildGameObjectByPath(gObj, tween.path) : gObj;
		if (!obj) return false;

		//Find the object to animate, if it exists.
		const animObject = AnimationClip.FindNestedObject(obj, tween.property);
		if(!animObject) return false;

		//Once you find the correct subobject, set its value.
		const tweenResult = tween.curve.Evaluate(time);
		AnimationClip.SetObjectPropertyDirectly(tween.property, animObject, tweenResult);
		return true;
	}

	static FindNestedObject(obj: any, accessor: string){
		//Split the accessor string. e.g:"transform.position.x" => ["transform", "position", "x"]
		//If it starts with a special character it is a component. "SpriteRenderer.colorizer.color" : "#FFF"
		const properties = accessor.split(".");	
		let i = 0;
		let curObject = obj;

		//Dive into the properties of each subobject
		do {
			const key = properties[i++]
			//This property exists on this object. Use it.
			if(curObject[key]){
				curObject = (curObject as any)[key];
				continue;
			}
			//This property does not exist on this object. Check it's components for it.
			if(curObject instanceof GameObject){
				const prop = curObject.components.find(comp => comp.name === key || comp.GetClassName() === key);
				if(prop){
					curObject = prop;
					continue;
				}
				else{
					curObject = null; break;
				}
			}
			
			//Found nothing. Exit.
			curObject = null; break;
		} while(i < properties.length - 1);
		
		//Once you find the correct subobject, set its value
		return curObject;
	}

	static SetNestedProperty(obj: any, accessor: string, value: any){
		//Find the object to animate.
		const animObject = this.FindNestedObject(obj, accessor);

		//Once you find the correct subobject, set its value.
		AnimationClip.SetObjectPropertyDirectly(accessor, animObject, value);
	}

	private static SetObjectPropertyDirectly(accessor: string, animObject: any, value: any) {
		const property = this.GetLastAccessorProperty(accessor);
		if (animObject && property)
			animObject[property] = value;
	}

	static GetLastAccessorProperty(accessor: string){
		const lastIndex = accessor.lastIndexOf(".");
		const property = accessor.substring(lastIndex + 1);
		return property;
	}

	static CreateAnimationClipFromFile(url: string, name: string, callback: (clip: AnimationClip) => void){
		return AssetManager.LoadJSONPromise(url)
		.then(data => this.CreateClipFromData(data as FileCurveTypeList[], name))
		.then(clip => callback(clip))
		.catch(err => console.error("Failed to create Animation Clip", err));
	}

	private static CreateClipFromData(data: FileCurveTypeList[], name: string){
		const clip = new AnimationClip(name);

		//Go through each curve list in the file.
		for (const curveTypeList of data) {
			for (const curve of curveTypeList.curves) {
				//Get the type of curve we need.
				let animationCurve = AnimationClip.CreateAnimationCurve(curveTypeList);

				//Add keys to the curve
				//animationCurve.keyframes = curve.curve;
				animationCurve.keyframes = AnimationClip.AddKeysToAnimationCurve(curveTypeList, curve.curve);
				animationCurve.ProcessKeys();

				//Add the curve to the clip
				clip.SetCurve(curve.property, animationCurve, curve.path);
			}	
		}

		return clip;
	}

	private static CreateAnimationCurve(curveList: FileCurveTypeList) {
		switch (curveList.curveType) {
			case "number": return new NumberAnimationCurve();
			case "vector": return new VectorAnimationCurve();
			case "notween": return new NoTweenAnimationCurve();
			default: return new NoTweenAnimationCurve();
		}
	}

	private static AddKeysToAnimationCurve(curveList: FileCurveTypeList, keys: Keyframe[]) {
		switch (curveList.curveType) {
			case "number": return keys;
			case "vector": return keys.map(key => {key.value = new Vector(...key.value); return key;}) as VectorKeyframe[];
			case "notween": return keys;
			default: return keys;
		}
	}
}

export interface Tween{
	curve: AnimationCurve;
	property: string;
	path?: string;
}

//File Management
interface FileCurveTypeList{
	curveType: "number" | "vector" | "notween";
	curves: FileCurve[]
}

interface FileCurve{
	property: string;
	curve: Keyframe[];
	path?: string;
}

/*
export class Animation{
	animName: string;
	animSpeed: number;
	animRepeat: boolean;
	animRepeatCount: number;
	animRepeatPingPong: boolean;
	frames: Sprite[];

	constructor(name: string, frames? : Sprite[]){
		this.animName = name;
		this.frames = frames || [];
		this.animSpeed = 0;
		this.animRepeat = false;
		this.animRepeatCount = 0;
		this.animRepeatPingPong = false;
	}
}
*/

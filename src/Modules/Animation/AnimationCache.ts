import { AnimationClip, Tween } from "./AnimationClip";
import { GameObject } from "../GameObjects/GameObject";
import { Transform } from "../GameObjects/Transform";

export class AnimationCache{
	static get defaultPath(){ return "/"; }

	static GenerateAnimationCache(gObj: GameObject, clip: AnimationClip){
		const cache: IAnimationCache = {
			failedTweens: new Set(),
			data: {}
		};
		for(let i = 0; i < clip.tweens.length; i++){
			const tween = clip.tweens[i];
			AnimationCache.AddTweenToCache(tween, gObj, cache);
		}
		return cache;
	}

	static AddTweenToCache(tween: Tween, gObj: GameObject, cache: IAnimationCache) {
		const obj = tween.path ? Transform.GetChildGameObjectByPath(gObj, tween.path) : gObj;
		const animObj = AnimationClip.FindNestedObject(obj, tween.property);
		const propertyName = AnimationClip.GetLastAccessorProperty(tween.property);
		const path = tween.path || AnimationCache.defaultPath;

		if (!cache.data[path]) cache.data[path] = {};
		cache.data[path][tween.property] = {
			rootObject: obj,
			animObject: animObj,
			propertyName: propertyName
		};
	}

	static UseCache(tween: Tween, time: number, gObj: GameObject, cache: IAnimationCache){
		const path = tween.path || AnimationCache.defaultPath;

		//This tween failed before...
		if(cache.failedTweens.has(tween)){
			return false;
			//throw new Error(`Recieved tween that this cache recorded as failed. ${tween.path}|${tween.property}`);
		}

		//Invalid path name. Perhaps the object was deleted/changed name?
		if (!cache.data[path]) {
			return false;
      //throw new Error(`Path '${path}' not found in cache. Perhaps the Transform was deleted or changed its name?`);
    }
		//Invalid property name.
		if (!cache.data[path][tween.property]) {
			return false;
      //throw new Error(`Property '${path}'|${tween.property} not found in cache.`);
    }

		const cacheData = cache.data[path][tween.property];
		//Animating Object not found.
		if (!cacheData.animObject){
			return false;
			//throw new Error(`Object ${path}|${tween.property.split(".")[0]} not found in cache.`);
		}

		const tweenResult = tween.curve.Evaluate(time);
		const animObject = cacheData.animObject as any;
		if (!animObject[cacheData.propertyName]){
			return false;
			//throw new Error(`Property ${cacheData.propertyName} doesn't exist on object '${path}'/${tween.property.split(".")[0]}.`);
		}

		//Gauntlet Complete! Set the property!
		animObject[cacheData.propertyName] = tweenResult;
		return true;
	}
}

//Animation Cache
export interface IAnimationCache {
	failedTweens: Set<Tween>,
	data: CacheData
}

interface CacheData {
	[path: string]: {
		[property: string]: {
			rootObject: object | null,
			animObject: object | null,
			propertyName: string,
		}
	}
}

class AnimationCacheError extends Error {
  failedAccessor?: string;
  constructor(message: string, failedAccessor?: string) {
    super(message);
    this.message = message;
    this.name = "AnimationCacheError";
    this.failedAccessor = failedAccessor;
  }
}

/* Example Cache Structure
const cache = {
	'BoxyChild/BoxyGrandChild': {
		'transform.localPosition': {
			rootObject: [[BoxyGrandChild]],
			animObject: [[BoxyGrandChild.transform]],
			propertyName: "localPosition",
		}
	},
	'/': {
		'transform.localPosition': {
			rootObject: gObj,
			animObject: [[gObj.transform]],
			propertyName: "localPosition",
		},
		'SpriteRenderer.colorizer.opacity':{
			rootObject: gObj,
			animObject: [[gObj.SpriteRenderer.colorizer]],
			propertyName: "opacity",
		}
	}
};
*/
import { AnimationClip } from "./AnimationClip";
import { Time } from "../System/Time";
import { MathX } from "../Math/MathX";
import { GameObject } from "../GameObjects/GameObject";
import { IAnimationCache } from "./AnimationCache";

export enum AnimationType {
	Repeat,
	PingPong,
	Once,
}

export class AnimationController{
	gameObject: GameObject | null;
	clip: AnimationClip | null;
	animationType: AnimationType;
	progress: number;
	progressPingPong: number;
	speed: number;
	repeatCount: number;
	repeatedCount: number;
	isPlaying: boolean;

	cache: IAnimationCache | undefined;

	constructor(){
		this.gameObject = null;
		this.clip = null;
		this.progress = this.progressPingPong = 0;
		this.isPlaying = false;
		this.speed = 1;
		this.animationType = AnimationType.Repeat;
		this.repeatCount = Infinity;
		this.repeatedCount = 0;
	}

	Play(){
		if(!this.isPlaying && this.clip){
			this.isPlaying = true;
			this.repeatedCount = 0;
		}
	}

	Stop(){
		if(this.isPlaying){
			this.isPlaying = false;
		}
	}

	Seek(position: number){
		if(this.clip){
			this.progress = MathX.Clamp(position, 0, this.clip.length);
			this.SampleAnimation();
		}
	}

	UpdateAnimationState(){
		if (!this.gameObject || !this.clip || !this.isPlaying){ this.Stop(); return; }

		const clipLength = this.clip.length;
		const increaseAmount = Time.deltaTime * this.speed;

		switch(this.animationType){
			case AnimationType.Repeat:
				this.RepeatHandler(increaseAmount, clipLength);
				break;

			case AnimationType.PingPong:
				this.PingPongHandler(increaseAmount, clipLength);
				break;

			case AnimationType.Once:
				this.progress = MathX.Clamp(this.progress + increaseAmount, 0, clipLength);
				if(this.progress >= clipLength) this.Stop();
				break;

			default:
				this.progress = 0;
				this.Stop();
				break;
		}

		//TODO: Cache the locations of the animation clips' fields in a weakmap
		this.SampleAnimation();
	}

	SampleAnimation(){
		if(this.clip && this.gameObject) this.clip.SampleAnimation(this.gameObject, this.progress, this.cache);
	}

	private RepeatHandler(increaseAmount: number, clipLength: number) {
		const newProgress = this.progress + increaseAmount;

		//Infinite Repeats
		if (this.repeatCount === Infinity){
			this.progress = newProgress % clipLength;
			return;
		}

		//Track repeat counts
		if (newProgress >= clipLength) {
			if(++this.repeatedCount >= this.repeatCount){
				this.progress = clipLength;
				this.Stop();
			}
			else
				this.progress = newProgress % clipLength;
		}
		else {
			this.progress = newProgress;
		}
	}

	private PingPongHandler(increaseAmount: number, clipLength: number) {
		const pingpongProgress = this.progressPingPong + increaseAmount;
		const clipLengthX2 = clipLength * 2;

		//Infinite Repeats
		if (this.repeatCount === Infinity) {
			this.progressPingPong = pingpongProgress % clipLengthX2;
			this.progress = (this.progressPingPong <= clipLength) ? this.progressPingPong % clipLength : clipLength - (this.progressPingPong - clipLength);
			return;
		}

		//Track repeat counts
		if (pingpongProgress >= clipLengthX2) {
			if (++this.repeatedCount >= this.repeatCount) {
				this.progress = 0;
				this.progressPingPong = clipLengthX2;
				this.Stop();
			}
			else{
				this.progressPingPong = pingpongProgress % clipLengthX2;
				this.progress = (this.progressPingPong <= clipLength) ? this.progressPingPong % clipLength : clipLength - (this.progressPingPong - clipLength);
			}
		}
		else{
			this.progressPingPong = pingpongProgress;
			this.progress = (this.progressPingPong <= clipLength) ? this.progressPingPong % clipLength : clipLength - (this.progressPingPong - clipLength);
		}
	}
}

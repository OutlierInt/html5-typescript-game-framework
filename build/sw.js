const cacheName = "OFFLINE_CACHE";
const appshellName = "appshell.json";
const swMessagePrefix = "[Service Worker] ";
const logMessages = true;

function fetchAsset(request, bypassCache){
	if(typeof request === "string"){
		request = new Request(request)
	}

	if(bypassCache){
		return fetch(request.url, {
			headers: request.headers,
			mode: request.mode,
			credentials: request.credentials,
			redirect: request.redirect,
			cache: "no-cache"
		});
	}
	else{
		return fetch(request);
	}
}

self.addEventListener("install", function (ev) {
	logMessage("Installing...");

	//Install process goes inside this waitUntil.
	ev.waitUntil(
		caches.open(cacheName)
		.then(function(cache){
			
			logMessage("Opened cache for app");
			return fetchAsset(appshellName, true)
			.then(res => res.json())
			.then(data => {
				let shellFiles = data.assetURLs;

				return fetchAsset("cfg/Assets.json",true)
				.then(req => req.json())
				.then(assetData => {
					const assetFiles = assetData.assetURLs;
					shellFiles = shellFiles.concat(assetFiles);
					logMessage("Loaded Appshell and Assets.json. Caching files:", shellFiles);
					return shellFiles;
				})
				.then(files => cache.addAll(files))
				.then(() => logMessage("Appshell and Assets caching successful!"))
				.then(() => logMessage("Installation successful!"))
				.catch(err => errorMessage("Failed to load cfg/assets.json", err));
			})
			.catch(err => errorMessage("Failed to load" + appshellName, err));
		})
	);
});

self.addEventListener("fetch", function (ev) {
	ev.respondWith(
		caches.match(ev.request)
		.then(function(cachedResource){
			if(cachedResource){
				logMessage("Fetching cached resource", ev.request.url);
				return cachedResource;
			} 
			else{
				fetch(ev.request)
				.then(response => {
					return caches.open(cacheName)
					.then(cache => {
						logMessage("Downloading new resource", ev.request.url);
						cache.put(ev.request, response.clone())
						return response;
					})
					.catch(err =>	logMessage("Failed to load resource", ev.request.url, err));
				})
			}
		})
	);
});

self.addEventListener("activate", function(ev){
	logMessage("Activated...");
	var cacheWhitelist = [cacheName];

	ev.waitUntil(
		caches.keys()
		.then(keyList => {
			logMessage("Found caches:", keyList);
			return Promise.all(keyList.map(cleanCaches(cacheWhitelist)))
		})
		.catch(() => errorMessage("Error while cleaning caches.", err))
		.then(() => logMessage("Activation Complete!"))
	);
});

function cleanCaches(cacheWhitelist) {
	return function (key) {
		if (cacheWhitelist.indexOf(key) === -1) {
			logMessage("Deleting cache", key);
			return caches.delete(key);
		}
	};
}

function logMessage(message, ...args){
	const logValues = (args.length > 0) ? args : [""];
	if (logMessages)
		console.log(swMessagePrefix + message, ...logValues);
}

function errorMessage(message, ...args) {
	const logValues = (args.length > 0) ? args : [""];
	if (logMessages)
		console.error(swMessagePrefix + message, ...logValues);
}